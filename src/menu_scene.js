class Menu_Scene
{
    constructor()
    {
        this.only_once = true;
        console.log("new menu");

        File_Reader.names_text = File_Reader.readTextFile('Assets/tank_names.txt');

    }
    start()
    {
        document.getElementById ("play_btn").addEventListener ("click", start_game, false);

        if(this.only_once)
        {                
            document.body.addEventListener("mousemove",this.play_background,false);

       
            this.only_once = false;
        }
     
    }

    play_background()
    {
        Audio_Manager.instance.play_menu_background();
    }

    hide()
    {
        document.body.removeEventListener("mousemove", this.play_background, false);
        Audio_Manager.instance.stop_menu_background();
        Audio_Manager.instance.play_game_background();
        Audio_Manager.instance.play_tank_movement();

        //document.getElementById ("menu_scene").className += 'invisible';

        document.getElementById ("menu_scene").classList.add('invisible');
    }
}