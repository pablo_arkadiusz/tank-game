
class Camera
{
    constructor(player)
    {
        this.target = player;
        this.offset = {x: player.position.x, y: 400};
        this.target_pos = {x: 0, y: 0};

        this.minX = 0;
        this.maxX = 800;
        this.minY = 0;

        this.movement_header = new Vector2();

        this.zoomed = false;
        this.actual_zoom_scale = 1;
        this.zoom_scale = 1;
        this.pos_scale = 0;

        this.position = new Vector2();
    }

    update(delta_time)
    {
        this.move_smoothly();
           
    }
    move_smoothly()
    {
        this.target_pos.x = this.target.position.x - this.movement_header.x- canvas_array[0].width/2;
        this.target_pos.y = this.target.position.y - this.movement_header.y - canvas_array[0].height/2;

        if(this.movement_header.x != 0)
            this.movement_header.x -= this.movement_header.x * 0.02; 
        
        if(this.movement_header.y != 0)
            this.movement_header.y -= this.movement_header.y * 0.02; 
    }

    change_target(new_target)
    {
        console.log("changed target");
        let camera_pos = this.target.position.subtract(this.movement_header);

        pink_point_x = camera_pos.x;
        pink_point_y = camera_pos.y;

        this.movement_header = new_target.position.subtract(camera_pos);

        this.target = new_target;
    }

    zoom_out() { this.zoomed = true; }

    zoom_in() { this.zoomed = false; }

    zoom_smoothly()
    {
        if(this.zoomed)
        {
            if(this.actual_zoom_scale > 0.5)
            {
                this.actual_zoom_scale -= 0.005;
                this.pos_scale += 0.005;
            } 
        }
        else
            if(this.actual_zoom_scale < 1)
            {
                this.actual_zoom_scale += 0.005;
                this.pos_scale -= 0.005;
            }
    }
    pre_draw(ctx)
    {
        ctx.save();      
        this.zoom_smoothly();

        ctx.scale(  this.actual_zoom_scale,  this.actual_zoom_scale);
        this.position.set(-this.target_pos.x + canvas_array[0].width * this.pos_scale,  -this.target_pos.y + canvas_array[0].height * this.pos_scale);
        ctx.translate(this.position.x + .5 | 0 , this.position.y + .5 | 0 );
    }

    post_draw(ctx) { ctx.restore(); }
}
