class IA_Tank
{

    //#region Initialization methods

    constructor(pos, weapon_name,id, team, ia_type, protecting_tank)
    {

        //#region Main member variables

        this.type = "IA Tank";
        //Type of the AI: scout, escort or defender
        this.ia_type = ia_type;
        this.team = team
        //Position of the platform on which this tank spawns
        this.platform_pos = pos;
        this.id = id;

        this.player_is_near = false;
        this.health_bar = new Health_Bar();
        
        this.alive = true;
        this.time_to_respawn = 0;
      
        //#endregion

        //#region Stadistics member varaibles
        this.asign_name();
        this.kills = 0;
        this.deaths = 0;
        this.cows = 0;
        this.points = 0;

        //#endregion
        //#region Movement member variables

        //The real direction of the movement
        this.actual_direction = new Vector2();
        this.position = new Vector2( pos.x,pos.y );
        this.actual_rotation = 0;
        this.desired_rotation = 0;
        this.cannon_rotation = 0;
        this.desired_direction;

        //#endregion

        //#region  Body creation

        if(this.ia_type == "scout" )
        {
            this.initial_health = 70;

            this.move_speed = 15;
            //Physics properties of the tank body
            this.physicsOptions = 
            {
                linearDamping : 5,
                angularDamping: 5,
                friction: 0,
                restitution : 0,
                density: 0, //makes the body not rotate with the hits
                type: b2Body.b2_dynamicBody,
                user_data: this,
            }

        }
        else if (this.ia_type == "escolt")
        {
            this.initial_health = 100;

            this.move_speed = 10;
            //Physics properties of the tank body
            this.physicsOptions = 
            {
                linearDamping: 5,
                angularDamping: 5,
                friction: 0,
                restitution : 0,
                density: 0, //makes the body not rotate with the hits
                type: b2Body.b2_dynamicBody,
                user_data: this,
            };
        }
        else
        {
            this.initial_health = 200;
            this.move_speed = 15;
            //Physics properties of the tank body
            this.physicsOptions = 
            {
                linearDamping : 10,
                angularDamping: 10,
                friction: 0,
                restitution : 0,
                density: 0, //makes the body not rotate with the hits
                type: b2Body.b2_dynamicBody,
                user_data: this,
            }
        }
        this.health =  this.initial_health;
        this.body = CreateBall(this.position.x / scale, (canvas_array[0].height - this.position.y) / scale,
            0.2, this.physicsOptions, collision_rule.IA);
        this.body.GetFixtureList().SetUserData(this);
        this.update_position();

        //#endregion

        //#region Draw member variables
        this.explosion_animation = new SSAnimation(graphics.explosion.image, 64, 64, [5,5,5,5,5], 1/24, false);
        this.play_exp_anim = false;
        this.configure_canvas();

   
        this.pivot = { x: - this.body_canvas.width  >> 1, y: - this.body_canvas.height >> 1 };
        this.cannon_pivot = { x: -this.cannon_canvas.width >> 1, y: this.cannon_canvas.height - this.cannon_canvas.height >> 1  };

        //#endregion

        //#region Fire variables

        //Target at which the tank will aim
        this.fire_target = undefined;
        //The radius of the sensor that detects other tanks
        IA_Tank.sensor_radius = 500;
        this.create_sensor();
        //Tanks that are in the sensor radius area
        this.posible_targets = [];
        this.select_weapon(weapon_name);

        //#endregion

        //#region Path member variables

        //The pathfinding mind of the tank
        this.pathfinding = new Horizontal_Search(this);      
        this.path_index = 0;
        this.going_base = false;
        //The AI states have two movement types: going straight to target or moving around in circles
        this.go_straight = false;

        //#endregion

        //#region Flag member variables

        this.asign_base_flag();
        this.carried_flag = null;
        this.seeing_flag = false;
        //#endregion
        
        //#region Prepare AI type

        if(this.ia_type == "scout")
        {
            this.state = "Follow flag"; 
            this.select_random_flag();
            this.find_path(this.target.position);
        }
        else if(this.ia_type == "escolt")
        {
            this.state = "Follow tank";
            this.target = this.main_tank_to_escolt = this.actual_tank_to_escort = protecting_tank; 
            this.find_path(this.target.position);
        }
        else // if Guard
        {   
            this.state = "Follow flag";
            this.target = this.base_cow;
            this.find_path(this.target.position);

        }
        //#endregion
    }

    asign_name()
    {
        this.name = File_Reader.names_text_arr[this.id];
    }

    asign_base_flag()
    { this.base_cow = this.team == "red" ?  red_cow : this.team == "green" ? green_cow: this.team == "black" ? black_cow: blue_cow; }

    configure_canvas()
    {
        switch(this.team)
        {
            case "red":
                    switch(this.ia_type)
                    {
                        case "scout":case "guard":
                            this.cannon_canvas = IA_Tank.red_scout_cannon_canvas;
                            this.body_canvas = IA_Tank.red_scout_body_canvas;
                            this.body_with_cow_canvas = IA_Tank.red_scout_body_with_cow_canvas;
                            break;

                        case "escolt":
                            this.cannon_canvas = IA_Tank.red_escolt_cannon_canvas;
                            this.body_canvas = IA_Tank.red_escolt_body_canvas;
                            this.body_with_cow_canvas = IA_Tank.red_escolt_body_with_cow_canvas;
                            break;
                    }
                break;
            case "blue":
                switch(this.ia_type)
                {
                    case "scout":case "guard":   
                        this.cannon_canvas = IA_Tank.blue_scout_cannon_canvas;   
                        this.body_canvas = IA_Tank.blue_scout_body_canvas;
                        this.body_with_cow_canvas = IA_Tank.blue_scout_body_with_cow_canvas;
                        break;

                    case "escolt": 
                        this.cannon_canvas = IA_Tank.blue_escolt_cannon_canvas;   
                        this.body_canvas = IA_Tank.blue_escolt_body_canvas;
                        this.body_with_cow_canvas = IA_Tank.blue_escolt_body_with_cow_canvas;
                        break;
                }
                break;
            case "black":
                switch(this.ia_type)
                {
                    case "scout": case "guard":     
                        this.cannon_canvas = IA_Tank.black_scout_cannon_canvas;   
                        this.body_canvas = IA_Tank.black_scout_body_canvas;
                        this.body_with_cow_canvas = IA_Tank.black_scout_body_with_cow_canvas;
                        break;

                    case "escolt":                 
                        this.cannon_canvas = IA_Tank.black_escolt_cannon_canvas;   
                        this.body_canvas = IA_Tank.black_escolt_body_canvas;
                        this.body_with_cow_canvas = IA_Tank.black_escolt_body_with_cow_canvas;
                        break;
                }
                break;
            case "green":
                switch(this.ia_type)
                {
                    case "scout":case "guard":     
                        this.cannon_canvas = IA_Tank.green_scout_cannon_canvas;   
                        this.body_canvas = IA_Tank.green_scout_body_canvas;
                        this.body_with_cow_canvas = IA_Tank.green_scout_body_with_cow_canvas;
                        break;

                    case "escolt":
                        this.cannon_canvas = IA_Tank.green_escolt_cannon_canvas;   
                        this.body_canvas = IA_Tank.green_escolt_body_canvas;
                        this.body_with_cow_canvas = IA_Tank.green_escolt_body_with_cow_canvas;
                        break;
                }
                break;
        }
        this.actual_body_canvas = this.body_canvas;
    }

    select_image_with_cow() { this.actual_body_canvas = this.body_with_cow_canvas;}
    select_default_image() { this.actual_body_canvas = this.body_canvas;}

    //Create a sensor to detect near tanks
    create_sensor()
    {
        let shape = new b2CircleShape();
        shape.m_radius = IA_Tank.sensor_radius / scale;

        // Fixture: define physics properties (density, friction, restitution)
        var fix_def = new b2FixtureDef();
        fix_def.shape = shape;
         
    
        fix_def.filter.categoryBits = collision_rule.SENSOR.collision;
        fix_def.filter.maskBits = collision_rule.SENSOR.layer;
        this.sensor = this.body.CreateFixture(fix_def);
        this.sensor.SetUserData("Sensor");
        this.sensor.SetSensor(true);     
    }

    select_weapon(weapon_name)
    {
        switch(weapon_name)
        {
            case "Rifle":
                this.weapon = new Rifle(this);
                break;
            case "Granade":
                this.weapon = new GranadeLauncher(this);
                break;
            case "MachineGun":
                this.weapon = new MachineGun(this);
                break;
            case "AutoMissile":
                this.weapon = new AutoMissile(this);
                break;
        }
    }

    select_random_flag()
    {
        let random_num = Math.floor(Math.random() * 3 + 1);

        switch(this.team)
        {
            case "red":   this.target = random_num == 1 ? blue_cow : random_num == 2 ? black_cow : green_cow; break;
            case "blue":  this.target = random_num == 1 ? red_cow : random_num == 2 ? black_cow : green_cow;  break;
            case "black": this.target = random_num == 1 ? blue_cow : random_num == 2 ? red_cow : green_cow;   break;
            case "green": this.target = random_num == 1 ? blue_cow : random_num == 2 ? black_cow : red_cow;   break;
        }
    }

    //#endregion

    //#region Pathfinding adn movement methods

    //Find path to the target position using the HorizontalSearch class
    find_path(target_pos)
    {
        this.pathfinding.find_path(this.position, target_pos);
        this.path = [];
        this.path = this.pathfinding.path;
        this.path_index = 1
        if(this.path[1] == undefined)
        {
            
            console.log("path is null: " + (this.path == null));
            console.log("path  lenght : " + (this.path.length));

            console.log("path[0]: " + this.path[0]);
        }
        this.next_pos = this.path[1];
        if(this.next_pos != undefined)
            this.desired_direction = this.next_pos.subtract(this.path[0]).normalize();  
    }

    target_point_reached() { return this.path.length == 0 || this.position.subtract(this.path[this.path_index]).magnitude() < 100; }

    change_to_circlular_movement()
    {
        this.body.SetLinearVelocity(new b2Vec2(0,0));
        this.go_straight = false;
        this.path = [];
        this.path_index=0;
    };

    move()
    {
        try
        {

       
        this.desired_direction = this.next_pos.subtract(this.position).normalize();
        this.desired_rotation = Math.atan2( this.desired_direction.y, this.desired_direction.x) + 1.56;
        this.actual_direction.set
        (
            Math.sin( this.actual_rotation ),
            Math.cos( this.actual_rotation )
        );
        //this.body.SetLinearVelocity({x: this.actual_direction.x *this.move_speed, y:  this.actual_direction.y * this.move_speed});

            let velocity = {x: this.actual_direction.x * this.move_speed, y: this.actual_direction.y * this.move_speed};
            
            this.body.ApplyForce(velocity, this.body.GetWorldCenter()); 
        }
        catch(exception)
        {
            console.log("Error: next pos is " + this.next_pos);
        }
    }

    //Makes the tank go straight to its target
    follow_target(target_pos) 
    {
        if(this.position.subtract(target_pos).magnitude() < 100)
            if((this.state == "Follow tank") || (this.ia_type == "guard" && this.target == this.base_cow && this.carried_flag == null) ||  (this.state == "Follow flag" && !this.target.visible ) || (this.state == "Follow flag" && !this.target.is_in_his_base() && target_pos != this.target.position)  )    
                { this.change_to_circlular_movement(); return; }
        if(this.target_point_reached())
            this.find_path(target_pos);
            try
            {
                this.move();

            }
            catch(exception)
            {
                console.log("Error: next pos is " + this.next_pos);
            }
    }

    change_to_straight_movement()
    {
        this.path = [];
        this.path_index = 0;
        this.go_straight=true;
    }

    movement_logic()
    {  
        let target_pos;
        if(this.ia_type == "scout")
        {
            //If the tank is going to the base flag, but the base flag has been stolen it will go to the original base position
            if(this.target == this.base_cow && this.going_base && !this.base_cow.is_in_his_base())
                target_pos = this.target.initial_pos;  
            else
            {
                //If someone of the team is carrying the target cow/flag, it will go to the cow real position (this.target.position)
                if(Tank_Manager.escorting_flag[this.team].some(e => (e == this.target)) || (this.seeing_flag && this.target.visible))
                    target_pos = this.target.position;
                
                else   //If not it will go to the base position, so it may find that the cow has been stolen by another team      
                    target_pos = this.target.initial_pos;   
            }
        }
        else if(this.ia_type == "escolt")  
        {          
            if(this.target == this.base_cow && !this.base_cow.is_in_his_base() && !this.base_cow.visible)
                target_pos = this.target.initial_pos;
            else
                target_pos = this.target.position;
        }
        else //if Guard
        {
            if(this.carried_flag == null)
                target_pos = this.target.position;
            else 
                target_pos = this.target.initial_pos;

        }

        

        if(this.go_straight) this.follow_target(target_pos); 
        else
        {
            let dist = this.position.subtract(target_pos).magnitude();
            if((dist > 250 && this.state == "Follow tank") || //If its a escolt that follow thank, it move with straight movement if its far from the target
                (this.seeing_flag && this.target.visible && this.target != this.base_cow))
                this.change_to_straight_movement();

            else this.move_in_circles(target_pos);
        }
        
    }

    //Makes the tank move in circles selecting random positions near the target
    move_in_circles(target_pos)
    {
        if(this.target_point_reached())
        {   
            this.path = [];
            this.path_index = 0;
            let random_pos, random_node;
            do
            {
                //Random drection: from -1 to +1 in both axis
                let random_direction = new Vector2 ((Math.random() * -2) +1 , (Math.random() * -2) +1 );

                random_pos = target_pos.add(random_direction.scale(300));
                random_node = grid.from_world_to_node(random_pos.x,random_pos.y);

            } while(random_node == null || !random_node.walkable)
                
            this.pathfinding.find_path(this.position, random_pos);
            this.path = this.pathfinding.path;      
            this.next_pos = this.path[this.path_index+1];
            this.path_index++;
        }
        this.move();
    }

    //#endregion

    //#region Fire and cannon management methods

    fire() { this.weapon.try_fire(); }

     get_cannon_angle() { return  this.cannon_rotation - PI2; }

     get_fire_direction()
     { return new Vector2 ( -Math.sin( this.get_cannon_angle() ), Math.cos( this.get_cannon_angle() )); }
 
     get_fire_point()
     { 
         let fire_dir = this.get_fire_direction();
         let fire_point = this.position.add(fire_dir.scale(45));
         return fire_point;
     }

    get_bullet_rotation() { return this.cannon_rotation  + Math.PI; }

    move_cannon_to_target()
    { this.cannon_rotation  = Math.atan2(this.fire_target.position.y - this.position.y, this.fire_target.position.x - this.position.x) - PIH; }

    //#endregion

    //#region Update and draw methods

    update(delta_time)
    {    
        if(this.alive)
        {

            this.movement_logic();
            this.weapon.update(delta_time);
            if(this.fire_target != undefined)
            {
                this.move_cannon_to_target();
                this.fire();
            }
            this.update_position();
            this.actual_rotation = angleLerp(  this.actual_rotation, this.desired_rotation , 0.03);              
        }
        else if(this.play_exp_anim) this.explosion_animation.Update(delta_time);
    }

    //Adjusts the image position in sync with the body
    update_position()
    {
        let bodyPosition = this.body.GetPosition();
        this.position.set
        (
            bodyPosition.x * scale,
            Math.abs((bodyPosition.y * scale) - canvas_array[0].height)
        );
    }

    draw(ctx)
    {
        if(this.alive)
        {

            ctx.save();
            //x = (x + .5) | 0;


            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);



            ctx.rotate( this.actual_rotation);     
            ctx.drawImage( this.actual_body_canvas, this.pivot.x, this.pivot.y);
            ctx.restore();

            ctx.save();
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.cannon_rotation );
            ctx.drawImage( this.cannon_canvas, this.cannon_pivot.x, this.cannon_pivot.y);
            ctx.restore();
            this.weapon.draw(ctx);

            
            this.health_bar.draw_health_bar(ctx,(this.position.x-50),this.position.y +this.body_canvas.width,100,10,this.health,this.initial_health);
        }
        else if(this.play_exp_anim)
        {
            ctx.save();

            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);

            this.explosion_animation.Draw(ctx);

            console.log("playing anim: " + this.explosion_animation.ended);
            ctx.restore();
        }
    }

    print_path(ctx) { this.pathfinding.print_path(ctx) }


    //#endregion

    //#region Health system

    revive()
    {

        var bullet_transform = 
        {
            position: {x: this.platform_pos.x / scale , y: (canvas_array[0].height - this.platform_pos.y)  / scale},
            GetAngle: function(){return 0;}
        }
        this.body.SetTransform(bullet_transform);
        this.update_position();

        this.alive = true;
        this.health =  this.initial_health;

       
    }

    take_damage(damage, killer_tank)
    {       
        this.health -= damage
        if(this.health <= 0 && this.alive)
        {
            this.die(); 
            if(killer_tank.type == "Tank")
                killer_tank.show_killed_tank_name(this.name,this.team);
            ++killer_tank.kills; 
            killer_tank.add_points(50);
   
        }


    }

    die()
    {
        ++this.deaths;
        this.alive = false;
        this.explode();
      
        game_scene.delete_body(this.body);

        if(this.carried_flag != null)
        {
            this.restart_escort(this.carried_flag); //The escorts that were protecting this tank will go back to their default state
            this.carried_flag.drop(new Vector2(this.position.x, this.position.y));
            this.select_default_image();
        }

        let flag =  this.carried_flag ;
        Tank_Manager.escorting_flag[this.team] =  Tank_Manager.escorting_flag[this.team].filter(function(value, index, arr)
        { return value != flag; }); 


        
        let this_tank = this;
        Tank_Manager.goig_to_flag[this.team].red =  Tank_Manager.goig_to_flag[this.team].red.filter(function(value, index, arr)
        { return value != this_tank; }); 

        Tank_Manager.goig_to_flag[this.team].blue =  Tank_Manager.goig_to_flag[this.team].blue.filter(function(value, index, arr)
        { return value != this_tank; }); 
        Tank_Manager.goig_to_flag[this.team].black =  Tank_Manager.goig_to_flag[this.team].black.filter(function(value, index, arr)
        { return value != this_tank; }); 
        Tank_Manager.goig_to_flag[this.team].green =  Tank_Manager.goig_to_flag[this.team].green.filter(function(value, index, arr)
        { return value != this_tank; }); 

       
        


        this.carried_flag = null;
        this.path = [];
        tank_manager.respawn_tank(this);
        this.going_base = false;
        this.seeing_flag = false;

       


        for(let i = 0 ; i < this.base_cow.cows_inside.length; ++i)
            if(this.base_cow.cows_inside[i].visible)
            {
                this.try_go_for_flag(this.base_cow.cows_inside[i]);
                return;
            }
        this.restart_objetive();
    }



    //#endregion

    //#region Animations

    explode()
    {
        if(this.player_is_near)
            Audio_Manager.instance.play_explosion();
        console.log("explode");
        this.play_exp_anim = true;
   
        this.explosion_animation.play_animation( (function(){ this.play_exp_anim=false; } ).bind(this) );
    }
    //#endregion

    //#region Collsion and trigger methods

    on_collision_enter(other, this_fixture, other_fixture)
    {
        if(this_fixture.GetUserData() != "Sensor" && other_fixture.GetUserData() != "Sensor")
        {   
            if(other.type == "Bullet") this.take_damage(other.damage, other.parent.owner);

            if(other_fixture.GetUserData() != "Sensor")
            {
                if(other.type == "Flag" && other != this.base_cow && other.visible && this.carried_flag == null)
                    this.on_get_enemy_cow(other);
                else if(other == this.base_cow && other.visible && this.carried_flag != null && this.base_cow.is_in_his_base())
                    this.on_bring_enemy_cow_to_base();
                else if(other == this.base_cow && other.visible && !this.base_cow.is_in_his_base())
                    this.on_get_base_cow(); 
            } 
        }
        else if(this_fixture.GetUserData() == "Sensor" && other != this)
            if((other.type == "Tank" || other.type == "IA Tank") && other.team != this.team)
                this.on_enemy_seen(other);      
    }

    on_collision_leave(other, other_fixture, this_fixture)
    {
        if(this_fixture.GetUserData() == "Sensor")
        {
            if(other.type == "IA Tank" || other.type == "Tank")
            {  
                this.posible_targets = this.posible_targets.filter(function(value, index, arr)
                {
                    return value != other;
                }); 
                if(this.fire_target == other)
                    this.on_stop_see_selected_target();


            }
        }
    }

    on_stop_see_selected_target()
    {
         //Just change the target to nother one. Or if its a scolt in "attack mode" and there are no more targets it will go to the escolting tank         
        this.fire_target = this.posible_targets[0];
  
        if(this.state == "Follow tank")
        {
            if(this.fire_target == undefined)   
            {      
                this.target =  this.actual_tank_to_escort;   
            }
        }
    }

    on_get_enemy_cow(cow)
    {
        this.selected_id = this.id;
        Tank_Manager.escorting_flag[this.team].push(cow); //Notify that this team is escorting the cow / flag
        cow.position = this.position;
        cow.be_picked_up();
        //The tank will go back to base carrying the new cow/ flag
       
        this.target = this.base_cow; 
        this.state = "Follow flag";
        this.carried_flag = cow;
        this.going_base = true;

        this.make_escort_protect_this_tank(this.carried_flag); //The escorts that were persuing this cow will protect this tank 
        this.notify_is_no_longer_temporal_escort();
        this.restart_ennemy_scouts_going_for_flag();
        this.seeing_flag = false;
        this.select_image_with_cow();
    }

    notify_is_no_longer_temporal_escort()
    {
        let this_tank = this;
        if(this.team != "red")
        Tank_Manager.temporal_escort[this.team].red = Tank_Manager.temporal_escort[this.team].red.filter(function(value, index, arr)
        { return value != this_tank }); 

        if(this.team != "blue")
        Tank_Manager.temporal_escort[this.team].blue = Tank_Manager.temporal_escort[this.team].blue.filter(function(value, index, arr)
        { return value != this_tank }); 
        
        if(this.team != "black")
        Tank_Manager.temporal_escort[this.team].black = Tank_Manager.temporal_escort[this.team].black.filter(function(value, index, arr)
        { return value != this_tank }); 

        if(this.team != "green")
        Tank_Manager.temporal_escort[this.team].green = Tank_Manager.temporal_escort[this.team].green.filter(function(value, index, arr)
        { return value != this_tank }); 
    }



    //Restart all enemy escolt that are going to this fag, to not follow the tank from another team that has pick up the flag
    restart_ennemy_scouts_going_for_flag()
    {
        let this_tank = this;
        switch(this.carried_flag.color)
        {
            case "red":
                    Tank_Manager.goig_to_flag["blue"].red.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].red = Tank_Manager.goig_to_flag["blue"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].red.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].red = Tank_Manager.goig_to_flag["red"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].red.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].red = Tank_Manager.goig_to_flag["black"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].red.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].red = Tank_Manager.goig_to_flag["green"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 
                    break;
                
            case "blue":  
                    Tank_Manager.goig_to_flag["blue"].blue.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].blue = Tank_Manager.goig_to_flag["blue"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].blue.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].blue = Tank_Manager.goig_to_flag["red"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].blue.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].blue = Tank_Manager.goig_to_flag["black"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].blue.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].blue = Tank_Manager.goig_to_flag["green"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    break;
            case "black":  
                    Tank_Manager.goig_to_flag["blue"].black.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].black = Tank_Manager.goig_to_flag["blue"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].blue.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].black = Tank_Manager.goig_to_flag["red"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].black.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].black = Tank_Manager.goig_to_flag["black"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].black.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].black = Tank_Manager.goig_to_flag["green"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 
            case "green":  
                    Tank_Manager.goig_to_flag["blue"].green.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].green = Tank_Manager.goig_to_flag["blue"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].green.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].green = Tank_Manager.goig_to_flag["red"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].green.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].green = Tank_Manager.goig_to_flag["black"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].green.forEach(element => 
                        { if(element.ia_type != "scout" && element != this_tank) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].green = Tank_Manager.goig_to_flag["green"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 
                    break;
        }
    }
    on_bring_enemy_cow_to_base()
    {
        ++this.cows;
        this.add_points(200);
        //Notify that this team is is no longer escortingthe this cow / flag
        let flag = this.carried_flag;
        Tank_Manager.escorting_flag[this.team] =  Tank_Manager.escorting_flag[this.team].filter(function(value, index, arr)
        { return value != flag; }); 

        this.restart_escort(this.carried_flag); //The escorts that were protecting this tank will go back to their default state

        this.carried_flag.restart(); // The carried flag goes back to his base
        this.restart_objetive();
        this.going_base = false;
        this.select_default_image();
        this.change_puntuation();
        this.carried_flag = null;
    }

    change_puntuation()
    {
        var this_team_score;
        switch(this.team)
        {
            case "red":
                winner_team = "red";
                this_team_score = ++red_puntuation;
                break;  
            case "blue":  
            winner_team = "blue";

                this_team_score = ++blue_puntuation;
                break;
            case "black": 
            winner_team = "black";
 
                this_team_score = ++black_puntuation;
                break;
            case "green":  
            winner_team = "green";

                this_team_score = ++green_puntuation;
                break;
        }
        switch(this.carried_flag.color)
        {
            case "red":
                red_puntuation--;
                break;   
            case "blue":  
              blue_puntuation--;
                break;
            case "black":  
                black_puntuation--;
                break;
            case "green":  
                green_puntuation--;
                break;
        }

        if(this_team_score > 5)
        {
            load_score_scene();
        }
    }
    on_get_base_cow()
    {
        this.base_cow.restart(); //The base return to original position
        if(this.carried_flag == null)  //If the target is carryin a cow his target will remain the base class, but now in his original position
            this.restart_objetive();
        this.restart_tanks_going_rescue_base_flag(); //Allies that were going to get this flag go back to their default state
    }
    
    on_enemy_seen(enemy)
    {
        this.posible_targets.push(enemy);
        if(this.fire_target == undefined)
        {
            this.fire_target = this.posible_targets[0];
            if(this.state == "Follow tank")
                this.target = this.fire_target; 
        }

    }

    //#endregion

    //#region IA types management

    restart_objetive()
    {
        if(this.ia_type == "scout")
            this.select_random_flag();
        
        else if(this.ia_type == "escolt")
        {
            this.state = "Follow tank";
            this.target = this.actual_tank_to_escort = this.main_tank_to_escolt;
            
            if(this.fire_target == undefined)
            {
                this.fire_target = this.posible_targets[0];
                if(this.fire_target != undefined)
                    this.target = this.fire_target;  
            }
            else  this.target = this.fire_target;  
        }
        else //if Guard
        {
            this.target = this.base_cow;

        }
    }

    //The escorts that were protecting the tank that was capturing the enemy cow will go back to protect their original target
    restart_escort(flag)
    {
        switch(flag.color)
        {
            case "red": 
                Tank_Manager.temporal_escort[this.team].red.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });
                Tank_Manager.temporal_escort[this.team].red = [];
                break;
            case "blue":  
                Tank_Manager.temporal_escort[this.team].blue.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });
                Tank_Manager.temporal_escort[this.team].blue = [];
                break;
            case "black":  
                Tank_Manager.temporal_escort[this.team].black.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });        
                Tank_Manager.temporal_escort[this.team].black = [];
                break;
            case "green":  
                Tank_Manager.temporal_escort[this.team].green.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });
                Tank_Manager.temporal_escort[this.team].green = [];
                break;
        }            
    }

    //Allies that were going to pick up the base flag go back to their default state
    restart_tanks_going_rescue_base_flag()
    {
        switch(this.base_cow.color)
        {
            case "red": 
                Tank_Manager.goig_to_flag[this.team].red.forEach(element => 
                    { if(element != this && element.carried_flag == null) { element.restart_objetive(); }});
                Tank_Manager.goig_to_flag[this.team].red = [];
                break;
            case "blue":  
                Tank_Manager.goig_to_flag[this.team].blue.forEach(element => 
                    { if(element != this && element.carried_flag == null) { element.restart_objetive(); }});
                Tank_Manager.goig_to_flag[this.team].blue = [];
                break;
            case "black":  
                Tank_Manager.goig_to_flag[this.team].black.forEach(element =>
                    { if(element != this && element.carried_flag == null) { element.restart_objetive();}});
                Tank_Manager.goig_to_flag[this.team].black = [];
                break;
            case "green":  
                Tank_Manager.goig_to_flag[this.team].green.forEach(element =>
                    { if(element != this&& element.carried_flag == null) { element.restart_objetive(); }});
                Tank_Manager.goig_to_flag[this.team].green = [];
                break;
        }     
    }

    //The escorts that are going to get a flag change their protection objetive to the tank who has caught the cow. 
    //(Used when someone get the cow that they are trying to get)
   make_escort_protect_this_tank(flag_target)
   {
       let this_tank = this;
        switch(flag_target.color)
        {
            case "red": 
                Tank_Manager.goig_to_flag[this_tank.team].red.forEach(element => {
                    if(element != this_tank)
                    { 
                        if(element.ia_type == "escolt")
                        {
                            element.state = "Follow tank";
                            element.actual_tank_to_escort = this_tank;
                            if(element.fire_target != undefined)
                                element.target = element.fire_target;
                            Tank_Manager.temporal_escort[this_tank.team].red.push(element);
                        }
                        else // guards
                            element.restart_objetive();
                        
                    }});
                Tank_Manager.goig_to_flag[this.team].red = [];
                break;
            case "blue":  
                Tank_Manager.goig_to_flag[this.team].blue.forEach(element => {
                    if(element != this_tank)
                    { 
                        if(element.ia_type == "escolt")
                        {
                            element.state = "Follow tank";

                            element.actual_tank_to_escort = this_tank;
                            if(element.fire_target != undefined)
                                element.target = element.fire_target;
                            Tank_Manager.temporal_escort[this_tank.team].blue.push(element);
                        }
                        else // guards
                        {                            
                            element.restart_objetive();
                        }
                    }
                });
                Tank_Manager.goig_to_flag[this_tank.team].blue = [];
                break;
            case "black":  
                Tank_Manager.goig_to_flag[this_tank.team].black.forEach(element => {
                    if(element != this_tank)
                    { 
                        if(element.ia_type == "escolt")
                        {
                            element.state = "Follow tank";

                            element.actual_tank_to_escort = this_tank;
                            if(element.fire_target != undefined)
                                element.target = element.fire_target;
                            Tank_Manager.temporal_escort[this_tank.team].black.push(element);
                        }
                        else // guards
                        {                           
                            element.restart_objetive();
                        }
                    }
                });        
                Tank_Manager.goig_to_flag[this_tank.team].black = [];
                break;
            case "green":  
                Tank_Manager.goig_to_flag[this_tank.team].green.forEach(element => {
                    if(element != this_tank)
                    { 
                        if(element.ia_type == "escolt")
                        {
                            element.state = "Follow tank";

                            element.actual_tank_to_escort = this;
                            if(element.fire_target != undefined)
                                element.target = element.fire_target;
                            Tank_Manager.temporal_escort[this_tank.team].green.push(element);
                        }
                        else // guards
                        {                            
                            element.restart_objetive();
                        }
                    }
                    
                });
                    Tank_Manager.goig_to_flag[this_tank.team].green = [];
                break;
        }  
   }

    try_go_for_flag(flag)
    {
        this.seeing_flag = true;

        if(this.ia_type == "escolt")
        {
            if(this.state == "Follow tank")
                this.change_state_to_follow_flag(flag);
        }

        else if(this.ia_type == "scout")
        {
            if(this.carried_flag == null)
                this.target = flag;
            
        }
        else //if guard
        {
            this.change_state_to_follow_flag(flag);
        }
    }   

    go_for_base_flag()
    {
        this.state = "Follow flag";
        this.target = this.base_cow;

        this.seeing_flag = true;
        switch(this.team)
        {
             case "red":
                Tank_Manager.goig_to_flag[this.team].red.push(this);
                break;
            case "blue":
                Tank_Manager.goig_to_flag[this.team].blue.push(this);
                break;
            case "black":
                Tank_Manager.goig_to_flag[this.team].black.push(this);
                break;
            case "green":
                Tank_Manager.goig_to_flag[this.team].green.push(this);
                break; 
        }
    }

    change_state_to_follow_flag(flag)
    {
        switch(flag.color)
        { 
            case "red":
                Tank_Manager.goig_to_flag[this.team].red.push(this);
                break;
            case "blue":
                Tank_Manager.goig_to_flag[this.team].blue.push(this);
                break;
            case "black":
                Tank_Manager.goig_to_flag[this.team].black.push(this);
                break;
            case "green":
                Tank_Manager.goig_to_flag[this.team].green.push(this);
                break;
        }

        this.state = "Follow flag";
        this.target = flag;
    }

  //#endregion

    add_points(points)
    {   
        this.points += points;
        tank_manager.sort_stadistics();
    }

}