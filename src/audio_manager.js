class Audio_Manager
{
    constructor()
    {
        this.menu_background = document.getElementById("menu_background");
        this.game_background = document.getElementById("game_background");
        this.tank_movement = document.getElementById("tank_movement");
        this.shoot = document.getElementById("shoot");
   
    }

    play_menu_background()
    {
        this.menu_background.muted = false;
        this.menu_background.volume = 0.1;
        this.menu_background.play();
    }

    stop_menu_background()
    {
        this.menu_background.pause();
        this.menu_background.currentTime = 0;
    }


    play_game_background()
    {
       this.game_background.muted = false;
       this.game_background.volume = 0.05;

        this.game_background.play();
    }

    stop_game_background()
    {
        this.game_background.pause();
        this.game_background.currentTime = 0;
    }

    play_tank_movement()
    {
        this.tank_movement.volume = 0.1;
        this.tank_movement.muted = false;
        this.tank_movement.play();
    }


    play_machine_shoot()
    {
        let audio = new Audio("./assets/souds/machine_shoot.mp3");
        audio.volume=0.04;
        audio.muted = false;
        audio.play();
    }

    play_rifle_shoot()
    {
        let audio = new Audio("./assets/souds/rifle_shoot.mp3");
        audio.volume=0.2;
        audio.muted = false;
        audio.play();
    }

    play_laser_shoot()
    {
        let audio = new Audio("./assets/souds/laser_shoot.mp3");
        audio.volume= 0.3;
        audio.muted = false;
        audio.play();
    }
    play_missile_shoot()
    {
        let audio = new Audio("./assets/souds/missile_shoot.mp3");
        audio.volume=0.5;
        audio.muted = false;
        audio.play();
    }
    play_rocket_shoot()
    {
        let audio = new Audio("./assets/souds/rocket_shoot.mp3");
        audio.volume=0.1;
        audio.muted = false;
        audio.play();
    }

    play_granade_shoot()
    {
        let audio = new Audio("./assets/souds/granade_shoot.mp3");
        audio.volume=0.1;
        audio.muted = false;
        audio.play();
    }

    play_explosion()
    {
        let audio = new Audio("./assets/souds/tank_explosion.mp3");
        audio.volume=0.4;
        audio.muted = false;
        audio.play();
    }
}