//Tank that can move, rotate, pick up different weapons and items, fire and be destroyed when hit by bullets
class Tank
{
    //#region Constructor

    constructor(initialPosition,id, team)
    {
        this.type = "Tank";
        this.name = "Player"
        this.ia_type = "scout";
        this.initialPosition = initialPosition;
        this.platform_pos = initialPosition;
       // this.move_speed = 20;
        this.rotation_speed = 5;
    
        this.team = team;
        this.killer_tank;
        this.kills = 0;
        this.deaths = 0;
        this.points = 0;
        this.cows = 0;
        this.position = new Vector2(initialPosition.x, initialPosition.y);
        this.rotation = 0;
        this.actual_rot = 0;
        this.camera = new Camera(this);
        this.camera_target_index = 0;
        //Vector to which the tank it is moving, it´s in accordance with the rotation
        this.displacement_vector = new Vector2(0, 0);

        this.click_to_respawn = false;
        //Final velocity vector (including the displacement_vector, the speed and the player input)
        this.velocity = { x: 0, y: 0 };
   
        this.health_bar = new Health_Bar();
        this.time_to_respawn = 0;
        if(this.ia_type == "scout" )
        {
            this.initial_health = 70;

            this.move_speed = 15;
            //Physics properties of the tank body
            this.physicsOptions = 
            {
                linearDamping : 5,
                angularDamping: 5,
                friction: 0,
                restitution : 0,
                density: 0, //makes the body not rotate with the hits
                type: b2Body.b2_dynamicBody,
                user_data: this,
            }
        }
        else if (this.ia_type == "escolt")
        {
            this.initial_health = 100;

            this.move_speed = 10;
            //Physics properties of the tank body
            this.physicsOptions = 
            {
                linearDamping: 5,
                angularDamping: 5,
                friction: 0,
                restitution : 0,
                density: 0, //makes the body not rotate with the hits
                type: b2Body.b2_dynamicBody,
                user_data: this,
            };
        }
        else
        {
            this.initial_health = 200;
            this.move_speed = 15;
            //Physics properties of the tank body
            this.physicsOptions = 
            {
                linearDamping : 10,
                angularDamping: 10,
                friction: 0,
                restitution : 0,
                density: 0, //makes the body not rotate with the hits
                type: b2Body.b2_dynamicBody,
                user_data: this,
            }
        }
        this.health =  this.initial_health;

        //Body that acts like collider and rigidbody
        this.body = CreateBall(this.position.x / scale, (canvas_array[0].height - this.position.y)  / scale ,
            0.2, this.physicsOptions, collision_rule.TANK);


        this.body.GetFixtureList().SetUserData(this);
        this.create_sensor();


        this.weapons = {};
        this.weapons["Machine gun"] = new MachineGun(this);
        this.weapons["Missile"] = new Missile(this);
        this.weapons["Laser"] = new Laser(this);
        this.weapons["AutoMissile"] = new AutoMissile(this);
        this.weapons["Rifle"] = new Rifle(this);
        this.weapons["Granade"] = new GranadeLauncher(this);


        //Current weapon of the tank
        this.weapon = this.weapons["Machine gun"];
        
        //Weapons that arent the current weapon but whose bullets are still in game, so they need call update and draw
        this.active_weapons = [];

        //All the available weapons
        this.weapon_store = ["Machine gun"];

        //Index to select weapon from weapon_store
        this.weapon_index = 0;

        //Can this tank be controlled with input ?
        this.is_controlling = true;
        
        //Weapon change cooldown relate variables
        this.weapon_change_elapsed_time = 0;
        this.weapon_change_cd = 0.5;
        this.weapon_change_is_on_cd = false;




        //Fire cooldown relate variables
        this.fire_elapsed_time = 0;
        this.fire_cd = 0.05;
        this.fire_is_on_cd = false;


        this.change_target_elapsed = 0;
        this.change_target_cd = 0.2;
        this.change_target_is_on_cd = false;

        this.show_killer_name= false;
        this.show_killer_name_time = 3;
        this.show_killer_elapsed_time = 0;



        this.show_killed_tank= false;
        this.show_killed_name_time = 3;
        this.killed_tank_elapsed_time = 0;



        this.camera_number = id;

        this.select_controls();

        this.id = id;


        this.alive = true;

        this.asign_base_flag();
        this.carried_flag = null;

        this.cannon_rotation = 0;



        this.add_weapon("Machine gun");
        this.add_weapon("Missile");
        this.add_weapon("Laser");
        this.add_weapon("AutoMissile");
        this.add_weapon("Rifle");
        this.add_weapon("Granade");

        this.configure_images();

        this.width = 50;
        this.height = 60;
        this.cannon_width = 15;
        this.cannon_height = 35;
        this.pivot = { x: - this.width  >> 1, y: - this.height >> 1 };
        this.cannon_pivot = { x: -this.cannon_width >> 1, y: this.cannon_height - this.cannon_height >> 1  };
    }

  
    //Create a sensor to detect sounds
    create_sensor()
    {

        var fix_def = new b2FixtureDef();
 
        // Shape: 2d geometry (circle or polygon)
        fix_def.shape = new b2PolygonShape();
     
        fix_def.shape.SetAsBox((canvas_array[0].width/2) / scale, (canvas_array[0].height - canvas_array[0].height/2) / scale);

        fix_def.filter.categoryBits = collision_rule.EAR_SENSOR.collision;
        fix_def.filter.maskBits = collision_rule.EAR_SENSOR.layer;
    
        this.ear_sensor = this.body.CreateFixture(fix_def);
        this.ear_sensor.SetUserData("Sensor");
        this.ear_sensor.SetSensor(true);  

    }


    add_points(points)
    {
        this.points+=points;
        tank_manager.sort_stadistics();
    }

    configure_images()
    {
        switch(this.team)
        {
            case "red":
                    switch(this.ia_type)
                    {
                        case "scout":
                            this.body_image = graphics.red_scout_body.image;
                            this.body_cow_image = graphics.red_scout_body_cow.image;
                            this.actual_cannon_image = graphics.red_scout_cannon.image;
                            break;

                        case "escolt":
                            this.body_image = graphics.red_escolt_body.image;
                            this.body_cow_image = graphics.red_escolt_body_cow.image;
                            this.actual_cannon_image = graphics.red_escolt_cannon.image;
                            break;
                        case "protector":
                
                            break;
                    }

                break;
            case "blue":
                switch(this.ia_type)
                {
                    case "scout":
                        this.body_image = graphics.blue_scout_body.image;
                        this.body_cow_image = graphics.blue_scout_body_cow.image;
                        this.actual_cannon_image = graphics.blue_scout_cannon.image;
                        break;

                    case "escolt":
                        this.body_image = graphics.blue_escolt_body.image;
                        this.body_cow_image = graphics.blue_escolt_body_cow.image;
                        this.actual_cannon_image = graphics.blue_escolt_cannon.image;
                        break;
                    case "protector":
                
                        break;
                }
                break;
            case "black":
                switch(this.ia_type)
                {
                    case "scout":
                        this.body_image = graphics.black_scout_body.image;
                        this.body_cow_image = graphics.black_scout_body_cow.image;
                        this.actual_cannon_image = graphics.black_scout_cannon.image;
                        break;

                    case "escolt":
                        this.body_image = graphics.black_escolt_body.image;
                        this.body_cow_image = graphics.black_escolt_body_cow.image;
                        this.actual_cannon_image = graphics.black_escolt_cannon.image;
                        break;
                    case "protector":
            
                        break;
                }
                break;
            case "green":
                switch(this.ia_type)
                {
                    case "scout":
                        this.body_image = graphics.green_scout_body.image;
                        this.body_cow_image = graphics.green_scout_body_cow.image;
                        this.actual_cannon_image = graphics.green_scout_cannon.image;
                        break;

                    case "escolt":
                        this.body_image = graphics.green_escolt_body.image;
                        this.body_cow_image = graphics.green_escolt_body_cow.image;
                        this.actual_cannon_image = graphics.green_escolt_cannon.image;
                        break;
                    case "protector":
            
                        break;
                }
                break;
        }
        this.actual_body_image = this.body_image;
    }

    select_image_with_cow() { this.actual_body_image = this.body_cow_image; }
    select_default_image() { this.actual_body_image = this.body_image; }
    


    //#endregion

    asign_base_flag()
    { 
        this.base_flag = this.team == "red" ?  red_cow : this.team == "green" ? green_cow: this.team == "black" ? black_cow: blue_cow;
    }

    take_damage(damage, killer_tank)
    {
        this.health -= damage
        if(this.health <= 0 && this.alive )
        {
            this.camera_target_index = killer_tank.id;
            this.killer_tank = killer_tank.name;
            this.set_camera_target(killer_tank);
            this.die();
            ++killer_tank.kills; 
            killer_tank.add_points(50);
        }
    }

    die()
    {
        ++this.deaths;
        this.show_killer_name = true;
        //this.camera.change_target(this.base_flag);
        if(this.carried_flag != null)
        {
            this.restart_escort(this.carried_flag); //The escorts that were protecting this tank will go back to their default state
            this.carried_flag.drop(new Vector2(this.position.x, this.position.y));
            this.select_default_image();
        }

        game_scene.delete_body(this.body);

        this.alive = false;
        this.carried_flag = null;
        tank_manager.respawn_tank(this);

        this.change_target_is_on_cd = false;
    }

    death_controls()
    {
        if(this.right_input() || (!this.keyboard_control && gamepads[this.pad_index].axes[0] > 0))
        {
            this.change_target_is_on_cd = true;
            if(++this.camera_target_index > tank_manager.tank_amount)
                this.camera_target_index = 0;
                console.log("looking camera indeX:" + this.camera_target_index);

            tanks.forEach(element => {
                if(element.id == this.camera_target_index)
                    this.set_camera_target(element);
                
            });
        }

        if(this.left_input() || ( !this.keyboard_control && gamepads[this.pad_index].axes[0] < 0))
        {
            this.change_target_is_on_cd = true;
            if(--this.camera_target_index < 0 )
                this.camera_target_index = tank_manager.tank_amount;;
            tanks.forEach(element => {
                if(element.id == this.camera_target_index)
                    this.set_camera_target(element);
            });
        }
    }


    show_killed_tank_name(name, color)
    {
        this.killed_tank = name;
        this.killed_tank_color = color
        this.show_killed_tank = true;
    }




    display_killed_tank_text()
    {
        ctx_array[this.id].fillStyle = this.killed_tank_color;
        ctx_array[this.id].font = "40px Comic Sans MS";
        if(this.killed_tank_color == this.team)
            ctx_array[this.id].fillText('YOU ARE A TRAITOR, YOU KILLED ' + this.killed_tank, this.camera.target_pos.x + canvas_array[0].width/2 - (15*20)/2, this.camera.target_pos.y + canvas_array[0].height/2 );      
        else
        ctx_array[this.id].fillText('YOU KILLED ' + this.killed_tank, this.camera.target_pos.x + canvas_array[0].width/2 - (15*20)/2, this.camera.target_pos.y + canvas_array[0].height/2 );      

    }
    display_die_counter()
    {
        ctx_array[this.id].fillStyle = "white";
        ctx_array[this.id].font = "50px Comic Sans MS";
        ctx_array[this.id].fillText('TIME TO RESPAWN: ' + Math.round(tank_manager.time_to_respawn - this.time_to_respawn), this.camera.target_pos.x + canvas_array[0].width/2 - (11*50)/2, this.camera.target_pos.y + canvas_array[0].height/2 + canvas_array[0].height/3  );
        
        ctx_array[this.id].fillText('SPECTATING: ' + this.camera.target.name, this.camera.target_pos.x + canvas_array[0].width/2 - (11*50)/2, this.camera.target_pos.y + canvas_array[0].height/2 + canvas_array[0].height/4  );

        ctx_array[this.id].font = "20px Comic Sans MS";
        ctx_array[this.id].fillText("<< A" , this.camera.target_pos.x + canvas_array[0].width/2  - canvas_array[0].width/2.3 , this.camera.target_pos.y +canvas_array[0].height/3    );
        ctx_array[this.id].fillText("D >>" , this.camera.target_pos.x + canvas_array[0].width/2 + canvas_array[0].width/3, this.camera.target_pos.y + canvas_array[0].height/3   );
    }


    display_killer_name()
    {
        ctx_array[this.id].fillStyle = "red";
        ctx_array[this.id].font = "20px Comic Sans MS";
        ctx_array[this.id].fillText('YOU HAS BEEN KILLED BY ' + this.killer_tank, this.camera.target_pos.x + canvas_array[0].width/2 - (15*20)/2, this.camera.target_pos.y + canvas_array[0].height/2 );      
    }

    enable_respawn()
    {
        this.click_to_respawn = true;
    }

    respawn_input()
    {
        if(this.fire_input())
        {
            this.click_to_respawn = false;
            this.revive();
        }
    }

    display_respawn_text()
    {
        ctx_array[this.id].fillStyle = "white";
        ctx_array[this.id].font = "50px Comic Sans MS";
        ctx_array[this.id].fillText('SPECTATING: ' + this.camera.target.name, this.camera.target_pos.x + canvas_array[0].width/2 - (11*50)/2, this.camera.target_pos.y + canvas_array[0].height/2 + canvas_array[0].height/4  );

        ctx_array[this.id].fillText("Press fire to respawn" , this.camera.target_pos.x + canvas_array[0].width/2 - (11*50)/2, this.camera.target_pos.y + canvas_array[0].height/2 + canvas_array[0].height/3   );
        ctx_array[this.id].font = "20px Comic Sans MS";
        ctx_array[this.id].fillText("<< Previous" , this.camera.target_pos.x + canvas_array[0].width/2  - canvas_array[0].width/2.3 , this.camera.target_pos.y +canvas_array[0].height/3    );
        ctx_array[this.id].fillText("Next >>" , this.camera.target_pos.x + canvas_array[0].width/2 + canvas_array[0].width/3, this.camera.target_pos.y + canvas_array[0].height/3   );
    }


    revive()
    {
        this.camera.change_target(this);

        var bullet_transform = 
        {
            position: {x: this.platform_pos.x / scale , y: (canvas_array[0].height - this.platform_pos.y)  / scale},
            GetAngle: function(){return 0;}
        }
        this.body.SetTransform(bullet_transform);

        //Make the image of the tank follow the body
        let bodyPosition = this.body.GetPosition();
        this.position.x = bodyPosition.x * scale;
        this.position.y = Math.abs((bodyPosition.y * scale) - canvas_array[0].height) ;

        this.alive = true;
        this.health = this.initial_health;
    }


    get_cannon_angle()
    {
        return  this.cannon_rotation ;
    }
    
    get_fire_direction()
    {
        return new Vector2
        (
            -Math.sin( this.get_cannon_angle() ),
            Math.cos( this.get_cannon_angle() )
        ); 
    }

    get_bullet_rotation()
    {
        return this.cannon_rotation + Math.PI;
    }

    get_fire_point()
    { 
        let fire_dir = this.get_fire_direction();
        
        let fire_point = this.position.add(fire_dir.scale(40));

        return fire_point;
    }

    select_controls()
    {
        if(this.camera_number == 1)
            this.controls = first_player_control;
        else if(this.camera_number == 0)
            this.controls = second_player_control;
        
        if(!(this.keyboard_control = this.controls.is_keyboard))
            this.pad_index = actual_pad_index++;
    }

    set_camera_target(new_target)
    {
        this.camera.change_target(new_target);
    }

    //#region Movement methods

    manage_tank_movement(deltaTime)
    {
        if(!this.is_controlling) return;

        this.rotate(deltaTime);
        this.move();
    }

    move()
    {
        //The vector of the direction of movement is rotated based on the angle of rotation, so that the tank moves in the direction to which it is rotated
        //Formula of vector rotation: x = sinα * L || y = cosα * L
        this.displacement_vector.x = Math.sin( this.rotation );
        this.displacement_vector.y = Math.cos( this.rotation );

        let displacement_input = 0; //0 stop, 1 forward, -1 backward

        let move = false;
        if( this.up_input())
        {
            move = true;
            displacement_input = 1;
        }

        if(this.down_input())
        {
            move = true;
            displacement_input = -1;
        }

        if(!this.keyboard_control && gamepads[this.pad_index] != undefined)
            {

                let direction = new Vector2(gamepads[this.pad_index].axes[0], gamepads[this.pad_index].axes[1]).normalize();
                if(direction.y != 0 || direction.x != 0)
                {
                    Audio_Manager.instance.play_tank_movement();

                    this.displacement_vector.set(direction.x * 15,-direction.y * 15);

                    let angle = Math.atan2( this.displacement_vector.y,  -this.displacement_vector.x);
                    this.rotation = angle - Math.PI/2;

                    this.body.ApplyForce( this.displacement_vector , this.body.GetWorldCenter());
                }
   
            }

     
        if(move)
        {    
            //
            //Apply the actual movement of the body    
            this.velocity.x =  this.displacement_vector.x * this.move_speed * displacement_input;
            this.velocity.y =  this.displacement_vector.y * this.move_speed  *displacement_input;

            this.body.ApplyForce(this.velocity, this.body.GetWorldCenter());
            //this.ApplyVelocity(this.velocity);
        }
    

        
      
    

        //Make the image of the tank follow the body
        let bodyPosition = this.body.GetPosition();
        this.position.x = bodyPosition.x * scale;
        this.position.y = Math.abs((bodyPosition.y * scale) - canvas_array[0].height) ;
    }

    rotate(deltaTime)
    {
        let rotation_input = 0;
        if(this.left_input())
            rotation_input = -1;
        if(this.right_input())
            rotation_input = 1;



        
        this.rotation += rotation_input * this.rotation_speed * deltaTime;
    }

    ApplyVelocity(vel)
    {
        this.body.SetLinearVelocity(vel);
    }

    disable_control()
    {
        this.stop_movement();
        this.is_controlling = false;
    }
    
    enable_control()
    {
        this.is_controlling = true;
    }

    stop_movement()
    {
        this.velocity = new b2Vec2(0,0);
        this.body.SetLinearVelocity(this.velocity);
    }
    //#endregion

    up_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.up_move)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.up_move, this.pad_index))); }
    right_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.right_move)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.right_move, this.pad_index))); }
    left_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.left_move)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.left_move, this.pad_index))); }
    down_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.bottom_move)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.bottom_move, this.pad_index))) }
    fire_input()
    { return((this.keyboard_control && (Input.IsKeyPressed(this.controls.fire) || Input.mouse.pressed ))|| (!this.keyboard_control && (Input.IsButtonPressed(this.controls.fire, this.pad_index) || Input.IsButtonPressed(this.controls.fire2, this.pad_index)))); }
    weapon_change_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.weapon_change)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.weapon_change, this.pad_index))); }
    left_cannon_rotation_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.left_cannon_rotation)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.left_cannon_rotation, this.pad_index))); }
    right_cannon_rotation_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.right_cannon_rotation)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.right_cannon_rotation, this.pad_index))); }
    show_stats_input()
    { return((this.keyboard_control && Input.IsKeyPressed(this.controls.show_stats)) || (!this.keyboard_control && Input.IsButtonPressed(this.controls.show_stats, this.pad_index))); }
  
    //#region Fire methods

    manage_fire()
    {
        if(!this.fire_is_on_cd)
            if(this.fire_input())
            {
                this.weapon.try_fire();
                this.fire_is_on_cd = true;
            }
    }

    add_weapon(weapon_name)
    { 
        if(!this.weapons[weapon_name] === "undefined")
        {
            console.log("already have this weapon: ");
            return;
        }

       switch(weapon_name)
        {
            case "Missile":
                this.weapon_store.push("Missile");
                break;
            case "Laser":
                this.weapon_store.push("Laser");
                break;
            case "Rifle":
                this.weapon_store.push("Rifle");
                break;
            case "AutoMissile":
                this.weapon_store.push("AutoMissile");
                break;
            case "Granade":
                this.weapon_store.push("Granade");
                break;
         
        }  
  
    }

    manage_weapon_change(delta_time)
    {     
        if(!this.weapon_change_is_on_cd)
            if(this.weapon_change_input())
            {
                this.change_weapon();
                this.weapon_change_is_on_cd = true;
            }
    }

    

    change_weapon()
    {
        //Move index
        if(this.weapon_index++ == this.weapon_store.length - 1)
            this.weapon_index = 0;
         
        let new_weapon = this.weapon_store[this.weapon_index];

        //Change the weapon only if the previous and the new weapon arent the same
        if(this.weapon.type != new_weapon)
        {
            //If the previous weapon was missile, the control is returned to the tank
            if(this.weapon.type == "Missile")
            {
                this.enable_control();
                this.weapon.disable_control();
            }
            //If the previous weapon was lasser, stop draw the laser path
            if(this.weapon.type == "Laser")
            {
                this.weapon.disable_drawing_laser();
                this.camera.zoom_in();
            }
            
            if(new_weapon == "Missile")
                this.weapons[new_weapon].reset();
            
           
            if(this.weapon.is_active())
                if(!isInArray(this.weapon, this.active_weapons));       
                    this.active_weapons.push(this.weapon);

            this.weapon = this.weapons[new_weapon];

            if(this.weapon.type == "Laser")
            {
                this.weapon.enable_drawing_laser();
                this.camera.zoom_out();
            }
            
            for(let i = 0; i < this.active_weapons.length; ++i)
                if(this.active_weapons[i] == this.weapon)
                    this.active_weapons.splice(i,1);     
        }
    }

    filter_old_weapons()
    { this.active_weapons = this.active_weapons.filter(function(value, index, arr){ return value.is_active();}); }

    //#endregion


    rotate_cannon(delta_time)
    {
        /*if(this.left_cannon_rotation_input()) 
            this.cannon_rotation += 5 * delta_time;
        if(this.right_cannon_rotation_input())
            this.cannon_rotation -= 5 * delta_time;*/
        if(this.keyboard_control)
        {
            let direction = canvas_center.subtract(Input.mouse);
            this.cannon_rotation =  Math.atan2(direction.y,direction.x) + PIH;
        }
        else
            if(gamepads[this.pad_index] != undefined)
            {
                let direction = new Vector2(gamepads[this.pad_index].axes[2], gamepads[this.pad_index].axes[3]);

               
                if(direction.y != 0 || direction.x != 0)
                {
                    this.cannon_rotation =  Math.atan2(direction.y,direction.x) - PIH;
                }
            }       
    }

    //#region Update and draw methods

    update(delta_time)
    {
        this.camera.update(delta_time);

        if(this.alive)
        {
            //this.laser.update(delta_time);
            this.manage_tank_movement(delta_time); //Displacement and rotation
        
            this.manage_fire();

            this.update_input_cd(delta_time);

            this.manage_weapon_change(delta_time);
        
            this.update_weapons(delta_time);

            this.rotate_cannon(delta_time);

            this.actual_rot = angleLerp(  this.actual_rot, this.rotation ,0.07);

            if(this.show_killed_tank)
            {
                this.killed_tank_elapsed_time += delta_time;
                if(this.killed_tank_elapsed_time > this.show_killed_name_time)
                {
                    this.killed_tank_elapsed_time = false;
                    this.show_killed_tank = false;
                }
            }
        }
        else
        {
            this.show_killer_elapsed_time += delta_time;
            if(this.show_killer_elapsed_time > this.show_killer_name_time)
            {
                this.show_killer_name = false;
                this.show_killer_elapsed_time = 0;

            }

            if(this.change_target_is_on_cd)
            {
                this.change_target_elapsed += delta_time;
                if(this.change_target_elapsed > this.change_target_cd)
                {        
                    this.change_target_is_on_cd = false;
                    this.change_target_elapsed = 0;
                }
            }
            else
                this.death_controls();
            
          
        }
    }

    update_weapons(delta_time)
    {
        //Update actual weapon class
        
        this.weapon.update(delta_time);

        //Delete the old weapons whose bullets ended his trayectory
        this.filter_old_weapons();

        //Update the old weapons whose bullet are still active
        this.active_weapons.forEach(element => { element.update(delta_time); });
    }

    update_input_cd(delta_time)
    {
        if(this.weapon_change_is_on_cd)
        {
            this.weapon_change_elapsed_time += delta_time;

            if(this.weapon_change_elapsed_time > this.weapon_change_cd)
            {
                this.weapon_change_is_on_cd = false;
                this.weapon_change_elapsed_time = 0;
            }
        }

        if(this.fire_is_on_cd)
        {
            this.fire_elapsed_time += delta_time;

            if(this.fire_elapsed_time > this.fire_cd)
            {
                this.fire_is_on_cd = false;
                this.fire_elapsed_time = 0;
            }
        }
    }

    pre_draw(ctx) { this.camera.pre_draw(ctx); }

    draw(ctx)
    {
  
        /*if(this.keyboard_control)
        console.log("pos: " + this.position);*/
        if(this.alive)
        {
            //this.laser.draw(ctx);
            

            this.active_weapons.forEach(element => { element.draw(ctx); });

            // draw the enemy ship spritew
            ctx.save();
    
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.actual_rot);

            //ctx.drawImage(this.image, 64, 128, 64, 64, -this.pivot.x, -this.pivot.y, 64, 64);
            ctx.drawImage(this.actual_body_image, this.pivot.x, this.pivot.y , this.width, this.height);  

            ctx.restore();

            ctx.save();


            ctx.translate(this.position.x , this.position.y );

            
            ctx.rotate(this.cannon_rotation);

            ctx.drawImage( this.actual_cannon_image, this.cannon_pivot.x, this.cannon_pivot.y,this.cannon_width,this.cannon_height );
            ctx.restore();

            this.weapon.draw(ctx);


            this.health_bar.draw_health_bar(ctx,(this.position.x-50),this.position.y+this.height,100,10,this.health,this.initial_health);


           
            if(this.show_killed_tank)
                this.display_killed_tank_text();
            

        }
        else
        {
            if(this.click_to_respawn)
            {
                this.display_respawn_text();
                this.respawn_input();
            }
            else
            {
              
                if(this.show_killer_name)
                    this.display_killer_name();

                
               
                this.display_die_counter(ctx);
            }
        } 
    }

    post_draw(ctx) { this.camera.post_draw(ctx); }

    on_collision_enter(other, this_fixture,other_fixture)
    {
        if(this_fixture != this.ear_sensor)
        {
            if(other_fixture.GetUserData() != "Sensor")
            {
                if(other.type == "Bullet")
                    this.take_damage(other.damage,other.parent.owner);

                if(other.type == "Flag" && other != this.base_flag && this.carried_flag == null)
                this.on_get_enemy_cow(other);
                
                else if(other == this.base_flag && this.carried_flag != null && this.base_flag.is_in_his_base())
                    this.on_bring_enemy_cow_to_base();
                
                else if(other == this.base_flag && other.visible && !this.base_flag.is_in_his_base())
                    this.on_get_base_cow(other);
                
            }
        }
        else
        {
            if(other.type == "IA Tank")
            {
                other.player_is_near = true;
            }
        }
    }

    on_collision_leave(other, other_fixture, this_fixture)
    {
        if(this_fixture == this.ear_sensor)
            if(other.type == "IA Tank")
                {
                    other.player_is_near = false;
                }
    }

    //#endregion

    on_get_base_cow(other)
    {
        other.restart();
        this.restart_tanks_going_rescue_base_flag();
    }

    on_bring_enemy_cow_to_base()
    {
        ++this.cows;
        this.add_points(200);
        let flag = this.carried_flag;
        Tank_Manager.escorting_flag[this.team] =  Tank_Manager.escorting_flag[this.team].filter(function(value, index, arr)
        {
            return value != flag;
        }); 

        this.restart_escort(this.carried_flag); //The escorts that were protecting this tank will go back to their default state
        this.change_puntuation();
        this.carried_flag.restart();
        this.carried_flag = null;
        this.select_default_image();
    }

    change_puntuation()
    {
        var this_team_score;
        switch(this.team)
        {
            case "red":
                winner_team = "red";

                this_team_score = ++red_puntuation;
                break;     
            case "blue":  
            winner_team = "blue";

            this_team_score = ++blue_puntuation;
                break;
            case "black": 
            winner_team = "black";
 
            this_team_score = ++black_puntuation;
                break;
            case "green":  
            winner_team = "green";

            this_team_score = ++green_puntuation;
                break;
        }
        switch(this.carried_flag.color)
        {
            case "red":
                red_puntuation--;
                break;
            case "blue":  
              blue_puntuation--;
                break;
            case "black":  
                black_puntuation--;
                break;
            case "green":  
                green_puntuation--;
                break;
        }

        if(this_team_score > 5)
        {
            load_score_scene();
        }
    }
    on_get_enemy_cow(cow)
    {

        Tank_Manager.escorting_flag[this.team].push(cow);

        cow.position = this.position;
        cow.be_picked_up();
        
        this.carried_flag = cow;



        this.make_escort_protect_this_tank(this.carried_flag); //The escorts that were persuing this cow will protect this tank 

        this.restart_ennemy_scouts_going_for_flag();
        this.select_image_with_cow();

    }

    restart_escort(flag)
    {
        switch(flag.color)
        {
            case "red": 
                Tank_Manager.temporal_escort[this.team].red.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });
                Tank_Manager.temporal_escort[this.team].red = [];
                break;
            case "blue":  
                Tank_Manager.temporal_escort[this.team].blue.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });
                Tank_Manager.temporal_escort[this.team].blue = [];
                break;
            case "black":  
                Tank_Manager.temporal_escort[this.team].black.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });        
                Tank_Manager.temporal_escort[this.team].black = [];
                break;
            case "green":  
                Tank_Manager.temporal_escort[this.team].green.forEach(element => { if(element.carried_flag == null) element.restart_objetive(); });
                Tank_Manager.temporal_escort[this.team].green = [];
                break;
        }            
    }

    restart_ennemy_scouts_going_for_flag()
    {
        let this_tank = this;
        switch(this.carried_flag.color)
        {
            case "red":
                    Tank_Manager.goig_to_flag["blue"].red.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].red = Tank_Manager.goig_to_flag["blue"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].red.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].red = Tank_Manager.goig_to_flag["red"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].red.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].red = Tank_Manager.goig_to_flag["black"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].red.forEach(element => 
                        { if(element.ia_type != "scout" ) element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].red = Tank_Manager.goig_to_flag["green"].red.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 
                    break;
                
            case "blue":  
                    Tank_Manager.goig_to_flag["blue"].blue.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].blue = Tank_Manager.goig_to_flag["blue"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].blue.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].blue = Tank_Manager.goig_to_flag["red"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].blue.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].blue = Tank_Manager.goig_to_flag["black"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].blue.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].blue = Tank_Manager.goig_to_flag["green"].blue.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    break;
            case "black":  
                    Tank_Manager.goig_to_flag["blue"].black.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].black = Tank_Manager.goig_to_flag["blue"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].blue.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].black = Tank_Manager.goig_to_flag["red"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].black.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].black = Tank_Manager.goig_to_flag["black"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].black.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].black = Tank_Manager.goig_to_flag["green"].black.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 
            case "green":  
                    Tank_Manager.goig_to_flag["blue"].green.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["blue"].green = Tank_Manager.goig_to_flag["blue"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["red"].green.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["red"].green = Tank_Manager.goig_to_flag["red"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["black"].green.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["black"].green = Tank_Manager.goig_to_flag["black"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 

                    Tank_Manager.goig_to_flag["green"].green.forEach(element => 
                        { if(element.ia_type != "scout") element.restart_objetive(); });
                    Tank_Manager.goig_to_flag["green"].green = Tank_Manager.goig_to_flag["green"].green.filter(function(value, index, arr)
                    { return value.ia_type == "scout" }); 
                    break;
        }
    }

  //The escorts that are going to get a flag change their protection objetive to the tank who has caught the cow. 
    //(Used when someone get the cow that they are trying to get)
    make_escort_protect_this_tank(flag_target)
    {
        let this_tank = this;
         switch(flag_target.color)
         {
             case "red": 
                 Tank_Manager.goig_to_flag[this_tank.team].red.forEach(element => {
                     if(element != this_tank)
                     { 
                         if(element.ia_type == "escolt")
                         {
                             element.state = "Follow tank";
                             element.actual_tank_to_escort = this_tank;
                             if(element.fire_target != undefined)
                                 element.target = element.fire_target;
                             Tank_Manager.temporal_escort[this_tank.team].red.push(element);
                         }
                         else // guards
                             element.restart_objetive();
                         
                     }});
                 Tank_Manager.goig_to_flag[this.team].red = [];
                 break;
             case "blue":  
                 Tank_Manager.goig_to_flag[this.team].blue.forEach(element => {
                     if(element != this_tank)
                     { 
                         if(element.ia_type == "escolt")
                         {
                             element.state = "Follow tank";
 
                             element.actual_tank_to_escort = this_tank;
                             if(element.fire_target != undefined)
                                 element.target = element.fire_target;
                             Tank_Manager.temporal_escort[this_tank.team].blue.push(element);
                         }
                         else // guards
                         {                            
                             element.restart_objetive();
                         }
                     }
                 });
                 Tank_Manager.goig_to_flag[this_tank.team].blue = [];
                 break;
             case "black":  
                 Tank_Manager.goig_to_flag[this_tank.team].black.forEach(element => {
                     if(element != this_tank)
                     { 
                         if(element.ia_type == "escolt")
                         {
                             element.state = "Follow tank";
 
                             element.actual_tank_to_escort = this_tank;
                             if(element.fire_target != undefined)
                                 element.target = element.fire_target;
                             Tank_Manager.temporal_escort[this_tank.team].black.push(element);
                         }
                         else // guards
                         {                           
                             element.restart_objetive();
                         }
                     }
                 });        
                 Tank_Manager.goig_to_flag[this_tank.team].black = [];
                 break;
             case "green":  
                 Tank_Manager.goig_to_flag[this_tank.team].green.forEach(element => {
                     if(element != this_tank)
                     { 
                         if(element.ia_type == "escolt")
                         {
                             element.state = "Follow tank";
 
                             element.actual_tank_to_escort = this;
                             if(element.fire_target != undefined)
                                 element.target = element.fire_target;
                             Tank_Manager.temporal_escort[this_tank.team].green.push(element);
                         }
                         else // guards
                         {                            
                             element.restart_objetive();
                         }
                     }
                     
                 });
                     Tank_Manager.goig_to_flag[this_tank.team].green = [];
                 break;
         }  
    }

    restart_tanks_going_rescue_base_flag()
    {
        switch(this.base_flag.color)
        {
            case "red": 
                Tank_Manager.goig_to_flag[this.team].red.forEach(element => {
                if(element != this && element.carried_flag == null)
                { 
                   element.restart_objetive();
                }});
                Tank_Manager.goig_to_flag[this.team].red = [];

                break;
            case "blue":  
                Tank_Manager.goig_to_flag[this.team].blue.forEach(element => {
                if(element != this && element.carried_flag == null)
                { 
                    element.restart_objetive();

                }});
                Tank_Manager.goig_to_flag[this.team].blue = [];
                break;
            case "black":  
                Tank_Manager.goig_to_flag[this.team].black.forEach(element => {
                if(element != this && element.carried_flag == null)
                { 
                    element.restart_objetive();

                }});
                Tank_Manager.goig_to_flag[this.team].black = [];
                break;
            case "green":  
                Tank_Manager.goig_to_flag[this.team].green.forEach(element => {
                if(element != this&& element.carried_flag == null)
                { 
                    element.restart_objetive();

                }});
                Tank_Manager.goig_to_flag[this.team].green = [];
                break;
        }     
    }
}