var scene = 
{
    x: 0,
    y: 0,
    w: 100,
    h: 100
}

point_origin = new Vector2(10,10);

point_destin = new Vector2(50,50);

class Grid
{
    constructor()
    {
        this.nodes = [];

        for(let i = 0; i < scene.h; ++i)
            this.nodes[i] = [];
        this.node_length = 50;

        this.times=1;
        this.actual_times=0;
    }

    create_grid()
    {
        let numberOfVerticalLines = scene.w;
        let numberOfHorizontalLines = scene.h;

        let start_pos = this.node_length/2;

        for(let i = 0 ; i < numberOfHorizontalLines; ++i)
        {
            for(let j = 0; j < numberOfVerticalLines; ++j)
            {
                this.nodes[i][j] = new Node(i,j,start_pos + (i * this.node_length), start_pos + (j * this.node_length)); 
            }
        }
    }

    draw_grid(ctx)
    {
        let numberOfVerticalLines = scene.w ;
        let numberOfHorizontalLines = scene.h ;
        for(let i = 0 ; i < numberOfHorizontalLines; ++i)
        {
            for(let j = 0; j < numberOfVerticalLines; ++j)
            {
                const node = this.nodes[i][j];
               
                    const x = node.x;
                    const y = node.y;

                    ctx.beginPath();
                    ctx.lineTo(node.x - grid.node_length/2, node.y + grid.node_length/2);
                  
                    ctx.lineTo(node.x + grid.node_length/2, node.y - grid.node_length/2);

                    ctx.lineTo(node.x + grid.node_length/2, node.y + grid.node_length/2);
                  
                    ctx.lineTo(node.x - grid.node_length/2, node.y - grid.node_length/2);
                  //  ctx.fillRect(node.x,node.y,grid.node_length,grid.node_length);


                    ctx.lineWidth = 3;
                    ctx.strokeStyle = '#FF0000';
                    ctx.stroke();
                

            }
        }
    }




    print_node(ctx, pos)
    {
        let node = this.from_world_to_node(pos.x,pos.y);
        const x = node.x;
        const y = node.y;

        ctx.beginPath();
        ctx.strokeStyle = '#00FF00';
        ctx.lineWidth = 3;

        ctx.lineTo(node.x - grid.node_length/2, node.y + grid.node_length/2);
      
        ctx.lineTo(node.x + grid.node_length/2, node.y - grid.node_length/2);

        ctx.lineTo(node.x + grid.node_length/2, node.y + grid.node_length/2);
      
        ctx.lineTo(node.x - grid.node_length/2, node.y - grid.node_length/2);
      //  ctx.fillRect(node.x,node.y,grid.node_length,grid.node_length);

        ctx.stroke();
    }

    print_points(ctx,a,b)
    {
        ctx.fillStyle = "#FF0000";
        ctx.fillRect(a.x ,a.y,50,50);

        ctx.fillRect(b.x ,b.y,50,50);
    }

    from_world_to_node(x,y)
    {

        let start_pos = this.node_length / 2;
        let x_node = Math.ceil((x - start_pos) / this.node_length);
        let y_node = Math.ceil((y - start_pos) / this.node_length); 

        
            return this.nodes[x_node] != undefined && this.nodes[x_node][y_node] ? this.nodes[x_node][y_node] : null;
    }

    load()
    {
        if(this.actual_times < this.times)
        {
            this.times++;
            for(let i = 0 ; i <scene.h; ++i)
            {
                for(let j = 0; j < scene.w; ++j)
                {
                    this.nodes[i][j].check_if_walkable();
                }
            }
        }
    }
}



function draw_grid(ctx)
{
    console.log("draaawing");
    let numberOfVerticalLines = scene.w /   this.node_length
    for (let i = 0; i < numberOfVerticalLines; i++)
    {
        ctx.beginPath();
        ctx.moveTo(i *   this.node_length, 0);
        ctx.lineTo(i *   this.node_length, scene.h);
        ctx.stroke();
    }

    // horizontal lines
    let numberOfHorizontalLines = scene.h /   this.node_length
    for (let i = 0; i < numberOfHorizontalLines; i++)
    {
        ctx.beginPath();
        ctx.moveTo(0, i *   this.node_length);
        ctx.lineTo(scene.w, i *   this.node_length);
        ctx.stroke();
    }
}


class Node
{
    constructor(x_index, y_index,x,y)
    {
        this.x_index = x_index;
        this.y_index = y_index;
        this.x = x;
        this.y = y;
        this.parent;
        this.walkable = true;

        Node.node_to_check = null;
        Node.i = 0;

    }


    static should_collide_with_laser(fixture)
    { return (fixture.GetUserData().type == "Wall"); }

    callback(fixture,point,normal,fraction)
    {
        if(fixture.GetUserData() != null)
            if(Node.should_collide_with_laser(fixture))
                Node.node_to_check.walkable=false;
    }


    check_if_walkable()
    {
        Node.node_to_check = this;
        let raycast_origin  = new Vector2 (this.x +grid.node_length/2 , this.y+grid.node_length*2);
        let raycast_destination = new Vector2 (this.x - grid.node_length/2, this.y-grid.node_length/2);


        world_array[0].RayCast( this.callback, raycast_origin.from_world_to_box_scale(), raycast_destination.from_world_to_box_scale());
        

        raycast_origin = new Vector2 (this.x - grid.node_length/2 , this.y - grid.node_length);
        raycast_destination  = new Vector2 (this.x + grid.node_length/2, this.y + grid.node_length/2);
      
        world_array[0].RayCast( this.callback, raycast_origin.from_world_to_box_scale(), raycast_destination.from_world_to_box_scale() );

       /* raycast_origin = raycast_destination;
        raycast_destination.set(this.x+grid.node_length , this.y + grid.node_length);
        world_array[0].RayCast( this.callback, raycast_origin.from_world_to_box_scale, raycast_destination.from_world_to_box_scale() );

        raycast_origin = raycast_destination;
        raycast_destination.set(this.x-grid.node_length , this.y + grid.node_length);
        world_array[0].RayCast( this.callback, raycast_origin.from_world_to_box_scale, raycast_destination.from_world_to_box_scale() );*/
    }

    

    
    equal(other)
    {
        return (this.x_index == other.x_index && this.y_index == other.y_index)
    }


    get_walkable_neighbours()
    {
        let neighbours = [];

        let neighbour = grid.nodes[this.x_index-1][this.y_index];//
       
        if(neighbour.walkable)
            neighbours.push(neighbour);
        
        neighbour = grid.nodes[this.x_index+1][this.y_index];//
      
        if(neighbour.walkable)
            neighbours.push(neighbour);
        neighbour = grid.nodes[this.x_index-1][this.y_index-1];//

        if(neighbour.walkable)
            neighbours.push(neighbour);

        if(grid.nodes[this.x_index-1] == undefined)
            console.log(this.x_index-1 + "   " + this.y_index)
        neighbour = grid.nodes[this.x_index-1][this.y_index+1];//
        if(neighbour == undefined)
        console.log(this.x_index-1 + "   " + this.y_index)
        if(neighbour.walkable)
            neighbours.push(neighbour);
        neighbour = grid.nodes[this.x_index+1][this.y_index+1];//
        if(neighbour == undefined)
        console.log(this.x_index-1 + "   " + this.y_index)
        if(neighbour.walkable)
            neighbours.push(neighbour);
        neighbour = grid.nodes[this.x_index+1][this.y_index-1];//
        if(neighbour == undefined)
        console.log(this.x_index-1 + "   " + this.y_index)
        if(neighbour.walkable)
            neighbours.push(neighbour);
        neighbour = grid.nodes[this.x_index][this.y_index-1];//
        if(neighbour == undefined)
        console.log(this.x_index-1 + "   " + this.y_index)
        if(neighbour.walkable)
            neighbours.push(neighbour);
        neighbour = grid.nodes[this.x_index][this.y_index+1];
        if(neighbour == undefined)
        console.log(this.x_index-1 + "   " + this.y_index)
        if(neighbour.walkable)
            neighbours.push(neighbour);

        return neighbours;
    }


    get_closest_neighbour(target_node, closed_nodes)
    {
        let target_pos = to_vector2(target_node);
        let dist;

        let selected_neighbour = null;
        let min_dist = 9999999999999;

        let neighbours = this.get_walkable_neighbours();
        for(let i = 0; i < neighbours.length; ++i)
        {
            dist = to_vector2(neighbours[i]).subtract(target_pos).magnitude();

            if(dist < min_dist)
            {
                if (!closed_nodes.some(e => (e.x_index == neighbours[i].x_index && e.y_index == neighbours[i].y_index ))) 
                {
                    selected_neighbour = neighbours[i];
                    min_dist = dist;
                }
                
            }
        }
        if(selected_neighbour == null)
        {

            console.log("NUUUUUUUUUUUUL" + neighbours.length);

        }
        return selected_neighbour; 
    }
}



