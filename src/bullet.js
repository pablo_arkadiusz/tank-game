class Bullet
{
    constructor(parent, type, ball_radius, damage, sensor=true)
    {
        //#region Fake abstract class

        if (this.constructor === Weapon)
        { throw new TypeError('Abstract class "Weapon" cannot be instantiated directly.'); }

        if (this.move === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement move method'); }

        if (this.update === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement update method'); }

        if (this.draw === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement draw method'); }

        if (this.stop === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement stop method'); }

        if (this.on_collision_enter === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement on_collision_enter method'); }

        //#endregion

        //#region Class attributes 

        this.parent = parent;
        this.type = "Bullet";
        this.damage = damage;

        this.position = new Vector2();
        this.direction = new Vector2();
        this.rotation = 0;

        //Is this bullet fired or in action ?
        this.is_active;

        //#endregion

        this.create_body(ball_radius,sensor);
    }
    
    //#region Body creation

    create_body(ball_radius,sensor)
    {
        if(sensor)
        {
            this.physic_options =       
            {
                type : b2Body.b2_dynamicBody,
                user_data: this,
                friction: 0
            };

            this.body = CreateBall(
                100, 100,
                10 / scale, this.physic_options, collision_rule.BULLET);

            let fixture = this.body.GetFixtureList();
            fixture.SetUserData(this);
            fixture.SetSensor(true);
        }
        else
        {
            this.physic_options =       
            {
                type : b2Body.b2_dynamicBody,
                user_data: this,
                density: 0.1,
                friction: 0,
                restitution:0.8,
                linearDamping: 0,
                angularDamping: 0,
            };
            this.body = CreateBall(
                100, 100,
                10 / scale, this.physic_options, collision_rule.BULLET);

            let fixture = this.body.GetFixtureList();
            fixture.SetUserData(this);
        }     
    }

    //#endregion

}