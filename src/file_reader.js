

class File_Reader
{
    static readTextFile(file)
    {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, true);
        rawFile.onreadystatechange = function ()
        {
            if(rawFile.readyState === 4)
            {
                if(rawFile.status === 200 || rawFile.status == 0)
                {
                    var allText = rawFile.responseText;
                    File_Reader.load_lines_to_array(allText);
                }
            }
        }
        rawFile.send(null);
    }


    static load_lines_to_array(text)
    {
        var lines = text.split('\n');
        for(var i = 0;i < lines.length;++i)
        {
           File_Reader.names_text_arr[i] = lines[i];
        }

        shuffleArray(File_Reader.names_text_arr);
        
    }
//readTextFile('Assets/text.txt');
}

File_Reader.names_text = null;
File_Reader.names_text_arr = [];