class SSAnimation
{
    //#region Constructor

    constructor(image, frameWidth, frameHeight, frameCount, framesDuration, loop)
    {
        this.image = image;

        this.pivot = {
            x: 32,
            y: 32
        };

        this.framesDuration = framesDuration;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.frameCount = frameCount;
        this.actualAnimation = 0;
        this.actualFrame = 0;
        this.actualFrameCountTime = 0;
        this.loop = loop;
        this.play = true;

        this.halfFrameWidth = this.frameWidth / 2;
        this.halfFrameHeight = this.frameHeight / 2;

        this.ended = false;
    }
  
   //#endregion

    //#region Update and draw

    Update(delta_time) {this.update_animation(delta_time); }

    Draw(ctx)
    {
        if(this.loop || this.play)
            ctx.drawImage(this.image, this.actualFrame * this.frameWidth, this.actualAnimation * this.frameHeight, this.frameWidth, this.frameHeight, -this.halfFrameWidth, -this.halfFrameHeight, this.frameWidth, this.frameHeight);
    }

    //#endregion

    //#region Animation management methods

    update_animation(delta_time)
    {
        if(this.loop || this.play)
        {
            this.actualFrameCountTime += delta_time;
            if (this.actualFrameCountTime >= this.framesDuration)
            {
                // update the animation with the new frame
                this.actualFrame = (this.actualFrame + 1) % this.frameCount[this.actualAnimation];

                if (this.actualFrame == 0)
                    this.actualAnimation = (this.actualAnimation + 1) % this.frameCount.length;

                this.actualFrameCountTime = 0;

                if(!this.loop)
                    if(this.is_on_last_frame())
                    {
                        this.play = false;   
                        this.ended = true;
                        if(this.on_animation_end != null)
                            this.on_animation_end();
                    }
            }
        }
    }

    play_animation(on_animation_end = null)
    {
        this.play = true;
        this.actualAnimation = 0;
        this.actualFrame = 0;
        this.on_animation_end = on_animation_end;
    }

    is_on_last_frame()
    { return this.actualFrame == this.frameCount[this.actualAnimation] - 1 && this.actualAnimation == this.frameCount.length -1; }

    //#endregion
}