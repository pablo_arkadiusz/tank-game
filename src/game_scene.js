//Global variables for the game

var canvas_center;
var canvas_array = [];
var ctx_array = [];


let grid = new Grid();
let tank_manager = new Tank_Manager();

var red_cow, green_cow, blue_cow, black_cow;
var red_puntuation=0, green_puntuation=0, blue_puntuation=0, black_puntuation=0;

var tank = null,
    second_tank = null,
    cows = [],
    tanks = [],
    items = [],
    walls = [],
    props = [],
    platforms = {};


// Layer masks and collisions

let layer = 
{
    NOTHING:       0x0000,
    WALL:          0x0001,
    TANK:          0x0002,
    BULLET:        0x0004,
    SENSOR:        0x0008,
    ITEM:          0x00016,
    IA:            0x00032,
    EAR_SENSOR:    0x00064,
};

let collision = 
{
    WALL:           layer.BULLET | layer.TANK | layer.IA,
    TANK:           layer.WALL | layer.TANK | layer.ITEM  | layer.IA | layer.SENSOR | layer.SENSOR_IA| layer.EAR_SENSOR,
    SENSOR:         layer.TANK | layer.IA,
    BULLET:         layer.WALL  | layer.IA,
    ITEM:           layer.TANK ,
    IA:             layer.TANK | layer.WALL | layer.BULLET | layer.IA | layer.SENSOR | layer.SENSOR_IA,
    EAR_SENSOR:     layer.TANK
};


let collision_rule =
{
    WALL:           { layer: layer.WALL,               collision: collision.WALL          },
    IA:             { layer: layer.IA,                 collision: collision.IA            },
    TANK:           { layer: layer.TANK,               collision: collision.TANK          },
    CROSS_BULLET:   { layer: layer.CROSS_BULLET,       collision: collision.CROSS_BULLET  },
    BULLET:         { layer: layer.BULLET,             collision: collision.BULLET        },
    ITEM:           { layer: layer.ITEM,               collision: collision.ITEM          },
    SENSOR:         { layer: layer.SENSOR,             collision: collision.SENSOR        },
    EAR_SENSOR:     { layer: layer.EAR_SENSOR,         collision: collision.EAR_SENSOR    }

}


class Game_Scene
{
    constructor(player_num)
    {
        this.bodies_to_set_position = [];
        this.bodies_to_delete = [];
        this.player_num = player_num;

        document.getElementById ("game_scene").className -= 'invisible';

    }


    start()
    {
        //Canvas management:

        //Getting all canvases and contexts
        for(let i = 0; i < this.player_num; ++i)
            canvas_array[i] =  document.getElementById("myCanvas" + i.toString());
    
        for(let i = 0; i < canvas_array.length; ++i)
            ctx_array[i] = canvas_array[i].getContext("2d",  { alpha: false });

        // Setting all canvas width and height accoriding to the number of players
        canvas_array.forEach(element => 
        {
            element.width = (screen.width - 10) / (Math.round((this.player_num / 3) + 1) ) ;
            element.height = (window.innerHeight )/ Math.ceil(this.player_num / 2);
        });
       
        canvas_center = new Vector2(canvas_array[0].width/2, canvas_array[0].height/2 );


        PreparePhysics(ctx_array);
        SetupKeyboardEvents();
        SetupMouseEvents();



        create_offscreen_canvases();

        /*ctx_array.forEach(element => {
            element.drawImage(tile_offscreen_canvas, 0, 0);
        });*/


        grid.create_grid();
        //Creating some walls to the world

        //Muro delimitador
        for(let i = 0; i <40 ; ++i)
            walls.push(new Wall({x:  i *120, y: 0}, "block_b_02"));

        for(let i = 0; i <40 ; ++i)
            walls.push(new Wall({x: 0 , y: i *120}, "block_b_02"));
        for(let i = 0; i <41 ; ++i)
            walls.push(new Wall({x: 4800 , y:  i *120}, "block_b_02"));

        for(let i = 0; i <40 ; ++i)
            walls.push(new Wall({x: i *120 , y: 4800}, "block_b_02"));



            //deocraciones 
            
        walls.push(new Wall({x: 500, y: 350}, "block_b_02"));
        walls.push(new Wall({x: 620, y: 350}, "block_b_02"));
        walls.push(new Wall({x: 500, y: 600}, "block_b_02"));
        walls.push(new Wall({x: 620, y: 600}, "block_b_02"));

        props.push (new Prop( { x: 403, y: 9 }, "tree_01" ));
        props.push (new Prop( { x: 403, y: 200 }, "tree_05" ));
        ///////////////////////////////

        //Base azul
        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({x: 1800  + i * 120, y: 720}, "block_b_02"));
        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({x: 2520  + i * 120, y: 720}, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 1800  , y: 120 + i * 120 }, "block_b_02"));
        for(let i = 0; i <1 ; ++i)
            walls.push(new Wall({x: 1800  , y: 600 + i * 120 }, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 3000, y: 120 + i * 120 }, "block_b_02"));
        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 3000, y: 600+ i * 120 }, "block_b_02"));
        


        let blue_artifact = new Prop( { x: 2400, y: 200 }, "artifact" );
        props.push (blue_artifact);

        blue_cow = new Flag( new Vector2( 2400, 200 ), "blue", blue_artifact);



        //walls.push (new Wall( { x: 2000, y: 500 }, "well"));
        walls.push (new Wall( { x: 1800, y: 1000 }, "log"));
        props.push (new Prop( { x: 2120, y: 550 }, "cactus1"));
        props.push (new Prop( { x: 2750, y: 570 }, "cactus2"));

        props.push (new Prop( { x: 2520, y: 790 }, "blueflag_dl"));
        props.push (new Prop( { x: 2160, y: 790 }, "blueflag_dr"));
        props.push (new Prop( { x: 3070, y: 585 }, "blueflag_rt"));
        props.push (new Prop( { x: 3070, y: 245 }, "blueflag_rd"));
        props.push (new Prop( { x: 1730, y: 585 }, "blueflag_lt"));
        props.push (new Prop( { x: 1730, y: 245 }, "blueflag_ld"));


        //Base red

        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({ x: 720, y: 1800  + i * 120 }, "block_b_02"));
        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({ x: 720 , y: 2520  + i * 120 }, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({ x: 120 + i * 120, y: 1800 }, "block_b_02"));
        for(let i = 0; i <1 ; ++i)
            walls.push(new Wall({ x: 600 + i * 120, y: 1800 }, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 120 + i * 120, y: 3000 }, "block_b_02"));
        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 600+ i * 120 , y: 3000 }, "block_b_02"));


        let red_artifact = new Prop( { x: 200, y: 2400 }, "artifact" );
        props.push (red_artifact);
        red_cow = new Flag( new Vector2( 200, 2400  ), "red", red_artifact);


        //walls.push (new Wall( { x: 500, y: 2000 }, "well"));
        walls.push (new Wall( { x: 1000, y: 1800 }, "log"));
        props.push (new Prop( { x: 550, y: 2120 }, "cactus1"));
        props.push (new Prop( { x: 570, y: 2750 }, "cactus2"));

        props.push (new Prop( { x: 790,  y: 2520 }, "redflag_rd"));
        props.push (new Prop( { x: 790,  y: 2160 }, "redflag_rt"));
        props.push (new Prop( { x: 585, y: 3070 }, "redflag_dr"));
        props.push (new Prop( { x: 245, y: 3070 }, "redflag_dl"));
        props.push (new Prop( { x: 585, y: 1730 }, "redflag_tr"));
        props.push (new Prop( { x: 245, y: 1730 }, "redflag_tl"));


        //Base black


        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({ x: 1800  + i * 120, y: 4080 }, "block_b_02"));
        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({ x: 2520  + i * 120, y: 4080 }, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 1800  , y: 4560 + i * 120 }, "block_b_02"));
        for(let i = 0; i <1 ; ++i)
            walls.push(new Wall({x: 1800  , y: 4200 + i * 120 }, "block_b_02"));
        
        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 3000, y: 4680 - i * 120 }, "block_b_02"));
        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 3000, y: 4200 - i * 120 }, "block_b_02"));
        

        
        let black_artifact = new Prop( { x: 2400, y: 4600 }, "artifact" );
        props.push (black_artifact);
        black_cow = new Flag( new Vector2( 2400, 4600 ), "black", black_artifact );



    // walls.push (new Wall( { x: 2000, y: 4300 }, "well"));
        walls.push (new Wall( { x: 1800, y: 3800 }, "log"));
        props.push (new Prop( { x: 2120, y: 4250 }, "cactus1"));
        props.push (new Prop( { x: 2750, y: 4270 }, "cactus2"));

        props.push (new Prop( { x: 2520, y: 4010 }, "blackflag_tr"));
        props.push (new Prop( { x: 2160, y: 4010 }, "blackflag_tl"));
        props.push (new Prop( { x: 3070, y: 4215 }, "blackflag_rt"));
        props.push (new Prop( { x: 3070, y: 4555 }, "blackflag_rd"));
        props.push (new Prop( { x: 1730, y: 4215 }, "blackflag_lt"));
        props.push (new Prop( { x: 1730, y: 4555 }, "blackflag_ld"));
        
        //Base green



        for(let i = 0; i <4 ; ++i)
        walls.push(new Wall({ x: 4080, y:  1800  + i * 120 }, "block_b_02"));
        for(let i = 0; i <4 ; ++i)
            walls.push(new Wall({ x: 4080, y: 2520  + i * 120 }, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 4560 + i * 120  , y: 1800 }, "block_b_02"));
        for(let i = 0; i <1 ; ++i)
            walls.push(new Wall({x: 4200 + i * 120   , y: 1800}, "block_b_02"));

        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 4680 - i * 120, y: 3000 }, "block_b_02"));
        for(let i = 0; i <2 ; ++i)
            walls.push(new Wall({x: 4200 - i * 120, y: 3000 }, "block_b_02"));


        let green_artifact = new Prop( { x: 4600, y: 2400 }, "artifact" );

        props.push ( green_artifact );
        green_cow = new Flag( new Vector2( 4600, 2400 ), "green" ,green_artifact);



        //walls.push (new Wall( { x: 4300, y: 2000 }, "well"));
        walls.push (new Wall( { x: 3800, y: 1800 }, "log"));
        props.push (new Prop( { x: 4250, y: 2120 }, "cactus1"));
        props.push (new Prop( { x: 4270, y: 2750 }, "cactus2"));

        props.push (new Prop( { x: 4010, y: 2520 }, "greenflag_ld"));
        props.push (new Prop( { x: 4010, y: 2160 }, "greenflag_lt"));
        props.push (new Prop( { x: 4215, y: 3070 }, "greenflag_dl"));
        props.push (new Prop( { x: 4555, y: 3070 }, "greenflag_dr"));
        props.push (new Prop( { x: 4215, y: 1730 }, "greenflag_tl"));
        props.push (new Prop( { x: 4555, y: 1730 }, "greenflag_tr"));



    
        cows.push (red_cow);
        cows.push (blue_cow);
        cows.push (green_cow);
        cows.push (black_cow);

        tank_manager.create_player_tanks();

        
      

        grid.load();

        tank_manager.create_platforms();
        tank_manager.create_ia_tanks(2,1,2);
    }

    //Game object manipulation


    set_position(body,pos) { this.bodies_to_set_position.push({body: body, pos:pos}); }
    set_bodies_position()
    {
        for(let i = 0; i < this.bodies_to_set_position.length; ++i)
        {
            //Put the bullet in the starting position
            var transform = 
            {
                position:  this.bodies_to_set_position[i].pos,
                GetAngle: function(){return 0;}
            }
            this.bodies_to_set_position[i].body.SetTransform(transform);   
        }  
        this.bodies_to_set_position = [];
    }



    delete_body(body) { this.bodies_to_delete.push(body); }

    //Simulate a delete. It just move the bodies to a hidden position to reuse the same bodies
    delete_dead_bodies()
    {
        for(let i = 0; i < this.bodies_to_delete.length; ++i)
        {
            //Put the bullet in the starting position
            var transform = 
            {
                position:  {x : 100, y: 100},
                GetAngle: function(){return 0;}
            }
            this.bodies_to_delete[i].SetTransform(transform);   
        }  
        this.bodies_to_delete = [];
    }


    update(delta_time)
    {
        world_array.forEach(element => 
            {
                element.Step(delta_time, 8, 3);
                element.ClearForces();
            });
        
            this.delete_dead_bodies();
            this.set_bodies_position();
            tank_manager.update(delta_time);

        
            tanks.forEach(element => {element.update(delta_time);});
            cows.forEach(element => {element.update(delta_time)});
    }



    pre_draw_first(ctx) { tank.pre_draw(ctx); }
    pre_draw_second(ctx) { second_tank.pre_draw(ctx); }
    post_draw_first(ctx) { tank.post_draw(ctx); }
    post_draw_second(ctx) { second_tank.post_draw(ctx); }

    draw(ctx,the_world)
    {
        // clean the canvas
        ctx.clearRect(0, 0, canvas_array[0].width, canvas_array[0].height);


        if(ctx == ctx_array[0])
            this.pre_draw_first(ctx);
        else if(ctx == ctx_array[1])
            this.pre_draw_second(ctx);
    
        // draw the box2d world
        this.draw_world(ctx, the_world);
    
        ctx.drawImage(tile_offscreen_canvas, 0, 0);
    
        //grid.draw_grid(ctx);
    
    
        platforms["blue"].forEach(element => {element.draw(ctx)});
        platforms["green"].forEach(element => {element.draw(ctx)});
        platforms["black"].forEach(element => {element.draw(ctx)});
        platforms["red"].forEach(element => {element.draw(ctx)});
    
    
        items.forEach(element => {element.draw(ctx)});
    
    
        walls.forEach(element => {element.draw(ctx); });
        props.forEach(element => {element.draw(ctx)});
        cows.forEach(element => {element.draw(ctx)});
    
    
        tanks.forEach(element => { element.draw(ctx); });
    

        if(ctx == ctx_array[0])
            this.post_draw_first(ctx);
        else if(ctx == ctx_array[1])
            this.post_draw_second(ctx);
    

        ctx.font = "30px impact";
        ctx.fillStyle = '#fff';
        ctx.fillText("        TEAM SCORE: ", 100, 50);
        ctx.fillText("______________" , 100, 55);
        ctx.fillText("RED TEAM: " + red_puntuation, 100, 100);
        ctx.fillText("BLUE TEAM: " + blue_puntuation, 100, 150);
        ctx.fillText("GREEN TEAM: " + green_puntuation, 100, 200);
        ctx.fillText("BLACK TEAM: " + black_puntuation, 100, 250);


        var ctx = canvas_array[0].getContext("2d");
      
        ctx.lineWidth = 6;
    }


    draw_world (ctx, world)
    {
        // Transform the canvas coordinates to cartesias coordinates
        ctx.save();
        ctx.translate(0, canvas_array[0].height);
        //ctx.scale(1, -1);
        world.DrawDebugData();
        ctx.restore();
    }

    loop()
    {
        //deltaTime
        let now = Date.now();
        let deltaTime = now - time;
        globalDT = deltaTime;

        if (deltaTime > 1000)
            deltaTime = 0;

        time = now;

        // frames counter
        frames++;
        acumDelta += deltaTime;

        if (acumDelta > 1000)
        {
            FPS = frames;
            frames = 0;
            acumDelta -= acumDelta;
        }

        // Game logic -------------------
        Input.Update();
        this.update(deltaTime / 1000);
        Input.PostUpdate();

        for(let i = 0; i < ctx_array.length; ++i)
            this.draw(ctx_array[i], world_array[i]);

            
        if(tank.show_stats_input())
            tank_manager.display_stadistics(tank);
        if(this.player_num > 1)
            if(second_tank.show_stats_input())
                tank_manager.display_stadistics(second_tank);
    }
}