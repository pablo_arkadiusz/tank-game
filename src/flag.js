class Flag
{
    constructor(initialPosition, color, artifact)
    {
        this.artifact = artifact;
        this.type = "Flag";
        this.color = color;
        this.initial_pos = initialPosition;
        this.position = initialPosition;
        this.select_image();

        this.physic_options = {type : b2Body.b2_staticBody, user_data: this};

        
        this.body = CreateBall(this.position.x / scale, (canvas_array[0].height - this.position.y)  / scale ,
        0.5, this.physic_options, collision_rule.SENSOR);

        let fixture =  this.body.GetFixtureList();
        fixture.SetSensor(true);
        fixture.SetUserData(this);


        this.width = 50;
        this.height = 50;
        this.pivot = new Vector2
        (
            - this.width/2,
            - this.height/2
        );

        this.visible = true;

        this.tank_carrying = null;
        Flag.base_radius = 500;
        this.create_sensor();

        this.tank_near_flag = [];
        this.cows_inside = [];

        this.move = false;
        this.time = 0;
        this.right = true;
    }

    be_picked_up()
    {
        this.visible = false;
        this.switch_off_artifact();
    }

    drop(new_pos)
    {
        this.visible = true;
        game_scene.set_position(this.body, new_pos.from_world_to_box_scale());
        this.position = new_pos;

        this.tank_near_flag.forEach(element => {
            if(element.team != this.color)
                element.try_go_for_flag(this);
            
            else if (element.team == this.color && !this.is_in_his_base()  && this.visible)
                element.go_for_base_flag(this);

        });
    }

    switch_off_artifact()
    {
        this.artifact.change_canvas(Prop.artifact_off_canvas);
    }
    switch_on_artifact()
    {
        this.artifact.change_canvas(Prop.artifact_canvas);
    }
    is_in_his_base()
    {
        return this.position == this.initial_pos;
    }

    create_sensor()
    {
         let shape = new b2CircleShape();
         shape.m_radius =  Flag.base_radius / scale;

        let options = {
            density : 1.0,
            friction: 1.0,
            restitution : 0.5,
            }

        // Fixture: define physics properties (density, friction, restitution)
        var fix_def = new b2FixtureDef();
        fix_def.shape = shape;

        fix_def.density = options.density;
        fix_def.friction = options.friction;
        fix_def.restitution = options.restitution;
            
    
        fix_def.filter.categoryBits = collision_rule.SENSOR.collision;
        fix_def.filter.maskBits = collision_rule.SENSOR.layer;
        this.sensor = this.body.CreateFixture(fix_def);
        this.sensor.SetUserData("Sensor");
        this.sensor.SetSensor(true);  
        
        this.sensor_targets = [];
    }


    
    restart()
    {
        this.switch_on_artifact();
        game_scene.set_position(this.body,  this.initial_pos.from_world_to_box_scale());
        this.position = this.initial_pos;
        this.visible = true;


        this.tank_near_flag.forEach(element => {
            if(element.team != this.color)
                element.try_go_for_flag(this);
        });
    }

    select_image()
    {
        switch(this.color)
        {
            case "red":   this.image = graphics.cow_flag.image;     break;
            case "blue":  this.image = graphics.cow_flag.image;    break;
            case "black": this.image = graphics.cow_flag.image;   break;
            case "green": this.image = graphics.cow_flag.image;   break;
        }
    }

    draw(ctx)
    {
        if(this.visible)
            ctx.drawImage(this.image, this.position.x + this.pivot.x, this.position.y + this.pivot.y, this.width, this.height);
        

    }

    update(delta_time)
    {
        if(Input.IsKeyPressed(KEY_SPACE))
        {
            this.move = true;
            console.log("move true");
        }
        if(this.move)
        {
            this.time +=delta_time;
            if(this.time > 1)
                {
                    this.right = !this.right;
                    this.time = 0;
                }
            if(this.right)
                this.body.SetLinearVelocity( new Vector2(1,1));
            else
                this.body.SetLinearVelocity( new Vector2(-1,-1));
        }
    }

    on_collision_enter(other, this_fixture)
    {
        if(this_fixture.GetUserData() == "Sensor")
        {   
            if(other.type == "IA Tank")
            {
                this.tank_near_flag.push(other);
                if(this.visible)
                {
                    if(other.team != this.color)
                        other.try_go_for_flag(this);
                    else
                        if(!this.is_in_his_base())
                        {
                            other.go_for_base_flag(this);
                        }
                    }
            }
            if(other.type == "Flag")
            {
                this.cows_inside.push(other);
            }
        }

    }

    on_collision_leave(other, this_fixture)
    {
        if(this_fixture.GetUserData() == "Sensor" )
        {
            if(other.type == "IA Tank")
            {
                this.tank_near_flag = this.tank_near_flag.filter(function(value, index, arr) { return value != other; }); 

            }
            if(other.type == "Flag")
            {
                this.cows_inside = this. this.cows_inside.filter(function(value, index, arr) { return value != other; }); 
            }
        }
    }





}