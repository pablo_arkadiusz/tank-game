
class Wall
{
    //#region Cosntructor

    constructor(position,type)
    {
        this.type = "Wall";
        this.wall_type = type;
        this.physic_options = {type : b2Body.b2_staticBody, user_data: this};
      


        this.box_scale_position = new Vector2(position.x/scale,(canvas_array[0].height - position.y)/scale);


        this.position = new Vector2(position.x,position.y);
     
        this.select_canvas();

        const body_width =  (this.canvas.width >> 1) /scale;
        const body_height = type != "log" ? (this.canvas.width >> 1) /scale : (this.canvas.height >> 1) /scale /2 ;
        this.body = CreateBox(this.box_scale_position.x,this.box_scale_position.y, body_width , body_height,  this.physic_options, collision_rule.WALL);
        this.body.GetFixtureList().SetUserData(this);
    }

    //#endregion

    select_canvas()
    {
        switch(this.wall_type)
        {
            case "block_b_02":
                this.canvas = Wall.block_b_02_canvas
                    break;
            case "well":
                    this.canvas = Wall.well_canvas;
                    break;
            case "log":
                    this.canvas = Wall.log_canvas;
                    break;
            default:
                console.log("NO SUCH TYPE : " + this.wall_type);
        }

        this.pivot = new Vector2
        (
            - this.canvas.width >> 1,
            - this.canvas.height >> 1
        );
    }
    draw(ctx)
    {
        ctx.drawImage( this.canvas, this.position.x + this.pivot.x, this.position.y + this.pivot.y);
    }
    //#region Collision methods

    on_collision_enter(other)
    {
        return;
    }

    //#endregion
}