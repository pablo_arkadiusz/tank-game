class Tank_Manager
{
    constructor()
    {
        this.tank_index = 0;
        this.tank_amount;
        this.tanks_to_respawn = [];
        this.time_to_respawn = 10;
        this.tank_names = [];
        this.ordered_tanks = [];

        
        Tank_Manager.temporal_escort = {};
        Tank_Manager.temporal_escort["green"] =  {red: [], blue: [], black: []};
        Tank_Manager.temporal_escort["red"] =    {green: [], blue: [], black: []};
        Tank_Manager.temporal_escort["black"] =  {red: [], blue: [], green: []};
        Tank_Manager.temporal_escort["blue"] =   {red: [], green: [], black: []};

        Tank_Manager.goig_to_flag = {};
        Tank_Manager.goig_to_flag["green"] = {red: [], blue: [], black: [], green: []};
        Tank_Manager.goig_to_flag["red"] = {green: [], blue: [], black: [], red: []};   
        Tank_Manager.goig_to_flag["blue"] = {red: [], green: [], black: [], blue: []};
        Tank_Manager.goig_to_flag["black"] = {red: [], blue: [], green: [], black: []};

        Tank_Manager.escorting_flag = {};
        Tank_Manager.escorting_flag["green"] = [];
        Tank_Manager.escorting_flag["red"] = [];
        Tank_Manager.escorting_flag["blue"] = [];
        Tank_Manager.escorting_flag["black"] = [];
    }

    update(delta_time) { this.respawn_tanks(delta_time); }
   
    respawn_tank(tank) { this.tanks_to_respawn.push(tank ); tank.time_to_respawn = 0;}
 
    respawn_tanks(delta_time)
    {
        for(let i = 0; i < this.tanks_to_respawn.length; ++i)
        {
            if(this.tanks_to_respawn[i].type == "IA Tank")
            {
                this.tanks_to_respawn[i].time_to_respawn += delta_time;
                if(this.tanks_to_respawn[i].time_to_respawn > this.time_to_respawn)
                {
                    this.tanks_to_respawn[i].revive();
                    this.tanks_to_respawn.splice(i,1);
                }
            }
            else
            {
                this.tanks_to_respawn[i].time_to_respawn += delta_time;
                if(this.tanks_to_respawn[i].time_to_respawn >  this.time_to_respawn)
                {
                    this.tanks_to_respawn[i].enable_respawn();
                    this.tanks_to_respawn.splice(i,1);
                }
            }
        };
    }

    sort_stadistics()
    {
        this.ordered_tanks.sort(function(a, b) { return a.points < b.points ? 1 : -1});
    }

    display_stadistics(player_tank)
    {
        let border = canvas_array[0].width/80;
        let space = canvas_array[0].height/18;
        let pos_margin =  canvas_array[0].width/60;
        let cows_margin =  canvas_array[0].width/35;
        let deaths_margin = canvas_array[0].width/27;
        let kills_margin = canvas_array[0].width/55;
        let score_margin =  canvas_array[0].width/35;
        ctx_array[player_tank.id].fillStyle = "rgba(0,0,0,.5)";

        ctx_array[player_tank.id].fillRect(border, border, canvas_array[0].width-border*2, canvas_array[0].height-border*2);

        let width_dist = canvas_array[0].width/6;
        let margin = canvas_array[0].width/30;

        let font_size = canvas_array[0].width/80;
        ctx_array[player_tank.id].fillStyle = "white";
        ctx_array[player_tank.id].font = (font_size*2) +"px Comic Sans MS";
        ctx_array[player_tank.id].fillText('Pos',canvas_array[0].width/2 - width_dist*2.5 - margin, canvas_array[0].height/15 );
        ctx_array[player_tank.id].fillText('Name',canvas_array[0].width/2 - width_dist*1.5 -margin, canvas_array[0].height/15 );
        ctx_array[player_tank.id].fillText('Kills',canvas_array[0].width/2 - width_dist *0.5-margin, canvas_array[0].height/15 );
        ctx_array[player_tank.id].fillText('Deaths',canvas_array[0].width/2 + width_dist *0.5-margin, canvas_array[0].height/15 );
        ctx_array[player_tank.id].fillText('Cows',canvas_array[0].width/2 + width_dist* 1.5-margin, canvas_array[0].height/15 );
        ctx_array[player_tank.id].fillText('Score',canvas_array[0].width/2 + width_dist*2.5-margin, canvas_array[0].height/15 );

        ctx_array[player_tank.id].font = font_size+"px Comic Sans MS";

        for(let i = 0; i < 15; ++i)
        {
            ctx_array[player_tank.id].fillStyle = this.ordered_tanks[i].id == player_tank.id ? "blue" : "white";

            ctx_array[player_tank.id].fillText(i+1,canvas_array[0].width/2 - width_dist*2.5 - margin + pos_margin, canvas_array[0].height/15 + space*1.5 + space * i );
        }
        for(let i = 0; i < 15; ++i)
        {
            ctx_array[player_tank.id].fillStyle = this.ordered_tanks[i].id == player_tank.id ? "blue" : "white";

            ctx_array[player_tank.id].fillText(this.ordered_tanks[i].name, canvas_array[0].width/2 - width_dist*1.5 -margin, canvas_array[0].height/15 + space *1.5+ space * i );

        }
        for(let i = 0; i < 15; ++i)
        {
            ctx_array[player_tank.id].fillStyle = this.ordered_tanks[i].id == player_tank.id ? "blue" : "white";

            ctx_array[player_tank.id].fillText(this.ordered_tanks[i].kills,canvas_array[0].width/2 - width_dist *0.5-margin + kills_margin, canvas_array[0].height/15 + space*1.5 + space * i );

        }
        for(let i = 0; i < 15; ++i)
        {
            ctx_array[player_tank.id].fillStyle = this.ordered_tanks[i].id == player_tank.id ? "blue" : "white";

            ctx_array[player_tank.id].fillText(this.ordered_tanks[i].deaths,canvas_array[0].width/2 + width_dist *0.5-margin + deaths_margin, canvas_array[0].height/15 + space *1.5+ space * i );

        }
        for(let i = 0; i < 15; ++i)
        {
            ctx_array[player_tank.id].fillStyle = this.ordered_tanks[i].id == player_tank.id ? "blue" : "white";

            ctx_array[player_tank.id].fillText(this.ordered_tanks[i].cows,canvas_array[0].width/2 + width_dist* 1.5-margin + cows_margin, canvas_array[0].height/15 + space *1.5+ space * i );

        }
        for(let i = 0; i < 15; ++i)
        {
            ctx_array[player_tank.id].fillStyle = this.ordered_tanks[i].id == player_tank.id ? "blue" : "white";

            ctx_array[player_tank.id].fillText(this.ordered_tanks[i].points,canvas_array[0].width/2 + width_dist*2.5-margin + score_margin, canvas_array[0].height/15 + space*1.5+ space * i );
        }
    }


    select_random_weapon()
    {
        let chance = Math.random();
        if (chance < 0.1) // 0-10         10%
            return "MachineGun";
        else if (chance < 0.50) // 10-55  40%    
            return "Rifle";
        else if(chance < 0.8) // 55-80    20%
            return "AutoMissile"
        else                             //20%
            return "Granade"
            // 30% chance of being here
    }

    create_player_tanks()
    {
        tank = new Tank({x: 2045, y: 170}, this.tank_index++ , "blue");
        tanks.push(tank);
        if(game_scene.player_num >1)
        {
            second_tank = new Tank({x: 4600, y: 2600}, this.tank_index++, "green");
            tanks.push(second_tank);
        }
    }

    create_ia_tanks(scout_amount, escolt_for_every_scout, guard_amount)
    {
        this.tank_amount = (scout_amount + scout_amount * escolt_for_every_scout + guard_amount) * 4;
        let tank_team_count = 0;

        for(let i = 0; i < scout_amount; ++i)
        {
            tanks.push(new IA_Tank(platforms["blue"][ tank_team_count ++].position, this.select_random_weapon(), this.tank_index++, "blue", "scout"));
            for(let j = 0; j < escolt_for_every_scout; ++j )
                tanks.push(new IA_Tank(platforms["blue"][ tank_team_count ++].position, this.select_random_weapon(), this.tank_index++, "blue", "escolt",  tanks[ tanks.length -1 -j ]));
        }

        for(let i = 0; i < guard_amount; ++i)
            tanks.push(new IA_Tank(platforms["blue"][ tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "blue", "guard"));
        
        tank_team_count = 0;


        for(let i = 0; i < scout_amount; ++i )
        {
            tanks.push(new IA_Tank(platforms["red"][ tank_team_count++ ].position, this.select_random_weapon(), this.tank_index++, "red", "scout"));
            for(let j = 0; j < escolt_for_every_scout; ++j )
                tanks.push(new IA_Tank(platforms["red"][ tank_team_count++ ].position, this.select_random_weapon(), this.tank_index++, "red", "escolt",  tanks[ tanks.length -1 -j ]));
        }

        for(let i = 0; i < guard_amount; ++i)
            tanks.push(new IA_Tank(platforms["red"][ tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "red", "guard"));

        tank_team_count = 0;

        for(let i = 0; i < scout_amount; ++i)
        {
            tanks.push(new IA_Tank(platforms["green"][tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "green", "scout"));
            for(let j = 0; j < escolt_for_every_scout; ++j)
                tanks.push(new IA_Tank(platforms["green"][tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "green", "escolt",  tanks[ tanks.length -1 -j ]));
        }

        for(let i = 0; i < guard_amount; ++i)
            tanks.push(new IA_Tank(platforms["green"][tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "green", "guard"));


        tank_team_count = 0;

        for(let i = 0; i < scout_amount; ++i)
        {
            tanks.push(new IA_Tank(platforms["black"][tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "black", "scout"));
            for(let j = 0; j < escolt_for_every_scout; ++j)
                tanks.push(new IA_Tank(platforms["black"][tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "black", "escolt",  tanks[ tanks.length -1 -j ]));
        }

        for(let i = 0; i < guard_amount; ++i)
            tanks.push(new IA_Tank(platforms["black"][tank_team_count++].position, this.select_random_weapon(), this.tank_index++, "black", "guard"));

        this.ordered_tanks = [...tanks];
    }

    create_platforms()
    {

        platforms["green"] = [];
        platforms["green"].push (new Prop( { x: 4610, y: 2760 }, "left_spawn_point"));
        platforms["green"].push (new Prop( { x: 4610, y: 2880 }, "left_spawn_point"));
        platforms["green"].push (new Prop( { x: 4610, y: 2640 }, "left_spawn_point"));
    
        platforms["green"].push (new Prop( { x: 4610, y: 2045 }, "left_spawn_point"));
        platforms["green"].push (new Prop( { x: 4610, y: 1920 }, "left_spawn_point"));
        platforms["green"].push (new Prop( { x: 4610, y: 2170 }, "left_spawn_point"));

        platforms["black"] = [];
        platforms["black"].push (new Prop( { x: 2760, y: 4610 }, "top_spawn_point"));
        platforms["black"].push (new Prop( { x: 2880, y: 4610 }, "top_spawn_point"));
        platforms["black"].push (new Prop( { x: 2640, y: 4610 }, "top_spawn_point"));
    
        platforms["black"].push (new Prop( { x: 2045, y: 4610 }, "top_spawn_point"));
        platforms["black"].push (new Prop( { x: 1920, y: 4610 }, "top_spawn_point"));
        platforms["black"].push (new Prop( { x: 2170, y: 4610 }, "top_spawn_point"));

        platforms["red"] = [];
        platforms["red"].push (new Prop( { x: 170, y: 2760 }, "right_spawn_point"));
        platforms["red"].push (new Prop( { x: 170, y: 2880 }, "right_spawn_point"));
        platforms["red"].push (new Prop( { x: 170, y: 2640 }, "right_spawn_point"));
    
        platforms["red"].push (new Prop( { x: 170, y: 2045 }, "right_spawn_point"));
        platforms["red"].push (new Prop( { x: 170, y: 1920 }, "right_spawn_point"));
        platforms["red"].push (new Prop( { x: 170, y: 2170 }, "right_spawn_point"));


        platforms["blue"] = [];
        platforms["blue"].push (new Prop( { x: 2760, y: 170 }, "down_spawn_point"));
        platforms["blue"].push (new Prop( { x: 2880, y: 170 }, "down_spawn_point"));
        platforms["blue"].push (new Prop( { x: 2640, y: 170 }, "down_spawn_point"));
    
        platforms["blue"].push (new Prop( { x: 2045, y: 170 }, "down_spawn_point"));
        platforms["blue"].push (new Prop( { x: 1920, y: 170 }, "down_spawn_point"));
        platforms["blue"].push (new Prop( { x: 2170, y: 170 }, "down_spawn_point"));
    }


}