//#region Missile class

class Missile extends Weapon
{
    //#region Initialization methods

    constructor(owner)
    {
        super("Missile", owner, 0.25);
        //Create the Missile bullet
        this.load_ammo();
        this.time_from_explosion = 0.6;
        this.time_from_fire = 0;
    }

    load_ammo() { this.bullet = new MissileBullet(this); }

    //#endregion

    //#region Missile state methods

    update_cooldown(delta_time)
    {
        this.elapsed_time_since_last_shoot += delta_time;
        if(this.elapsed_time_since_last_shoot > this.cooldown)
        {
            this.is_on_cooldown = false;
            this.elapsed_time_since_last_shoot = 0;
        }
    }

    reset()
    {
        this.bullet.is_active = false;
        this.enable_control();
    }

    is_active() { return this.bullet.is_active; }
    disable_control() { this.bullet.is_controled = false; }
    enable_control() { this.bullet.is_controled = true; }

    //#endregion

    //#region Fire and bullet manipulation methods

    try_fire()
    {
        if(this.bullet.is_in_movement && !this.is_on_cooldown)      
        {
            this.bullet.explode();   
            console.log("set from try fire");
            this.owner.set_camera_target(this.owner); 
        }
   
        else if (this.time_from_explosion > 0.5)
            //if(!this.is_on_cooldown )       
            if(!this.bullet.is_in_movement)
            {
                this.fire();
                console.log("set from try fire dwn");

                this.owner.set_camera_target(this.bullet);
            }               
    }

    fire()
    {
        if(this.owner.type == "IA Tank")
        {
            if(this.owner.player_is_near)
                Audio_Manager.instance.play_rocket_shoot();

        }
        else
        {
            Audio_Manager.instance.play_rocket_shoot();
        }



        this.reset_bullet_position();

        //Update the direction and rotation and of the bullet
        this.bullet.rotation = this.owner.get_cannon_angle() - Math.PI;
       
        this.bullet.direction.set
        (
            Math.sin(  this.bullet.rotation ) * MissileBullet.speed,
            Math.cos(  this.bullet.rotation ) * MissileBullet.speed
        );

        
        this.bullet.body.SetLinearVelocity(this.bullet.direction);

      
        
        this.bullet.fire();

        this.owner.disable_control();
        this.is_on_cooldown = true;
    }

    reset_bullet_position()
    {
        //Put the bullet in the starting position
        var bullet_transform = 
        {
            position: {x: this.owner.get_fire_point().x / scale , y: (canvas_array[0].height - this.owner.get_fire_point().y)  / scale},
            GetAngle: function(){return 0;}
        }
        this.bullet.body.SetTransform(bullet_transform);

         //Make the image of the tank follow the body
         let bodyPosition = new Vector2(this.bullet.body.GetPosition().x, this.bullet.body.GetPosition().y);
         this.bullet.position = bodyPosition.from_box_scale_to_world();   
    }

    //#endregion

   //#region Update adn draw methods

    update(delta_time)
    {
        this.time_from_explosion += delta_time;
        this.time_from_fire += delta_time;
        this.bullet_update(delta_time);
        if(this.is_on_cooldown)
            this.update_cooldown(delta_time);
    }
    
    bullet_update(delta_time) { this.bullet.update(delta_time); }
    draw(ctx) { this.bullet_draw(ctx); }
    bullet_draw(ctx) { this.bullet.draw(ctx); }

    //#endregion
}

//#endregion

//#region MissileBullet class

class MissileBullet extends Bullet
{
    //#region Constructor

    constructor(parent)
    {
        super(parent, "Missile Bullet", 20, 30);

    
        MissileBullet.speed = 5;
        /*MissileBullet.height = 25;
        MissileBullet.width = 55;*/

        MissileBullet.bullet_pivot = 
        {
            x: -Missile.canvas.width >> 1,  //  /2
            y: -Missile.canvas.height >> 1
        };

        this.rotation_speed = 5;

        //True from start of shoot to collision (false on explosion animation)
        this.is_in_movement = false;


        this.animations = [];     
        this.animation_index = 0;

        this.is_controled = true;
        this.is_active = false;
    }

    //#endregion

    //#region Movement methods

    move()
    {
        //The vector of the direction of movement is rotated based on the angle of rotation, so it can move based on his rotation direction
        //Formula of vector rotation: x = sinα * L || y = cosα * L

        //Make the image of the tank follow the body
        let bodyPosition = new Vector2(this.body.GetPosition().x, this.body.GetPosition().y);
        this.position = bodyPosition.from_box_scale_to_world();   
    }

    stop()
    {
        this.is_in_movement = false;
        this.body.SetLinearVelocity(new b2Vec2(0,0));    
    }

    rotate(delta_time)
    {
        let rotation_input = 0;
        if (this.parent.owner.left_input())
            rotation_input -= 1;
        if (this.parent.owner.right_input())
            rotation_input += 1;

        if(rotation_input == 0 && !this.parent.owner.keyboard_control && gamepads[this.parent.owner.pad_index] != undefined)
            rotation_input = gamepads[this.parent.owner.pad_index].axes[0];
         

        if(rotation_input != 0) 
        {
            this.direction.set
            (
                Math.sin(  this.rotation ) * MissileBullet.speed,
                Math.cos(  this.rotation ) * MissileBullet.speed
            );
    
            this.body.SetLinearVelocity(this.direction);
            this.rotation += rotation_input * this.rotation_speed * delta_time;
        }
    }

    //#endregion

    //#region Action methods (fire and explosion)

    fire()
    {
        console.log("fire missile");
        this.is_in_movement = true;
        this.is_active = true;
    }

    explode()
    {
        this.parent.time_from_explosion = 0; 

        this.stop();
        this.animations.push(
            {animation: new SSAnimation(graphics.explosion.image, 64, 64, [5,5,5,5,5], 1/24, false), position: {x: this.position.x, y: this.position.y}
        });
        this.animations[this.animation_index++].animation.play_animation( (function(){ this.is_inactive=true; }).bind(this));
        this.parent.owner.enable_control();
    }
    
   //#endregion
    
    //#region Update and draw methods

    update(delta_time)
    {
        if(this.is_in_movement)
        {
            if(this.is_controled)
                this.rotate(delta_time);
            this.move();
        }
        this.play_explosion_animations(delta_time);
    }

    draw(ctx)
    {
        if(this.is_in_movement)
        {
            ctx.save();
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.rotation - 1.56);


            ctx.drawImage(Missile.canvas, MissileBullet.bullet_pivot.x, MissileBullet.bullet_pivot.y);
            ctx.restore();
        }
        
        for(let i = 0 ; i < this.animations.length; ++i)
        {
            ctx.save();
            ctx.translate(this.animations[i].position.x + .5 | 0,this.animations[i].position.y + .5 | 0);

            this.animations[i].animation.Draw(ctx);
            ctx.restore();
        }
    }

    //#endregion

    //#region Animation management

    play_explosion_animations(delta_time)
    {
        for(let i = 0 ; i < this.animations.length; ++i)
            if(!this.animations[i].animation.ended)
                this.animations[i].animation.Update(delta_time);
            else //If the animation end it delete it from the animation array
            {
                this.animations.splice(i,1);   
                --this.animation_index;    
            }
    }

    //#endregion

    //#region Collision methods

    on_collision_enter(other)
    {
        if(this.is_in_movement && (other.type == "Wall" || other.type == "Tank" || other.type == "IA Tank"))
        {
            this.explode();
            console.log("set from on collision fire");

            this.parent.owner.set_camera_target(this.parent.owner);
            game_scene.delete_body(this.body);
        }
    }

    //#endregion
}

//#endregion