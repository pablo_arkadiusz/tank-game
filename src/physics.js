// auxiliar code for working with Box2D

// Box2D lib
var b2Vec2 = Box2D.Common.Math.b2Vec2
    ,   b2AABB = Box2D.Collision.b2AABB
    ,   b2BodyDef = Box2D.Dynamics.b2BodyDef
    ,   b2Body = Box2D.Dynamics.b2Body
    ,   b2FixtureDef = Box2D.Dynamics.b2FixtureDef
    ,   b2Fixture = Box2D.Dynamics.b2Fixture
    ,   b2World = Box2D.Dynamics.b2World
    ,   b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
    ,   b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
    ,   b2DebugDraw = Box2D.Dynamics.b2DebugDraw
    ,   b2MouseJointDef =  Box2D.Dynamics.Joints.b2MouseJointDef
    ,   b2Shape = Box2D.Collision.Shapes.b2Shape
    ,   b2RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef
    ,   b2Joint = Box2D.Dynamics.Joints.b2Joint
    ,   b2PrismaticJointDef = Box2D.Dynamics.Joints.b2PrismaticJointDef
    ,   b2DistanceJointDef = Box2D.Dynamics.Joints.b2DistanceJointDef
    ,   b2PulleyJointDef = Box2D.Dynamics.Joints.b2PulleyJointDef
    ;

// 1 meter = 100 pixels
var scale = 100;
var gravity;
var world;
var world2;

var world_array = []
// aux function for creating boxes
function CreateBox (x, y, width, height, options, collision_set)
{
    // default values
    let defaultOptions = {
    	density : 1.0,
    	friction: 1.0,
    	restitution : 0.5,
 
    	linearDamping : 0.0,
    	angularDamping: 0.0,
    	fixedRotation : true,
 
    	type : b2Body.b2_dynamicBody
    }
    options = Object.assign(defaultOptions, options);

    // Fixture: define physics properties (density, friction, restitution)
	var fix_def = new b2FixtureDef();
 
	fix_def.density = options.density;
	fix_def.friction = options.friction;
	fix_def.restitution = options.restitution;
 
	// Shape: 2d geometry (circle or polygon)
	fix_def.shape = new b2PolygonShape();
 
	fix_def.shape.SetAsBox(width, height);

	fix_def.filter.categoryBits = collision_set.collision;
    fix_def.filter.maskBits = collision_set.layer;


    // Body: position of the object and its type (dynamic, static o kinetic)
	var body_def = new b2BodyDef();
	body_def.position.Set(x, y);
 
	body_def.linearDamping = options.linearDamping;
	body_def.angularDamping = options.angularDamping;
 
	body_def.type = options.type; // b2_dynamicBody
	body_def.userData = options.user_data;
 
	//Problemas
	var b = world_array[0].CreateBody(body_def);
	var f = b.CreateFixture(fix_def);
 
	return b;
}

// aux function for creating balls
function CreateBall (x, y, radius, options, collision_set)
{
	// default values
    let defaultOptions = {
    	density : 1.0,
    	friction: 1.0,
    	restitution : 0.5,
 
    	linearDamping : 0.1,
    	angularDamping: 0.1,
    	fixedRotation : true,
 
    	type : b2Body.b2_dynamicBody
    }
	options = Object.assign(defaultOptions, options);
	
    var body_def = new b2BodyDef();
    var fix_def = new b2FixtureDef;

	fix_def.density = options.density;
	fix_def.friction = options.friction;
	fix_def.restitution = options.restitution;

    // Shape: 2d geometry (circle or polygon)
    var shape = new b2CircleShape(radius);
	fix_def.shape = shape;
	
	body_def.position.Set(x, y);

	fix_def.filter.categoryBits = collision_set.collision;
    fix_def.filter.maskBits = collision_set.layer;

    // friction
	body_def.linearDamping = options.linearDamping;
	body_def.angularDamping = options.angularDamping;

    body_def.type = b2Body.b2_dynamicBody;
    body_def.userData = options.user_data;


	var b = world_array[0].CreateBody(body_def);

    var f = b.CreateFixture(fix_def);

    return b;
}

// Create a Box2D world object
function CreateWorld(ctx, gravity)
{
	var doSleep = false;
	var world = new b2World(gravity, doSleep);
 
	// DebugDraw is used to create the drawing with physics
	var debugDraw = new b2DebugDraw();
	debugDraw.SetSprite(ctx);
	debugDraw.SetDrawScale(scale);
	debugDraw.SetFillAlpha(0.5);
	debugDraw.SetLineThickness(1.0);
	debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
 	
	world.SetDebugDraw(debugDraw);
 
	return world;
}

function PreparePhysics(ctx_array)
{
    // create the gravity vector("down" with force 10)
    gravity = new b2Vec2(0, 0);

	for(let i = 0; i < ctx_array.length; ++i)
		world_array[i] = CreateWorld(ctx_array[i], gravity);
    // create the world
	//world = CreateWorld(ctx, gravity);

	// prepare the collision event function
	Box2D.Dynamics.b2ContactListener.prototype.BeginContact = on_contact_begin;
	Box2D.Dynamics.b2ContactListener.prototype.EndContact = on_contact_end;
}


function on_contact_begin (contact)
{
	var fixture_a = contact.GetFixtureA();
	var fixture_b = contact.GetFixtureB();

	var a = contact.GetFixtureA().GetBody().GetUserData();
	var b = contact.GetFixtureB().GetBody().GetUserData();	


    if (a != null && b != null && typeof(a.type) !== 'undefined' && typeof(b.type) !== 'undefined')
    {	
		if(typeof a.on_collision_enter === 'function') 
			a.on_collision_enter(b, fixture_a, fixture_b);
		if(typeof b.on_collision_enter === 'function') 
			b.on_collision_enter(a ,fixture_b, fixture_a);	

	}
}

function on_contact_end (contact)
{
	var fixture_a = contact.GetFixtureA();
	var fixture_b = contact.GetFixtureB();
	var a = contact.GetFixtureA().GetBody().GetUserData();
	var b = contact.GetFixtureB().GetBody().GetUserData();	


    if (a != null && b != null && typeof(a.type) !== 'undefined' && typeof(b.type) !== 'undefined')
    {	
		
		
		if(typeof a.on_collision_leave === 'function') 
			a.on_collision_leave(b, fixture_b,fixture_a);	
		if(typeof b.on_collision_leave === 'function') 
			b.on_collision_leave(a, fixture_a, fixture_b);		
		
	}
}


