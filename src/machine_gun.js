//#region Machine gun class

/*
    * Machine gun weapon that the tanks can posses
    * that fires bullets fast, the bullets bounce when they 
    * hit the wall, and disappear after some time
*/

class MachineGun extends Weapon
{
    //#region Initialization methods

    constructor(owner)
    {
        super("Machine gun", owner, 0.05);

        MachineGun.max_bullets = 50;

        //Index to select the bullet to fire
        this.bullet_index = 0;

        //Creates all the bullets
        this.load_ammo();  

        this.active_bullets = 0;

    }
    
    load_ammo()
    {
        this.bullets = new Array(MachineGun.max_bullets);

        for(let i = 0; i < MachineGun.max_bullets; ++i )
            this.bullets[i] = new MachineGun_Bullet(this);
    }
    
    //#endregion

    //#region Fire methods

    try_fire()
    {
        if(!this.is_on_cooldown) this.fire();
        this.is_on_cooldown=true;
    }

    fire()
    {
        
        if(this.owner.type == "IA Tank")
        {
            if(this.owner.player_is_near)
                Audio_Manager.instance.play_machine_shoot();

        }
        else
        {
            Audio_Manager.instance.play_machine_shoot();
        }


        //Put the bullet in the starting position
        let bullet_pos = this.owner.get_fire_point().from_world_to_box_scale();
        var bullet_transform = 
        {
            position: {x: bullet_pos.x , y: bullet_pos.y},
            GetAngle: function(){return 0;}
        }

        this.bullets[this.bullet_index].body.SetTransform(bullet_transform);

        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.bullets[this.bullet_index].body.GetPosition().x,  this.bullets[this.bullet_index].body.GetPosition().y);
        this.bullets[this.bullet_index].position = bodyPosition.from_box_scale_to_world();  

        //Update the direction and rotation and of the bullet
        let cannon_angle = this.owner.get_cannon_angle();

        let x_dir = -Math.sin(  cannon_angle );
        let y_dir = -Math.cos( cannon_angle );

        let angle = (Math.random() * -30) + 15;  
        let dir = rotateVector({x: x_dir, y: y_dir}, angle);
        this.bullets[this.bullet_index].direction.set
        (
            dir.x * MachineGun_Bullet.speed,
            dir.y * MachineGun_Bullet.speed
        );

        this.bullets[this.bullet_index].body.SetLinearVelocity( this.bullets[this.bullet_index].direction);

        //this.bullets[this.bullet_index].body.ApplyForce( this.bullets[this.bullet_index].direction , this.bullets[this.bullet_index].body.GetWorldCenter());

        this.bullets[this.bullet_index].rotation = this.owner.get_bullet_rotation();

        if( !this.bullets[this.bullet_index].active)
        {
             //Activate the fired bulled. When its active it will call the bullet move method
            this.bullets[this.bullet_index].active = true;
            ++this.active_bullets;
        }
    
        //Update the bullet index
        if(++this.bullet_index == MachineGun.max_bullets)
            this.bullet_index = 0;     
    }

   //#endregion

    //#region Updates and Draw methods

    update(deltaTime)
    {
       
        if(this.is_on_cooldown)
            this.update_cooldown(deltaTime);
        if(this.is_active())
            this.bullet_update(deltaTime);
        
    }

    update_cooldown(deltaTime)
    {       
        this.elapsed_time_since_last_shoot += deltaTime
        if(this.elapsed_time_since_last_shoot  > this.cooldown)
        {
            this.is_on_cooldown = false;
            this.elapsed_time_since_last_shoot = 0;
        }
    }

    //Calls the update method of all the bullets
    bullet_update(deltaTime)
    { this.bullets.forEach(element => { element.update(deltaTime); }); }

    draw(ctx)
    {      
        if(this.is_active())
            this.bullet_draw(ctx); 
    }
    
    bullet_draw(ctx) { this.bullets.forEach(element => { element.draw(ctx); }); }

    //#endregion

    //#region Machine gun state methods

    is_active() { return this.active_bullets != 0;}

    //#endregion
}

//#endregion

//#region Bullet class

//Bullet that the machine gun fires
class MachineGun_Bullet extends Bullet
{
    //#region Constructor
    constructor(parent)
    {
        super(parent, "Gun Bullet", 10, 1,false);

        MachineGun_Bullet.image =  graphics.missil_rifle.image;
        MachineGun_Bullet.speed = 6;
        /*MachineGun_Bullet.height = 50; Defined in offscreen_canvases.js
        MachineGun_Bullet.width = 50;*/

        MachineGun_Bullet.bullet_pivot = 
        {
            x: -MachineGun.canvas.width  >> 1, //  /2
            y: -MachineGun.canvas.height >> 1
        };

        //Time(seconds) in which the bullet will disappear
        MachineGun_Bullet.time_alive = 1.5;

        //Elapsed time since firing this bullet
        this.elapsed_time = 0;
    }


    //#endregion

    //#region Movement Methods

    move(deltaTime) 
    {
        //Moves the body
       // this.body.SetLinearVelocity(this.direction);

        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.body.GetPosition().x,  this.body.GetPosition().y);
        this.position = bodyPosition.from_box_scale_to_world();  
 
    }

    stop() { this.body.SetLinearVelocity(new b2Vec2(0,0)); }

    //#endregion

    //#region Updates and Draw methods

    update(deltaTime)
    {
        if(this.active)
        { 
            this.move(deltaTime);
            this.update_life_timer(deltaTime); 
        }
    }

    //Makes the bullet dissapear when the time_alive period ends
    update_life_timer(deltaTime)
    {
        this.elapsed_time += deltaTime;
        if(this.elapsed_time > MachineGun_Bullet.time_alive)
            this.deactivate();    
        
    }
    
    draw(ctx)
    {   

        if(this.active)
        {
            ctx.save();
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.rotation);

            ctx.drawImage(MachineGun.canvas, MachineGun_Bullet.bullet_pivot.x, MachineGun_Bullet.bullet_pivot.y);
            ctx.restore();
        }   
    }

    //#endregion

    //#region  Bullet activation methods

    deactivate()
    {
        this.elapsed_time = 0;
        this.active = false;
        --this.parent.active_bullets;
        this.stop();
        
        game_scene.delete_body(this.body);

    }

    //#endregion

    //#region Collision methods

    on_collision_enter(other)
    {

    }


    bounce(wall)
    {
        /*let angle = wall.rotation;

        let wall_half_width = wall.width/2, wall_half_height = wall.height/2,
            bullet_pos = this.position, wall_pos = wall.position;

        let wall_bullet_header = bullet_pos.subtract(wall_pos); 

        let wall_horizontal_axis = new Vector2(Math.cos(angle), -Math.sin(angle));  	
        let wall_vertical_axis = new Vector2(Math.sin(angle), Math.cos(angle)); 

        // Project wall_bullet_header onto wall_horizontal_axis to get distance along wall_horizontal_axis from bullet_pos
        let horizontal_axis_distance = wall_bullet_header.dot(wall_horizontal_axis);
        
        if(horizontal_axis_distance > wall_half_width)    horizontal_axis_distance = wall_half_width;
        if(horizontal_axis_distance < -wall_half_width)   horizontal_axis_distance = -wall_half_width;

        // Project wall_bullet_header onto wall_vertical_axis to get distance along wall_vertical_axis from bullet_pos
        let dy = wall_bullet_header.dot(wall_vertical_axis);

        if(dy > wall_half_height)    dy = wall_half_height;
        if(dy < -wall_half_height)   dy = -wall_half_height;
    
        //Headers vector along horizontal/vertical axis from wall position to the point of collision
        let horizontal_header = wall_horizontal_axis.scale(horizontal_axis_distance);
        let vertical_header = wall_vertical_axis.scale(dy);

        //Unification of the two headers in one
        let header = horizontal_header.add(vertical_header);

        //The collision point is the sum of the wall position with the header vector
        let  collision_point = wall_pos.add(header);

        //The normal vector of the collision wall
        let normal = bullet_pos.subtract(collision_point).normalize();

        if(normal.is_zero())
            return;
        
       
        if(angle == 0)
        {
            //The new direction of the bullet is the reflection between the old direction vector and the normal vector
            let reflected_vector = this.direction.reflect(normal);	      
            this.direction = reflected_vector;

        }
        else//If the axis is not the x and y (when the wall has angle of rotation) it is used other formula to make reflections (vector proyection)
        { 
            var direction = box_to_normal_direction(this.direction);

            let proyected_vector = normal.scale(normal.dot(direction) / normal.dot(normal));

            let reflected_vector = direction.subtract(proyected_vector.scale(2));
            this.direction = normal_to_box_direction(reflected_vector);

        }
        this.rotation = Math.atan2(this.direction.x, this.direction.y);*/
    }

    //#endregion
}



//#endregion