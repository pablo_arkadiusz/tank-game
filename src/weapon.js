class Weapon
{
    constructor(type, owner, cooldown)
    {
        //#region Fake abstract class

        if (this.constructor === Weapon)
        { throw new TypeError('Abstract class "Weapon" cannot be instantiated directly.'); }

        if (this.load_ammo === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement load_ammo method'); }
        
        if (this.try_fire === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement try_fire method'); }

        if (this.fire === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement fire method'); }

        if (this.update_cooldown === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement update_cooldown method'); }

        if (this.is_active === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement is_active method'); }

        if (this.update === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement update method'); }

        if (this.bullet_update === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement bullet_update method'); }

        if (this.draw === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement draw method'); }

        if (this.bullet_draw === undefined)
        { throw new TypeError('Classes extending the Weapon abstract class need implement bullet_draw method'); }

        //#endregion

        //#region Class attributes

        this.type = type;
        this.owner = owner;  //The tank that owns this weapon
        this.cooldown = cooldown;
        this.is_on_cooldown = false;
        this.elapsed_time_since_last_shoot = 0;

        //#endregion
    }
}