//#region Machine gun class

/*
    * Machine gun weapon that the tanks can posses
    * that fires bullets fast, the bullets bounce when they 
    * hit the wall, and disappear after some time
*/

class AutoMissile extends Weapon
{
    //#region Initialization methods

    constructor(owner)
    {
        super("AutoMissile", owner, 0.5);

        AutoMissile.max_bullets = 10;

        //Index to select the bullet to fire
        this.bullet_index = 0;

        //Creates all the bullets
        this.load_ammo();  

        this.active_bullets = 0;
    }
    
    load_ammo()
    {
        this.bullets = new Array(AutoMissile.max_bullets);

        for(let i = 0; i < AutoMissile.max_bullets; ++i )
            this.bullets[i] = new AutoMissileBullet(this);
    }
    

    //#endregion


    //#region Fire methods

    try_fire()
    {
        if(!this.is_on_cooldown) this.fire();
        this.is_on_cooldown=true;
    }

    reset_bullet_pos()
    {
        //Put the bullet in the starting position
        let bullet_pos = this.owner.get_fire_point().from_world_to_box_scale();
        let bullet_transform = 
        {
            position: {x: bullet_pos.x , y: bullet_pos.y},
            GetAngle: function(){return 0;}
        }

        this.bullets[this.bullet_index].body.SetTransform(bullet_transform);

        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.bullets[this.bullet_index].body.GetPosition().x,  this.bullets[this.bullet_index].body.GetPosition().y);
        this.bullets[this.bullet_index].position = bodyPosition.from_box_scale_to_world();  
        this.bullets[this.bullet_index].body.SetLinearVelocity( this.bullets[this.bullet_index].direction);

    }

    reset_bullet_variables()
    {
        this.bullets[this.bullet_index].time = 0;
        this.bullets[this.bullet_index].time_diff =  Math.random() *-2 +1;
    }

    reset_bullet_rotation_and_direction()
    {
        this.bullets[this.bullet_index].rotation = this.owner.get_bullet_rotation();

        let x_dir = Math.sin(  this.bullets[this.bullet_index].rotation );
        let y_dir = Math.cos( this.bullets[this.bullet_index].rotation );

        this.bullets[this.bullet_index].rotation -= PIH; 

        this.bullets[this.bullet_index].real_direction.set
        (
            x_dir * AutoMissileBullet.speed,
            y_dir * AutoMissileBullet.speed
        );
        this.bullets[this.bullet_index].actual_rot = this.bullets[this.bullet_index].rotation;
    }

    fire()
    {


        if(this.owner.type == "IA Tank")
        {
            if(this.owner.player_is_near)
                Audio_Manager.instance.play_rocket_shoot();
        }
        else
            Audio_Manager.instance.play_rocket_shoot();
        


        this.reset_bullet_pos();
        this.reset_bullet_variables();
        this.reset_bullet_rotation_and_direction();  

        if( !this.bullets[this.bullet_index].active)
        {
             //Activate the fired bulled. When its active it will call the bullet move method
            this.bullets[this.bullet_index].active = true;
            ++this.active_bullets;
        }
        
        //Update the bullet index
        if(++this.bullet_index == AutoMissile.max_bullets)
            this.bullet_index = 0;     
    }

   //#endregion

    //#region Updates and Draw methods

    update(deltaTime)
    {
        if(this.is_on_cooldown)
            this.update_cooldown(deltaTime);
        if(this.is_active())
            this.bullet_update(deltaTime);   
    }

    update_cooldown(deltaTime)
    {       
        this.elapsed_time_since_last_shoot += deltaTime
        if(this.elapsed_time_since_last_shoot  > this.cooldown)
        {
            this.is_on_cooldown = false;
            this.elapsed_time_since_last_shoot = 0;
        }
    }

    //Calls the update method of all the bullets
    bullet_update(deltaTime)
    { this.bullets.forEach(element => { element.update(deltaTime); }); }

    draw(ctx)
    {      
        this.bullets.forEach(element => { element.draw(ctx); }); 
    }
    
    bullet_draw(ctx) { this.bullet.draw(ctx); }

    //#endregion

    //#region Machine gun state methods

    is_active() { return this.active_bullets != 0;}

    //#endregion
}

//#endregion

//#region Bullet class

//Bullet that the machine gun fires
class AutoMissileBullet extends Bullet
{
    //#region Constructor
    constructor(parent)
    {
        super(parent, "Gun Bullet", 10,5);

        AutoMissileBullet.speed = 4;
        /*AutoMissileBullet.height = 30;
        AutoMissileBullet.width = 30;*/

        AutoMissileBullet.bullet_pivot = 
        {
            x: -AutoMissile.canvas.width  >> 1, //  /2
            y: -AutoMissile.canvas.height >> 1
        };


        AutoMissileBullet.sensor_radius = 200;
        //Elapsed time since firing this bullet
        this.create_sensor();

        this.target = null;
        this.actual_rot = 0;

        this.real_direction = new Vector2();
        this.angle_speed = 0.03;


        this.frequency = 3;  // Speed of sine movement
        this.magnitude = 0.5;   // Size of sine movement

        this.time = 0;

        this.final_rotation = this.rotation;
        this.time_diff;
    }

    create_sensor()
    {
         let shape = new b2CircleShape();
         shape.m_radius = (AutoMissileBullet.sensor_radius/1.5) / scale;

        let options = {
            density : 1.0,
            friction: 1.0,
            restitution : 0.5,
            }

        // Fixture: define physics properties (density, friction, restitution)
        var fix_def = new b2FixtureDef();
        fix_def.shape = shape;

        fix_def.density = options.density;
        fix_def.friction = options.friction;
        fix_def.restitution = options.restitution;
            
    
        fix_def.filter.categoryBits = collision_rule.SENSOR.collision;
        fix_def.filter.maskBits = collision_rule.SENSOR.layer;
        this.sensor = this.body.CreateFixture(fix_def);
        this.sensor.SetUserData("Sensor");
        this.sensor.SetSensor(true);     
    }

    //#endregion

    //#region Movement Methods

    move(delta_time) 
    {

        if(this.target != null)
        {
            if(this.angle_speed > 0)
                this.angle_speed += Math.random() < 0.5 ? 0.005 : -0.005;
            else
                this.angle_speed += 0.0005;

            let new_dir = this.target.position.subtract(this.position);
            this.direction = new_dir.normalize().scale(AutoMissileBullet.speed);
            this.rotation = Math.atan2( this.direction.y, this.direction.x);
            
            if(new_dir.magnitude() > AutoMissileBullet.sensor_radius)
            {
                this.target = null;
                this.angle_speed = 0.03;
            }
        }

        this.final_rotation = this.actual_rot  +  Math.sin((this.time + this.time_diff )* this.frequency) *  this.magnitude;

        this.real_direction.set
        (
            Math.sin( this.final_rotation + PIH) * AutoMissileBullet.speed,
            Math.cos( this.final_rotation + PIH) * AutoMissileBullet.speed
        );

        this.actual_rot = angleLerp(  this.actual_rot, this.rotation ,this.angle_speed);

        //Moves the body
        this.body.SetLinearVelocity(this.real_direction);
        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.body.GetPosition().x,  this.body.GetPosition().y);
        this.position = bodyPosition.from_box_scale_to_world();  
    }

    stop() { this.body.SetLinearVelocity(new b2Vec2(0,0)); }

    //#endregion

    //#region Updates and Draw methods

    update(deltaTime)
    {
        this.time += deltaTime;
        if(this.active)
            this.move(deltaTime);  
    }

  
    
    draw(ctx)
    {   
        if(this.active)
        {
            ctx.save();
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.final_rotation );

            ctx.drawImage(AutoMissile.canvas, AutoMissileBullet.bullet_pivot.x, AutoMissileBullet.bullet_pivot.y);
            ctx.restore();
        }   
    }

    //#endregion

    //#region  Bullet activation methods

    deactivate()
    {
        this.angle_speed = 0.03;
        this.target = null;
        this.elapsed_time = 0;
        this.active = false;
        --this.parent.active_bullets;
        this.stop();
        
        game_scene.delete_body(this.body);

    }

    //#endregion


    //#region Collision methods

    on_collision_enter(other, this_fixture)
    {
        if(this.active)
        {
            if(this_fixture.GetUserData() != "Sensor" )
            {
                if(other.type == "Wall")
                {
                    this.deactivate();
                }

                if(other.type == "Tank" || other.type == "IA Tank")
                {
                    this.deactivate();
                }
            
            }
            else if(other != this.parent.owner)
            {
                this.target = other;
            }
        }
    }
    //#endregion
}

//#endregion