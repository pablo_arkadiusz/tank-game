class Health_Bar
{
    draw_health_bar(ctx,x,y,width,height,health,max_health)
    {
        let color_number = Math.round((1-(health/max_health)) * 0xff) * 0x10000+ Math.round((health/max_health) * 0xff)*0x100;

        let color_string = color_number.toString(16);
        if(color_number >= 100000)
        {
            ctx.fillStyle = '#'+color_string;
        }
        else if(color_number << 0x1000000 && color_number >= 100000)
        {
            ctx.fillStyle = '#0'+color_string;

        }
        else if(color_number << 0x100000)
        {
            ctx.fillStyle = '#00'+color_string;
        }

        ctx.fillRect(x+1,y+1,(health/max_health) * (width-1), height -2);
    }
}