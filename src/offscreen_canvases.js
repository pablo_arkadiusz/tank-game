var tile_offscreen_canvas = null;
function create_tile_offscreen_canvas()
{
    tile_offscreen_canvas = document.createElement('canvas');
    tile_offscreen_canvas.width = graphics.grass.image.width *20;
    tile_offscreen_canvas.height = graphics.grass.image.height *20;
    var context = tile_offscreen_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas

    for(let i = 0; i < 19; ++i)
        for(let j = 0; j < 19; ++j)
            context.drawImage(graphics.grass.image,graphics.grass.image.naturalWidth * i, graphics.grass.image.naturalHeight *j);


    for(let i = 14; i < 23; ++i)
        for(let j = 0; j < 6; ++j)
            context.drawImage(graphics.decor_tile_b_05.image,graphics.decor_tile_b_05.image.width * i + 25, graphics.decor_tile_b_05.image.height *j - 40);


    for(let i = 14; i < 23; ++i)
        for(let j = 0; j < 6; ++j)
            context.drawImage(graphics.decor_tile_b_05.image,graphics.decor_tile_b_05.image.width * j -40 , graphics.decor_tile_b_05.image.height *i + 25);

    for(let i = 14; i < 23; ++i)
        for(let j = 0; j < 6; ++j)
            context.drawImage(graphics.decor_tile_b_05.image, graphics.decor_tile_b_05.image.width * i + 25, 4800 - graphics.decor_tile_b_05.image.height *j - 80);


    for(let i = 14; i < 23; ++i)
        for(let j = 0; j < 6; ++j)
            context.drawImage(graphics.decor_tile_b_05.image,4800 - graphics.decor_tile_b_05.image.height *j - 80, graphics.decor_tile_b_05.image.width * i + 25);
}


function create_blockb2_offscreen_canvas()
{
    const image = graphics.block_b2.image;

    Wall.block_b_02_canvas = document.createElement('canvas');
    Wall.block_b_02_canvas.width = image.width;
    Wall.block_b_02_canvas.height = image.height;
    let ctx = Wall.block_b_02_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}


function create_log_offscreen_canvas()
{
    const image = graphics.wall_log.image;
    Wall.log_canvas = document.createElement('canvas');
    Wall.log_canvas.width = image.width;
    Wall.log_canvas.height = image.height;
    let ctx = Wall.log_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_cactus1_offscreen_canvas()
{
    const image = graphics.cactus1.image;
    Prop.cactus1_canvas = document.createElement('canvas');
    Prop.cactus1_canvas.width = image.width;
    Prop.cactus1_canvas.height = image.height;
    let ctx = Prop.cactus1_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}


function create_cactus2_offscreen_canvas()
{
    const image = graphics.cactus2.image;
    Prop.cactus2_canvas = document.createElement('canvas');
    Prop.cactus2_canvas.width = image.width;
    Prop.cactus2_canvas.height = image.height;
    let ctx = Prop.cactus2_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_platform_offscreen_canvas()
{
    const image = graphics.platform.image;
    Prop.platform_canvas = document.createElement('canvas');
    Prop.platform_canvas.width = image.width;
    Prop.platform_canvas.height = image.height;
    let ctx = Prop.platform_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_artifact_offscreen_canvas()
{
    const image = graphics.artifact.image;
    Prop.artifact_canvas = document.createElement('canvas');
    Prop.artifact_canvas.width = image.width;
    Prop.artifact_canvas.height = image.height;
    let ctx = Prop.artifact_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_artifactoff_offscreen_canvas()
{
    const image = graphics.artifact_off.image;
    Prop.artifact_off_canvas = document.createElement('canvas');
    Prop.artifact_off_canvas.width = image.width;
    Prop.artifact_off_canvas.height = image.height;
    let ctx = Prop.artifact_off_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_greenflagld_offscreen_canvas()
{
    const image = graphics.green_flag_ld.image;
    Prop.greenflagld_canvas = document.createElement('canvas');
    Prop.greenflagld_canvas.width = image.width;
    Prop.greenflagld_canvas.height = image.height;
    let ctx = Prop.greenflagld_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_greenflaglt_offscreen_canvas()
{
    const image = graphics.green_flag_lt.image;
    Prop.greenflaglt_canvas = document.createElement('canvas');
    Prop.greenflaglt_canvas.width = image.width;
    Prop.greenflaglt_canvas.height = image.height;
    let ctx = Prop.greenflaglt_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_greenflagdl_offscreen_canvas()
{
    const image = graphics.green_flag_dl.image;
    Prop.greenflagdl_canvas = document.createElement('canvas');
    Prop.greenflagdl_canvas.width = image.width;
    Prop.greenflagdl_canvas.height = image.height;
    let ctx = Prop.greenflagdl_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_greenflagdr_offscreen_canvas()
{
    const image = graphics.green_flag_dr.image;
    Prop.greenflagdr_canvas = document.createElement('canvas');
    Prop.greenflagdr_canvas.width = image.width;
    Prop.greenflagdr_canvas.height = image.height;
    let ctx = Prop.greenflagdr_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_greenflagtr_offscreen_canvas()
{ 
    const image = graphics.green_flag_tr.image;
    Prop.greenflagtr_canvas = document.createElement('canvas');
    Prop.greenflagtr_canvas.width = image.width;
    Prop.greenflagtr_canvas.height = image.height;
    let ctx = Prop.greenflagtr_canvas.getContext("2d");

    // draw the image on the hidden
    ctx.drawImage(image,0,0);
}
function create_greenflagtl_offscreen_canvas()
{
    const image = graphics.green_flag_tl.image;
    Prop.greenflagtl_canvas = document.createElement('canvas');
    Prop.greenflagtl_canvas.width = image.width;
    Prop.greenflagtl_canvas.height = image.height;
    let ctx = Prop.greenflagtl_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blackflagld_offscreen_canvas()
{
    const image = graphics.black_flag_ld.image;
    Prop.blackflagld_canvas = document.createElement('canvas');
    Prop.blackflagld_canvas.width = image.width;
    Prop.blackflagld_canvas.height = image.height;
    let ctx = Prop.blackflagld_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blackflaglt_offscreen_canvas()
{
    const image = graphics.black_flag_lt.image;
    Prop.blackflaglt_canvas = document.createElement('canvas');
    Prop.blackflaglt_canvas.width = image.width;
    Prop.blackflaglt_canvas.height = image.height;
    let ctx = Prop.blackflaglt_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blackflagrt_offscreen_canvas()
{
    const image = graphics.black_flag_rt.image;
    Prop.blackflagrt_canvas = document.createElement('canvas');
    Prop.blackflagrt_canvas.width = image.width;
    Prop.blackflagrt_canvas.height = image.height;
    let ctx = Prop.blackflagrt_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_blackflagrd_offscreen_canvas()
{
    const image = graphics.black_flag_rd.image;
    Prop.blackflagrd_canvas = document.createElement('canvas');
    Prop.blackflagrd_canvas.width = image.width;
    Prop.blackflagrd_canvas.height = image.height;
    let ctx = Prop.blackflagrd_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_blackflagtr_offscreen_canvas()
{
    const image = graphics.black_flag_tr.image;
    Prop.blackflagtr_canvas = document.createElement('canvas');
    Prop.blackflagtr_canvas.width = image.width;
    Prop.blackflagtr_canvas.height = image.height;
    let ctx = Prop.blackflagtr_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_blackflagtl_offscreen_canvas()
{
    const image = graphics.black_flag_tl.image;
    Prop.blackflagtl_canvas = document.createElement('canvas');
    Prop.blackflagtl_canvas.width = image.width;
    Prop.blackflagtl_canvas.height = image.height;
    let ctx = Prop.blackflagtl_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}



function create_redflagdl_offscreen_canvas()
{
    const image = graphics.red_flag_dl.image;
    Prop.redflagdl_canvas = document.createElement('canvas');
    Prop.redflagdl_canvas.width = image.width;
    Prop.redflagdl_canvas.height = image.height;
    let ctx = Prop.redflagdl_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_redflagdr_offscreen_canvas()
{
    const image = graphics.red_flag_dr.image;
    Prop.redflagdr_canvas = document.createElement('canvas');
    Prop.redflagdr_canvas.width = image.width;
    Prop.redflagdr_canvas.height = image.height;
    let ctx = Prop.redflagdr_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_redflagrt_offscreen_canvas()
{
    const image = graphics.red_flag_rt.image;
    Prop.redflagrt_canvas = document.createElement('canvas');
    Prop.redflagrt_canvas.width = image.width;
    Prop.redflagrt_canvas.height = image.height;
    let ctx = Prop.redflagrt_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_redflagrd_offscreen_canvas()
{
    const image = graphics.red_flag_rd.image;
    Prop.redflagrd_canvas = document.createElement('canvas');
    Prop.redflagrd_canvas.width = image.width;
    Prop.redflagrd_canvas.height = image.height;
    let ctx = Prop.redflagrd_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_redflagtr_offscreen_canvas()
{
    const image = graphics.red_flag_tr.image;
    Prop.redflagtr_canvas = document.createElement('canvas');
    Prop.redflagtr_canvas.width = image.width;
    Prop.redflagtr_canvas.height = image.height;
    let ctx = Prop.redflagtr_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_redflagtl_offscreen_canvas()
{
    const image = graphics.red_flag_tl.image;
    Prop.redflagtl_canvas = document.createElement('canvas');
    Prop.redflagtl_canvas.width = image.width;
    Prop.redflagtl_canvas.height = image.height;
    let ctx = Prop.redflagtl_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blueflagdl_offscreen_canvas()
{
    const image = graphics.blue_flag_dl.image;
    Prop.blueflagdl_canvas = document.createElement('canvas');
    Prop.blueflagdl_canvas.width = image.width;
    Prop.blueflagdl_canvas.height = image.height;
    let ctx = Prop.blueflagdl_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blueflagdr_offscreen_canvas()
{
    const image = graphics.blue_flag_dr.image;
    Prop.blueflagdr_canvas = document.createElement('canvas');
    Prop.blueflagdr_canvas.width = image.width;
    Prop.blueflagdr_canvas.height = image.height;
    let ctx = Prop.blueflagdr_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blueflagrd_offscreen_canvas()
{
    const image = graphics.blue_flag_rd.image;
    Prop.blueflagrd_canvas = document.createElement('canvas');
    Prop.blueflagrd_canvas.width = image.width;
    Prop.blueflagrd_canvas.height = image.height;
    let ctx = Prop.blueflagrd_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blueflagrt_offscreen_canvas()
{
    const image = graphics.blue_flag_rt.image;
    Prop.blueflagrt_canvas = document.createElement('canvas');
    Prop.blueflagrt_canvas.width = image.width;
    Prop.blueflagrt_canvas.height = image.height;
    let ctx = Prop.blueflagrt_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blueflaglt_offscreen_canvas()
{
    const image = graphics.blue_flag_lt.image;
    Prop.blueflaglt_canvas = document.createElement('canvas');
    Prop.blueflaglt_canvas.width = image.width;
    Prop.blueflaglt_canvas.height = image.height;
    let ctx = Prop.blueflaglt_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_blueflagld_offscreen_canvas()
{
    const image = graphics.blue_flag_ld.image;
    Prop.blueflagld_canvas = document.createElement('canvas');
    Prop.blueflagld_canvas.width = image.width;
    Prop.blueflagld_canvas.height = image.height;
    let ctx = Prop.blueflagld_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_leftspawnpoint_offscreen_canvas()
{
    const image = graphics.left_spawn_point.image;
    Prop.leftspawnpoint_canvas = document.createElement('canvas');
    Prop.leftspawnpoint_canvas.width = image.width;
    Prop.leftspawnpoint_canvas.height = image.height;
    let ctx = Prop.leftspawnpoint_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_rightspawnpoint_offscreen_canvas()
{
    const image = graphics.right_spawn_point.image;
    Prop.rightspawnpoint_canvas = document.createElement('canvas');
    Prop.rightspawnpoint_canvas.width = image.width;
    Prop.rightspawnpoint_canvas.height = image.height;
    let ctx = Prop.rightspawnpoint_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_downspawnpoint_offscreen_canvas()
{
    const image = graphics.down_spawn_point.image;
    Prop.downspawnpoint_canvas = document.createElement('canvas');
    Prop.downspawnpoint_canvas.width = image.width;
    Prop.downspawnpoint_canvas.height = image.height;
    let ctx = Prop.downspawnpoint_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}
function create_topspawnpoint_offscreen_canvas()
{
    const image = graphics.top_spawn_point.image;
    Prop.topspawnpoint_canvas = document.createElement('canvas');
    Prop.topspawnpoint_canvas.width = image.width;
    Prop.topspawnpoint_canvas.height = image.height;
    let ctx = Prop.topspawnpoint_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_well_offscreen_canvas()
{
    const image = graphics.wall_well.image;
    Wall.well_canvas = document.createElement('canvas');
    Wall.well_canvas.width = image.width;
    Wall.well_canvas.height = image.height;
    let ctx = Wall.well_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_tree_01_offscreen_canvas()
{
    const image = graphics.prop_tree_01.image;
    Prop.tree01_canvas = document.createElement('canvas');
    Prop.tree01_canvas.width = image.width;
    Prop.tree01_canvas.height = image.height;
    let ctx = Prop.tree01_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}

function create_tree_05_offscreen_canvas()
{
    const image = graphics.prop_tree_05.image;
    Prop.tree05_canvas = document.createElement('canvas');
    Prop.tree05_canvas.width = image.width;
    Prop.tree05_canvas.height = image.height;
    let ctx = Prop.tree05_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0);
}


function create_machinegun_offscreen_canvas()
{
    const height = 50;
    const width = 50;
    const image = graphics.missil_rifle.image;

    MachineGun.canvas = document.createElement('canvas');
    MachineGun.canvas.width =  width
    MachineGun.canvas.height =  height;
    let ctx = MachineGun.canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0, width, height);
}

function create_laser_offscreen_canvas()
{
    const height = 50;
    const width = 50;
    const image = graphics.misil.image;

    Laser.canvas = document.createElement('canvas');
    Laser.canvas.width =  width
    Laser.canvas.height =  height;
    let ctx = Laser.canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0, width, height);
}


function create_rifle_offscreen_canvas()
{
    const height = 170;
    const width = 170;
    const image = graphics.missil_rifle.image;

    Rifle.canvas = document.createElement('canvas');
    Rifle.canvas.width =  width
    Rifle.canvas.height =  height;
    let ctx = Rifle.canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0, width, height);
}

function create_missile_offscreen_canvas()
{
    const height = 25;
    const width = 55;
    const image = graphics.missil_bullet.image;

    Missile.canvas = document.createElement('canvas');
    Missile.canvas.width =  width
    Missile.canvas.height =  height;
    let ctx = Missile.canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0, width, height);
}


function create_granade_offscreen_canvas()
{
    const height = 80;
    const width = 80;
    const image = graphics.missil_rifle.image;

    GranadeLauncher.canvas = document.createElement('canvas');
    GranadeLauncher.canvas.width =  width
    GranadeLauncher.canvas.height =  height;
    let ctx = GranadeLauncher.canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0, width, height);
}

function create_auto_missile_offscreen_canvas()
{
    const height = 30;
    const width = 30;
    const image =  graphics.misil.image;

    AutoMissile.canvas = document.createElement('canvas');
    AutoMissile.canvas.width =  width
    AutoMissile.canvas.height =  height;
    let ctx = AutoMissile.canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0, width, height);
}

const tank_width = 50;
const tank_height = 60;
const cannon_width = 15;
const cannon_height = 35;

function create_red_scout_body_offscreen_canvas()
{
    const image = graphics.red_scout_body.image;
    IA_Tank.red_scout_body_canvas = document.createElement('canvas');
    IA_Tank.red_scout_body_canvas.width = tank_width;
    IA_Tank.red_scout_body_canvas.height = tank_height;
    let ctx = IA_Tank.red_scout_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_blue_scout_body_offscreen_canvas()
{
    const image = graphics.blue_scout_body.image;
    IA_Tank.blue_scout_body_canvas = document.createElement('canvas');
    IA_Tank.blue_scout_body_canvas.width = tank_width;
    IA_Tank.blue_scout_body_canvas.height = tank_height;
    let ctx = IA_Tank.blue_scout_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_black_scout_body_offscreen_canvas()
{
    const image = graphics.black_scout_body.image;
    IA_Tank.black_scout_body_canvas = document.createElement('canvas');
    IA_Tank.black_scout_body_canvas.width = tank_width;
    IA_Tank.black_scout_body_canvas.height = tank_height;
    let ctx = IA_Tank.black_scout_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_green_scout_body_offscreen_canvas()
{
    const image = graphics.green_scout_body.image;
    IA_Tank.green_scout_body_canvas = document.createElement('canvas');
    IA_Tank.green_scout_body_canvas.width = tank_width;
    IA_Tank.green_scout_body_canvas.height = tank_height;
    let ctx = IA_Tank.green_scout_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}


function create_red_scout_body_with_cow_offscreen_canvas()
{
    const image = graphics.red_scout_body_cow.image;
    IA_Tank.red_scout_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.red_scout_body_with_cow_canvas.width = tank_width;
    IA_Tank.red_scout_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.red_scout_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_blue_scout_body_with_cow_offscreen_canvas()
{
    const image = graphics.blue_scout_body_cow.image;
    IA_Tank.blue_scout_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.blue_scout_body_with_cow_canvas.width = tank_width;
    IA_Tank.blue_scout_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.blue_scout_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_black_scout_body_with_cow_offscreen_canvas()
{
    const image = graphics.black_scout_body_cow.image;
    IA_Tank.black_scout_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.black_scout_body_with_cow_canvas.width = tank_width;
    IA_Tank.black_scout_body_with_cow_canvas.height =tank_height;
    let ctx = IA_Tank.black_scout_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}


function create_green_scout_body_with_cow_offscreen_canvas()
{
    const image = graphics.green_scout_body_cow.image;
    IA_Tank.green_scout_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.green_scout_body_with_cow_canvas.width = tank_width;
    IA_Tank.green_scout_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.green_scout_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}




function create_red_escolt_body_offscreen_canvas()
{
    const image = graphics.red_escolt_body.image;
    IA_Tank.red_escolt_body_canvas = document.createElement('canvas');
    IA_Tank.red_escolt_body_canvas.width = tank_width;
    IA_Tank.red_escolt_body_canvas.height = tank_height;
    let ctx = IA_Tank.red_escolt_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_blue_escolt_body_offscreen_canvas() 
{
    const image = graphics.blue_escolt_body.image;
    IA_Tank.blue_escolt_body_canvas = document.createElement('canvas');
    IA_Tank.blue_escolt_body_canvas.width = tank_width;
    IA_Tank.blue_escolt_body_canvas.height = tank_height;
    let ctx = IA_Tank.blue_escolt_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_black_escolt_body_offscreen_canvas() 
{
    const image = graphics.black_escolt_body.image;
    IA_Tank.black_escolt_body_canvas = document.createElement('canvas');
    IA_Tank.black_escolt_body_canvas.width = tank_width;
    IA_Tank.black_escolt_body_canvas.height = tank_height;
    let ctx = IA_Tank.black_escolt_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_green_escolt_body_offscreen_canvas() 
{
    const image = graphics.green_escolt_body.image;
    IA_Tank.green_escolt_body_canvas = document.createElement('canvas');
    IA_Tank.green_escolt_body_canvas.width = tank_width;
    IA_Tank.green_escolt_body_canvas.height = tank_height;
    let ctx = IA_Tank.green_escolt_body_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}


function create_red_escolt_body_with_cow_offscreen_canvas() 
{
    const image = graphics.red_escolt_body_cow.image;
    IA_Tank.red_escolt_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.red_escolt_body_with_cow_canvas.width = tank_width;
    IA_Tank.red_escolt_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.red_escolt_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_blue_escolt_body_with_cow_offscreen_canvas() 
{
    const image = graphics.blue_escolt_body_cow.image;
    IA_Tank.blue_escolt_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.blue_escolt_body_with_cow_canvas.width = tank_width;
    IA_Tank.blue_escolt_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.blue_escolt_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}

function create_black_escolt_body_with_cow_offscreen_canvas() 
{
    const image = graphics.black_escolt_body_cow.image;
    IA_Tank.black_escolt_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.black_escolt_body_with_cow_canvas.width = tank_width;
    IA_Tank.black_escolt_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.black_escolt_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}


function create_green_escolt_body_with_cow_offscreen_canvas() 
{
    const image = graphics.green_escolt_body_cow.image;
    IA_Tank.green_escolt_body_with_cow_canvas = document.createElement('canvas');
    IA_Tank.green_escolt_body_with_cow_canvas.width = tank_width;
    IA_Tank.green_escolt_body_with_cow_canvas.height = tank_height;
    let ctx = IA_Tank.green_escolt_body_with_cow_canvas.getContext("2d");

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,tank_width,tank_height);
}


function create_red_escolt_cannon_offscreen_canvas()
{
    const image = graphics.red_escolt_cannon.image;
    IA_Tank.red_escolt_cannon_canvas = document.createElement('canvas');
    IA_Tank.red_escolt_cannon_canvas.width = cannon_width;
    IA_Tank.red_escolt_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.red_escolt_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}

function create_blue_escolt_cannon_offscreen_canvas() 
{
    const image = graphics.blue_escolt_cannon.image;
    IA_Tank.blue_escolt_cannon_canvas = document.createElement('canvas');
    IA_Tank.blue_escolt_cannon_canvas.width = cannon_width;
    IA_Tank.blue_escolt_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.blue_escolt_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}

function create_black_escolt_cannon_offscreen_canvas() 
{
    const image = graphics.black_escolt_cannon.image;
    IA_Tank.black_escolt_cannon_canvas = document.createElement('canvas');
    IA_Tank.black_escolt_cannon_canvas.width = cannon_width;
    IA_Tank.black_escolt_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.black_escolt_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}

function create_green_escolt_cannon_offscreen_canvas() 
{
    const image = graphics.green_escolt_cannon.image;
    IA_Tank.green_escolt_cannon_canvas = document.createElement('canvas');
    IA_Tank.green_escolt_cannon_canvas.width = cannon_width;
    IA_Tank.green_escolt_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.green_escolt_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}




function create_red_scout_cannon_offscreen_canvas()
{
    const image = graphics.red_scout_cannon.image;
    IA_Tank.red_scout_cannon_canvas = document.createElement('canvas');
    IA_Tank.red_scout_cannon_canvas.width = cannon_width;
    IA_Tank.red_scout_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.red_scout_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}

function create_blue_scout_cannon_offscreen_canvas()  
{
    const image = graphics.blue_scout_cannon.image;
    IA_Tank.blue_scout_cannon_canvas = document.createElement('canvas');
    IA_Tank.blue_scout_cannon_canvas.width = cannon_width;
    IA_Tank.blue_scout_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.blue_scout_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}

function create_black_scout_cannon_offscreen_canvas() 
{
    const image = graphics.black_scout_cannon.image;
    IA_Tank.black_scout_cannon_canvas = document.createElement('canvas');
    IA_Tank.black_scout_cannon_canvas.width = cannon_width;
    IA_Tank.black_scout_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.black_scout_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}

function create_green_scout_cannon_offscreen_canvas() 
{
    const image = graphics.green_scout_cannon.image;
    IA_Tank.green_scout_cannon_canvas = document.createElement('canvas');
    IA_Tank.green_scout_cannon_canvas.width = cannon_width;
    IA_Tank.green_scout_cannon_canvas.height = cannon_height;
    let ctx = IA_Tank.green_scout_cannon_canvas.getContext("2d",{ alpha: false });

    // draw the image on the hidden canvas
    ctx.drawImage(image,0,0,cannon_width,cannon_height);
}



function create_offscreen_canvases()
{
    create_tile_offscreen_canvas();
    create_blockb2_offscreen_canvas();

    create_blueflagdl_offscreen_canvas();
    create_blueflagdr_offscreen_canvas();
    create_blueflagrt_offscreen_canvas();
    create_blueflagrd_offscreen_canvas();
    create_blueflagld_offscreen_canvas();
    create_blueflaglt_offscreen_canvas();


    create_redflagdl_offscreen_canvas();
    create_redflagdr_offscreen_canvas();
    create_redflagrt_offscreen_canvas();
    create_redflagrd_offscreen_canvas();
    create_redflagtr_offscreen_canvas();
    create_redflagtl_offscreen_canvas();


    create_blackflagld_offscreen_canvas();
    create_blackflaglt_offscreen_canvas();
    create_blackflagrt_offscreen_canvas();
    create_blackflagrd_offscreen_canvas();
    create_blackflagtr_offscreen_canvas();
    create_blackflagtl_offscreen_canvas();


    create_greenflagld_offscreen_canvas();
    create_greenflaglt_offscreen_canvas();
    create_greenflagdl_offscreen_canvas();
    create_greenflagdr_offscreen_canvas();
    create_greenflagtr_offscreen_canvas();
    create_greenflagtl_offscreen_canvas();

    create_platform_offscreen_canvas();
    create_tree_01_offscreen_canvas();
    create_tree_05_offscreen_canvas();
    create_artifact_offscreen_canvas();
    create_artifactoff_offscreen_canvas();
    create_well_offscreen_canvas();
    create_log_offscreen_canvas();
    create_cactus1_offscreen_canvas();
    create_cactus2_offscreen_canvas();

    create_leftspawnpoint_offscreen_canvas();
    create_rightspawnpoint_offscreen_canvas();
    create_topspawnpoint_offscreen_canvas();
    create_downspawnpoint_offscreen_canvas();


    create_machinegun_offscreen_canvas();
    create_laser_offscreen_canvas();
    create_rifle_offscreen_canvas();
    create_missile_offscreen_canvas();
    create_granade_offscreen_canvas();
    create_auto_missile_offscreen_canvas();

    
    create_red_scout_body_offscreen_canvas();
    create_blue_scout_body_offscreen_canvas();
    create_black_scout_body_offscreen_canvas();
    create_green_scout_body_offscreen_canvas();

    create_red_scout_body_with_cow_offscreen_canvas();
    create_blue_scout_body_with_cow_offscreen_canvas();
    create_black_scout_body_with_cow_offscreen_canvas();
    create_green_scout_body_with_cow_offscreen_canvas();

 
    create_green_escolt_body_offscreen_canvas(); 
    create_black_escolt_body_offscreen_canvas(); 
    create_blue_escolt_body_offscreen_canvas();
    create_red_escolt_body_offscreen_canvas();
    

    create_green_escolt_body_with_cow_offscreen_canvas();
    create_black_escolt_body_with_cow_offscreen_canvas();
    create_blue_escolt_body_with_cow_offscreen_canvas(); 
    create_red_escolt_body_with_cow_offscreen_canvas(); 


    create_green_scout_cannon_offscreen_canvas(); 
    create_black_scout_cannon_offscreen_canvas();  
    create_blue_scout_cannon_offscreen_canvas();
    create_red_scout_cannon_offscreen_canvas();

    create_green_escolt_cannon_offscreen_canvas(); 
    create_black_escolt_cannon_offscreen_canvas();  
    create_blue_escolt_cannon_offscreen_canvas();
    create_red_escolt_cannon_offscreen_canvas();
}