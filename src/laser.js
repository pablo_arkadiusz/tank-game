//Laser Debug Draw points
var black_point_x,black_point_y,white_point_x,white_point_y,  pink_point_x =0, pink_point_y =0;

//#region Laser class

class Laser extends Weapon
{

    //#region Initialization methods

    constructor(owner)
    {
        super("Laser", owner, 0.5);
        this.owner = owner;

        //Array of points to create the laser path
        this.points = [];

        this.index = 0;

        Laser.ray_length = 50; //Long enough to hit everything

        //Points to throw in the raycast
        this.raycast_origin = new Vector2();
        this.raycast_destination = new Vector2();

        //Distance of the laser
        this.distance = 2000;

        this.actual_distance = 0;

        //Did the last raycast suceed ?
        this.raycast_succeed = false;

        //Create the laser bullet
        this.load_ammo();

        this.end_laser_point = new Vector2();

        //To compare the closest object found by the raycast
        this.closest=99999;

        this.show_laser = true;

        this.visual_end = false;
        this.index_visual_end = 0;
        this.visual_end_distace = 0;
    }

    load_ammo() { this.bullet = new LaserBullet(this);  }

    //#endregion
    
    //#region Laser manipulation methods

    static should_collide_with_laser(fixture)
    { return !(fixture.GetUserData() != null && (fixture.GetUserData().type === "Bullet" || fixture.GetUserData().type === "Tank" || fixture.GetUserData().type == "IA Tank" || fixture.GetUserData() == "Sensor" || fixture.GetUserData().type == "Flag"  )); }


    static should_end_visual(fixture)
    { return (fixture.GetUserData() != null && (fixture.GetUserData().type === "Tank" || fixture.GetUserData().type == "IA Tank")); }


    //Raycast callback that creates the laser path and update the next raycast origin and destination
    callback(fixture,point,normal,fraction)
    {   

        if(Laser.should_end_visual(fixture) && !Laser.ray_user.visual_end && fraction < Laser.ray_user.closest)
        {
            Laser.ray_user.visual_end = true;
            Laser.ray_user.index_visual_end = Laser.ray_user.index+1;
            //Set the point of collision to draw the laser path
            Laser.ray_user.points[ Laser.ray_user.index ] = new Vector2(point.x, point.y)

            let this_point = new Vector2 (point.x, point.y);
        
            let dest =  Laser.ray_user.points[ Laser.ray_user.index - 1 ].subtract(this_point);
          
            //Set this as the closest
            Laser.ray_user.closest = fraction;
            Laser.ray_user.raycast_origin = new Vector2( point.x, point.y );

            white_point_x = this_point.from_box_scale_to_world().x;
            white_point_y = this_point.from_box_scale_to_world().y;

            Laser.ray_user.raycast_destination = new Vector2
            ( 
                point.x - dest.x * Laser.ray_length,
                point.y - dest.y * Laser.ray_length
            );

            let temp = new Vector2
            (
                point.x - dest.x * 1,
                point.y - dest.y * 1
            );
            black_point_x = temp.from_box_scale_to_world().x;
            black_point_y = temp.from_box_scale_to_world().y;
            
            Laser.ray_user.raycast_succeed = true;
            return;
        }

        if(Laser.should_collide_with_laser(fixture))
            if(fraction < Laser.ray_user.closest)  //If this raycast hit is the closest it will update the laser point path
            {        
                //Set this as the closest
                Laser.ray_user.closest = fraction;

                //Reflection of the actual laser direction over the normal vector of the hit to get the new laser direction
                let reflected_vector = new Vector2(point.x,point.y).subtract(Laser.ray_user.points[Laser.ray_user.index -1]).normalize().reflect(normal);

                //Set the point of collision to draw the laser path
                Laser.ray_user.points[ Laser.ray_user.index ] = new Vector2(point.x, point.y)

                //Set origin and destination of the next raycast call
                Laser.ray_user.raycast_destination = new Vector2
                ( 
                    point.x + reflected_vector.x * Laser.ray_length,
                    point.y + reflected_vector.y * Laser.ray_length
                );
                Laser.ray_user.raycast_origin = new Vector2( point.x, point.y );
            }

         
        //Bool to notify the laser creation loop that the raycast succeed so it can continue firing the next raycast
        Laser.ray_user.raycast_succeed = true;
    }

    create_laser()
    {
        this.reset_laser_path();
        this.update_laser_starting_point();
        this.create_laser_path();    
    }

    update_laser_starting_point()
    {
        let fire_direction = this.owner.get_fire_direction();
        //The first ray origin is the owner position
        this.raycast_origin.set(this.owner.get_fire_point().x / scale, (canvas_array[0].height - this.owner.get_fire_point().y)  / scale );
                
        //The first ray destination is a point in the tank movement direction with "Laser.ray_length" distance from the origin point
        this.raycast_destination.set ( this.raycast_origin.x + fire_direction.x * Laser.ray_length, this.raycast_origin.y - fire_direction.y *Laser.ray_length ); 

        //Set the first laser point of the path
        this.points[0] = this.raycast_origin;
    }

    create_laser_path()
    {
        //#region Set up variables to start the loop

        this.raycast_succeed = true;
        this.index = 1;
        this.actual_distance = 0;

        //The user of the raycast is this instance. But the callback dont let use a reference to this so it is made with a static variable
        Laser.ray_user = this; 

        //#endregion

        //#region Laser loop

        //Loop that updates all the collision point of the laser
        for(; this.raycast_succeed && this.actual_distance < this.distance; ++this.index)
        {
            this.raycast_succeed = false;

           
            //The raycast is lauched. It will update his next origin and destination points inside the callback
            world_array[0].RayCast( this.callback, this.raycast_origin, this.raycast_destination );

            if(this.raycast_succeed)
            {
                this.point_b = this.raycast_origin;

                if(this.index != 1)
                {
                    //The two points of this segment of the laser
                    //a_point = actual point, b_point = previous point
                    let a_point = this.raycast_origin.from_box_scale_to_world();
                    if(this.points[this.index-1] == undefined) return;
                    let b_point = this.points[this.index-1].from_box_scale_to_world();
                    
                    //Distance between the two points
                    var segment_distance = a_point.subtract(b_point).magnitude();

                    //The traveled distance of the laser is updated 
                    this.actual_distance += segment_distance;

                /* black_point_x = a_point.x;
                    black_point_y = a_point.y;
                
                    white_point_x = b_point.x;
                    white_point_y = b_point.y;*/
                }
                //Reset the closest fragment of the callback, to start from scratch the next raycast call
                this.closest = 99999;
            }
        }

        //#endregion
    }

    enable_drawing_laser() { this.show_laser = true; }
    disable_drawing_laser() { this.show_laser = false; }

    reset_laser_path() { this.points = []; this.visual_end=false;}

    draw_laser(world_index)
    {
        let draw = true;
        let lenght = this.points.length;
        let draw_dist =0;
        for(var i = 0; i<lenght; ++i) 
            {
                if(this.visual_end && i == this.index_visual_end)
                    draw=false;
                if(i != 0) //The first element cant do drawing, because is just the first point and it need two points to draw the segment, so the calculations start from the second point
                {
                    //The two points of this segment of the laser
                    let a_point = this.points[i-1].from_box_scale_to_world();
                    let b_point = this.points[i].from_box_scale_to_world();
                                
                    let header_between_points = b_point.subtract(a_point);
                    let distance_between_points = header_between_points.magnitude();

                   

                    //The distance of the laser that remains to be painted
                    let dist_that_can_be_draw = this.distance - draw_dist;
                

                    if(draw)
                    {
                        //The header normalized
                        let direction_between_points = header_between_points.normalize();
                        this.end_laser_point.set
                        (
                                a_point.x + direction_between_points.x * dist_that_can_be_draw,
                                a_point.y + direction_between_points.y * dist_that_can_be_draw
                        );
                    }
                    
                    //Updates the distance of the laser that already has been drawn
                    draw_dist += distance_between_points;
                }
                
                //If this segment distance is enough it just draw the line to the point of raycast hit
                if(draw_dist < this.distance )
                {  if(draw)
                    {
                        this.end_laser_point.set(this.points[i].x * scale, canvas_array[0].height -( this.points[i].y * scale ));
                        ctx_array[world_index].lineTo( this.end_laser_point.x, this.end_laser_point.y );
                    }
                }
                else //If the segment distance is too long it will print only the distance that can be draw and exit the drawing loop
                {
                    if(draw)
                        ctx_array[world_index].lineTo( this.end_laser_point.x, this.end_laser_point.y );         
                    break;
                }
        }
        //Remove the part of the laser that is left over
        this.points.length = i;
    }

    //#endregion

    //#region Fire methods

    try_fire()
    {
        if(!this.is_on_cooldown) this.fire();
        this.is_on_cooldown = true;        
    }

    fire()
    {

        Audio_Manager.instance.play_laser_shoot();
        

        this.reset_bullet_position();


        this.send_path_to_bullet();
        
        //Activate the bulled. When its active it will call the bullet move method
        this.bullet.is_active = true;
    }

    //#endregion

    //#region Bullet manipulation

    reset_bullet_position()
    {
        var bullet_transform = 
        {
            position:
            {
                x: this.owner.get_fire_point().x / scale , 
                y: (canvas_array[0].height - this.owner.get_fire_point().y)  / scale
            },
            GetAngle: function(){return 0;}
        }
        this.bullet.body.SetTransform(bullet_transform);
        let body_position = new Vector2(this.bullet.body.GetPosition().x,this.bullet.body.GetPosition().y );
        this.bullet.position = body_position.from_box_scale_to_world();   
    }

    send_path_to_bullet()
    {
        this.points[this.points.length] = this.end_laser_point.from_world_to_box_scale();
        this.bullet.path =  [...this.points]; //clone array to a new memory space
        this.bullet.set_direction_to_next_path();
    }

    //#endregion

    //#region Laser state methods

    update_cooldown()
    {
        if(this.bullet.target_reached)
        {
            this.is_on_cooldown = false;
            this.bullet.is_active = false;
            this.bullet.target_reached = false;  
        }
    }

    is_active() { return this.bullet.is_active; }

    //#endregion
    
    //#region Update and Draw

    update(delta_time)
    {
        this.create_laser();
        
        if(this.is_on_cooldown)
            this.update_cooldown();

        if(this.is_active())
            this.bullet_update(delta_time);    
    }

    draw(ctx)
    {
        if(this.show_laser)
        {
            let world_index = this.owner.camera_number;

            ctx.save();

            if(ctx == ctx_array[world_index])
            {
                ctx.strokeStyle = 'red';
                ctx.setLineDash([20, 15]);
                ctx.lineWidth = 7;
                ctx.beginPath();
                this.draw_laser(world_index);
                ctx.stroke();
            }
            ctx.restore();
        }
        ctx.setLineDash([]);
        this.bullet_draw(ctx);
    }

    bullet_update(deltaTime) { this.bullet.update(deltaTime); }

    bullet_draw(ctx) { this.bullet.draw(ctx); }

    //#endregion
}

//#endregion

//#region LaserBullet class

class LaserBullet extends Bullet
{
     //#region Initialization methods

    constructor(parent)
    {
        super(parent, "Laser Bullet",  0.001, 100);
      
        LaserBullet.image =  graphics.misil.image;
        LaserBullet.speed = 7;
        /*LaserBullet.height = 50;
        LaserBullet.width = 50;*/

        LaserBullet.bullet_pivot = 
        {
            x: -Laser.canvas.width >> 1,// /2
            y: -Laser.canvas.height >> 1
        };

        this.path = [];

        this.index = 0;

        //True when reaches the last path point
        this.target_reached = false;

        this.follow_path = true;
    }

    //#endregion

    //#region Movement methods

    move() 
    {
                     
       
        this.manage_direction();
        //Moves the image according to the body position
        var body_position = new Vector2( this.body.GetPosition().x,  this.body.GetPosition().y);
        this.position = body_position.from_box_scale_to_world();   
    }

    //Check if reached the next target and change the direction if did
    manage_direction()
    {
        let next_target_point = this.path[this.index +1].from_box_scale_to_world();
 
        if(Math.abs(this.direction.x) > Math.abs(this.direction.y)) //Moving horizontally
            if(this.direction.x > 0)  
            {
                if(this.position.x > next_target_point.x) //reached the target point
                {
                    ++this.index;
                    this.set_direction_to_next_path();
                }
            }
            else /*moving left*/
            {
                if(this.position.x < next_target_point.x) //reached the target point
                {
                    ++this.index;
                    this.set_direction_to_next_path();
                } 
            }    
        else //Moving Vertically
            if(this.direction.y > 0) //Going upward
            {
                if(this.position.y < next_target_point.y) //reached the target point
                {
                    ++this.index;
                    this.set_direction_to_next_path();
                }
            }
            else //Going downward
                if(this.position.y > next_target_point.y) //reached the target point
                {
                    ++this.index;
                    this.set_direction_to_next_path();
                }
    }

    set_direction_to_next_path()
    {
        //If its already going to the last target
       if(this.index == this.path.length-1)
       {
           this.target_reached = true;
           this.index = 0;
           this.stop();
           game_scene.delete_body(this.body);

           return;
       }

       let direction = this.path[this.index+1].from_box_scale_to_world().subtract(this.position).normalize();
       direction.y*=-1;
       this.change_direction(direction);
    }

    //Change the direction and update the rotation according to it
    change_direction(direction)
    {
       //Update the direction and rotation and of the bullet
       this.direction.set
       (
           direction.x * LaserBullet.speed,
           direction.y * LaserBullet.speed
       );
       this.rotation = Math.atan2(direction.x,direction.y) - 1.56;
        //Moves the body
        this.body.SetLinearVelocity(this.direction);
        console.log("changed");
    }

    stop() { this.is_active = false; this.body.SetLinearVelocity(new b2Vec2(0,0));}

    //#endregion

     //#region Updates and Draw methods

     update(deltaTime)
     {
            this.move();  
     }
     
     draw(ctx)
     {        
         if(this.is_active)
         {
             ctx.save();
             ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
             ctx.rotate(this.rotation);
 
             ctx.drawImage(Laser.canvas, LaserBullet.bullet_pivot.x, LaserBullet.bullet_pivot.y);
             ctx.restore();
         }   
     }
 
     //#endregion
  
     //#region Collisions
 
     on_collision_enter(other)
     {  
         if(other.type == "Tank" || other.type == "IA Tank")
         {
            this.target_reached = true;
            this.index=0;
            this.stop();
            game_scene.delete_body(this.body);
         }
     }

     //#endregion 
}

//#endregion