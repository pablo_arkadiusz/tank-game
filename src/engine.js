//Engine variables
var targetDT = 1 / 60;
var globalDT;
var time = 0,
    FPS  = 0,
    frames    = 0,
    acumDelta = 0;


let text;


//Scenes
let game_scene = null,
    menu_scene = new Menu_Scene(),
    score_scene = null;

var scene_name = "menu";
window.requestAnimationFrame = (function (evt) {
    return window.requestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame     ||
        function (callback) {
            window.setTimeout(callback, targetDT * 1000);
        };
}) ();
window.onload = body_loaded;

function load_game_images(assets, onloaded)
{
    let imagesToLoad = 0;
    
    const onload = () => --imagesToLoad === 0 && onloaded();

    // iterate through the object of assets and load every image
    for (let asset in assets)
        if (assets.hasOwnProperty(asset))
        {
            imagesToLoad++; // one more image to load

            // create the new image and set its path and onload event
            const img = assets[asset].image = new Image;
            img.src = assets[asset].path;
            img.onload = onload;
        }
     
    return assets;
}

//On body loaded
function body_loaded()
{
    Audio_Manager.instance = new Audio_Manager();
    gamepad_on_load();
    menu_scene.start(); 
}

//Called when the player click the PLAY button. Starts the game scene
function start_game()
{
    scene_name = "game";
    menu_scene.hide();
    game_scene = new Game_Scene(player_num);
    load_game_images(graphics, function()
    {
        game_scene.start();
        loop();
    });
}

//Called when the game  game is over, and loads the score scene
function load_score_scene()
{
    ctx_array[0].clearRect(0, 0, canvas_array[0].width, canvas_array[0].height);
    //document.getElementById ("game_scene").className += 'invisible';
    document.getElementById ("game_scene").classList.add('invisible');
    game_scene = null;

    scene_name = "score";
    score_scene = new Score_Scene();
    score_scene.start();
}




function loop() { { game_scene.loop(); requestAnimationFrame(loop); } }





