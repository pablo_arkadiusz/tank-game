
var PI2 = Math.PI * 2;
var PIH = Math.PI / 2;
var degToRad = Math.PI / 180;

function randomBetween(min, max)
{
    return min + (Math.random() * (max - min));
}

function Clamp(num, min, max)
{
    return num <= min ? min : num >= max ? max : num;
}

function RemoveElement(arr, e)
{
    for (let i = 0; i < arr.length; i++)
    {
        if (arr[i] === e)
            arr.splice(i, 1);
    }
    return arr;
}

function RemoveElementAt(arr, i)
{
    arr.splice(i, 1);
    return arr;
}

function RotatePointAroundPoint(origCoord, pointCoord, angle)
{
    var x = pointCoord.x,
        y = pointCoord.y,
        cx = origCoord.x,
        cy = origCoord.y;
    var rad = angle;//(Math.PI / 180) * angle;
    var cos = Math.cos(rad);
    var sin = Math.sin(rad);
    return {
        x: (cos * (x - cx)) + (sin * (y - cy)) + cx,
        y: (cos * (y - cy)) - (sin * (x - cx)) + cy
    };
}

// collisions
function PointInsideCircle(pointPosition, circle)
{
    let distX = pointPosition.x - circle.position.x;
    let distY = pointPosition.y - circle.position.y;

    let dist = Math.sqrt
    (
        distX * distX +
        distY * distY
    );

    return dist < circle.radious;
}

function CheckCollisionPolygon(point, polygon)
{
    // Check if the point is inside the polygon
    var count = polygon.length;
    for (var i = 0; i < polygon.length; i++)
    {
        //var d = DistancePointToSegment(polygon[i], polygon[(i + 1) % polygon.length], point);
        var d = PointToSegmentSign(polygon[i], polygon[(i + 1) % polygon.length], point);
        if (d < 0)
            count--;
    }
    return (count == 0);
}

function DistancePointToSegment(A, B, p)
{
    // A & B are points of the segment
    return (((B.x - A.x)*(A.y - p.y) - (A.x - p.x)*(B.y - A.y)) /
        (Math.sqrt((B.x - A.x)*(B.x - A.x) + (B.y - A.y)*(B.y - A.y))));
}

function PointToSegmentSign(A, B, p)
{
    return ((B.x - A.x)*(A.y - p.y) - (A.x - p.x)*(B.y - A.y));
}

function isInArray(value, array) { return array.indexOf(value) > -1; }


function grades_from_radian(rad)
{
    return rad * 180 / Math.PI;
}

function normal_to_box_direction(vector)
{
	return new Vector2(vector.x, - vector.y);
}

function box_to_normal_direction(vector)
{
	return new Vector2(vector.x, - vector.y);
}


function print_direction(point, direction)
{
	point_a_print = point;
	point_b_print = point.add(direction.scale(-1));

	white_point_x = point_b_print.x;
	white_point_y = point_b_print.y;
}

			
function print_direction2(point, direction)
{
	point2_a_print = point;
	point2_b_print = point.add(direction);

}

function print_direction3(point, direction)
{
	point3_a_print = point;
	point3_b_print = point.add(direction);

}

function to_vector2(a)
{
    return new Vector2(a.x,a.y);
}

function shortAngleDist(a0,a1)
 {
    var max = Math.PI*2;
    var da = (a1 - a0) % max;
    return 2*da % max - da;
}

function angleLerp(a0,a1,t) 
{
    return a0 + shortAngleDist(a0,a1)*t;
}


function rotateVector(vec, ang)
{
    ang = -ang * (Math.PI/180);
    var cos = Math.cos(ang);
    var sin = Math.sin(ang);
    return {x:Math.round(10000*(vec.x * cos - vec.y * sin))/10000, y: Math.round(10000*(vec.x * sin + vec.y * cos))/10000};
};

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
  