// graphic assets references
var graphics = {
    weapon: {
        path: "assets/weapon.png",
        image: null
    },
    misil: {
        path: "assets/misil.png",
        image: null
    },
    explosion: {
        path: "assets/explosion.png", // https://orig00.deviantart.net/fa75/f/2017/267/3/3/mountain_sprite_001_by_jonata_d-dbogk4i.png
        image: null
    },
    box: {
        path: "assets/Animations/explosion.png", // https://forums.rpgmakerweb.com/index.php?threads/whtdragons-trees-recolors.49581/
        image: null
    },
    block_a2: {
        path: "assets/Blocks/Block_A_02.png", // https://forums.rpgmakerweb.com/index.php?threads/whtdragons-trees-recolors.49581/
        image: null
    },
    block_b2: {
        path: "assets/Blocks/Block_B_02.png", // https://forums.rpgmakerweb.com/index.php?threads/whtdragons-trees-recolors.49581/
        image: null
    },
    grass: {
        path: "assets/Tiles/Ground_Tile_02_C.png", // https://forums.rpgmakerweb.com/index.php?threads/whtdragons-trees-recolors.49581/
        image: null
    },
    prop_rock_03: {
        path: "assets/Props/Rock_03.png",
        image: null
    },
    prop_tree_01: {
        path: "assets/Props/Tree_01.png",
        image: null
    },
    prop_tree_05: {
        path: "assets/Props/Tree_05.png",
        image: null
    },
    bullet1: {
        path: "assets/Bullets/bullet1.png",
        image: null
    },
    missil_bullet: {
        path: "assets/Bullets/missil_bullet.png",
        image: null
    },
    missil_rifle: {
        path: "assets/Bullets/rifle_bullet.png",
        image: null
    },
    granade_explosion: {
        path: "assets/Animations/explosion.png",
        image: null
    },
    cow_flag: {
        path: "assets/Props/vaca_arka.png",
        image: null
    },
    blue_flag_dl: {
        path: "assets/Props/blue_flag_dl.png",
        image: null
    },
    blue_flag_dr: {
        path: "assets/Props/blue_flag_dr.png",
        image: null
    },
    blue_flag_rd: {
        path: "assets/Props/blue_flag_rd.png",
        image: null
    },
    blue_flag_rt: {
        path: "assets/Props/blue_flag_rt.png",
        image: null
    },
    blue_flag_ld: {
        path: "assets/Props/blue_flag_ld.png",
        image: null
    },
    blue_flag_lt: {
        path: "assets/Props/blue_flag_lt.png",
        image: null
    },
    red_flag_tl: {
        path: "assets/Props/red_flag_tl.png",
        image: null
    },
    red_flag_tr: {
        path: "assets/Props/red_flag_tr.png",
        image: null
    },
    red_flag_rd: {
        path: "assets/Props/red_flag_rd.png",
        image: null
    },
    red_flag_rt: {
        path: "assets/Props/red_flag_rt.png",
        image: null
    },
    red_flag_dl: {
        path: "assets/Props/red_flag_dl.png",
        image: null
    },
    red_flag_dr: {
        path: "assets/Props/red_flag_dr.png",
        image: null
    },
    black_flag_tl: {
        path: "assets/Props/black_flag_tl.png",
        image: null
    },
    black_flag_tr: {
        path: "assets/Props/black_flag_tr.png",
        image: null
    },
    black_flag_rd: {
        path: "assets/Props/black_flag_rd.png",
        image: null
    },
    black_flag_rt: {
        path: "assets/Props/black_flag_rt.png",
        image: null
    },
    black_flag_lt: {
        path: "assets/Props/black_flag_lt.png",
        image: null
    },
    black_flag_ld: {
        path: "assets/Props/black_flag_ld.png",
        image: null
    },
    green_flag_tl: {
        path: "assets/Props/green_flag_tl.png",
        image: null
    },
    green_flag_tr: {
        path: "assets/Props/green_flag_tr.png",
        image: null
    },
    green_flag_dr: {
        path: "assets/Props/green_flag_dr.png",
        image: null
    },
    green_flag_dl: {
        path: "assets/Props/green_flag_dl.png",
        image: null
    },
    green_flag_lt: {
        path: "assets/Props/green_flag_lt.png",
        image: null
    },
    green_flag_ld: {
        path: "assets/Props/green_flag_ld.png",
        image: null
    },
    black_scout_body: {
        path: "assets/Tanks/Scout/black_tank_body.png",
        image: null
    },
    red_scout_body: {
        path: "assets/Tanks/Scout/red_tank_body.png",
        image: null
    },
    green_scout_body: {
        path: "assets/Tanks/Scout/green_tank_body.png",
        image: null
    },
    blue_scout_body: {
        path: "assets/Tanks/Scout/blue_tank_body.png",
        image: null
    },
    blue_scout_cannon: {
        path: "assets/Tanks/Scout/blue_cannon.png",
        image: null
    },
    red_scout_cannon: {
        path: "assets/Tanks/Scout/red_cannon.png",
        image: null
    },
    green_scout_cannon: {
        path: "assets/Tanks/Scout/green_cannon.png",
        image: null
    },
    black_scout_cannon: {
        path: "assets/Tanks/Scout/black_cannon.png",
        image: null
    },
    platform: {
        path: "assets/Props/Platform.png",
        image: null
    },

    black_scout_body_cow: {
        path: "assets/Tanks/Scout/black_tank_body_with_cow.png",
        image: null
    },
    red_scout_body_cow: {
        path: "assets/Tanks/Scout/red_tank_body_with_cow.png",
        image: null
    },
    green_scout_body_cow: {
        path: "assets/Tanks/Scout/green_tank_body_with_cow.png",
        image: null
    },
    blue_scout_body_cow: {
        path: "assets/Tanks/Scout/blue_tank_body_with_cow.png",
        image: null
    },

    black_escolt_body: {
        path: "assets/Tanks/Escolt/black_escolt_body.png",
        image: null
    },
    red_escolt_body: {
        path: "assets/Tanks/Escolt/red_escolt_body.png",
        image: null
    },
    green_escolt_body: {
        path: "assets/Tanks/Escolt/green_escolt_body.png",
        image: null
    },
    blue_escolt_body: {
        path: "assets/Tanks/Escolt/blue_escolt_body.png",
        image: null
    },
    blue_escolt_cannon: {
        path: "assets/Tanks/Escolt/blue_escolt_cannon.png",
        image: null
    },
    red_escolt_cannon: {
        path: "assets/Tanks/Escolt/red_escolt_cannon.png",
        image: null
    },
    green_escolt_cannon: {
        path: "assets/Tanks/Escolt/green_escolt_cannon.png",
        image: null
    },
    black_escolt_cannon: {
        path: "assets/Tanks/Escolt/black_escolt_cannon.png",
        image: null
    },

    black_escolt_body_cow: {
        path: "assets/Tanks/Escolt/black_escolt_body_with_cow.png",
        image: null
    },
    red_escolt_body_cow: {
        path: "assets/Tanks/Escolt/red_escolt_body_with_cow.png",
        image: null
    },
    green_escolt_body_cow: {
        path: "assets/Tanks/Escolt/green_escolt_body_with_cow.png",
        image: null
    },
    blue_escolt_body_cow: {
        path: "assets/Tanks/Escolt/blue_escolt_body_with_cow.png",
        image: null
    },
    decor_tile_b_05: {
        path: "assets/Decor_Tiles/Decor_Tile_B_05.png",
        image: null
    },
    artifact: {
        path: "assets/Props/Artifact.png",
        image: null
    },
    artifact_off: {
        path: "assets/Props/Artifact_off.png",
        image: null
    },
    left_spawn_point: {
        path: "assets/Props/LeftSpawnPoint.png",
        image: null
    },
    right_spawn_point: {
        path: "assets/Props/RightSpawnPoint.png",
        image: null
    },
    top_spawn_point: {
        path: "assets/Props/TopSpawnPoint.png",
        image: null
    },
    down_spawn_point: {
        path: "assets/Props/DownSpawnPoint.png",
        image: null
    },
    wall_well: {
        path: "assets/Props/Well.png",
        image: null
    },
    wall_log: {
        path: "assets/Props/Log.png",
        image: null
    },
    cactus1: {
        path: "assets/Props/Cactus_01.png",
        image: null
    },
    cactus2: {
        path: "assets/Props/Cactus_02.png",
        image: null
    },


    
};