SetupKeyboardEvents ();
player_num = 1;
gamepad_manager = [];

let keyboard_user = 1;
var gamepads = [];



function gamepad_on_load()
{
    disable_change_to_gamepad();
}
window.addEventListener("gamepadconnected", function(e) {
    if(scene_name == "menu")
        add_controller(e.gamepad);

    
    console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
      e.gamepad.index, e.gamepad.id,
      e.gamepad.buttons.length, e.gamepad.axes.length);
  });

window.addEventListener("gamepaddisconnected", function(e) {

    if(scene_name == "menu")
    {
       remove_controller(e.gamepad);
    }

    //console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.");
    
});

var a = false, w = false, d = false;
window.requestAnimationFrame(update_game_pads);

function update_game_pads()
{
    const conected_gamepads = navigator.getGamepads();

    
    if(Input.IsKeyPressed(KEY_A) && !a)
    {
       a = true;
        add_controller({index: 0, gamepad:true});
    }
    
    if(!Input.IsKeyPressed(KEY_A) && a)
    {
        a=false;
        remove_controller({index: 0, gamepad:true});
    }
    

    if(conected_gamepads[0])
    {
        const gamepad = conected_gamepads[0];
        const gamepad_info =
        {
            axes: 
            [
                gamepad.axes[0].toFixed(2),  /*Left joystich horizontal*/
                gamepad.axes[1].toFixed(2), /*Left joystich vertical*/
                gamepad.axes[2].toFixed(2), /*Right joystich horizontal*/
                gamepad.axes[3].toFixed(2)  /*Right joystich horizontal*/
            ],
            buttons: 
            [
                /*BUTTON_A:*/ gamepad.buttons[0].pressed,
                /*BUTTON_B:*/ gamepad.buttons[1].pressed,
                /*BUTTON_X:*/ gamepad.buttons[2].pressed,
                /*BUTTON_Y:*/ gamepad.buttons[3].pressed,
                /*BUTTON_LB:*/ gamepad.buttons[4].pressed,
                /*BUTTON_RB:*/ gamepad.buttons[5].pressed,
                /*BUTTON_LT:*/ gamepad.buttons[6].pressed,
                /*BUTTON_RT:*/ gamepad.buttons[7].pressed,
                /*BUTTON_back:*/ gamepad.buttons[8].pressed,
                /*BUTTON_start:*/ gamepad.buttons[9].pressed,
                /*BUTTON_L3:*/ gamepad.buttons[10].pressed,
                /*BUTTON_R3:*/ gamepad.buttons[11].pressed,
                /*BUTTON_UP:*/ gamepad.buttons[12].pressed,
                /*BUTTON_DOWN:*/ gamepad.buttons[13].pressed,
                /*BUTTON_LEFT:*/ gamepad.buttons[14].pressed,
                /*BUTTON_RIGHT:*/ gamepad.buttons[15].pressed,
            ]
        }

        gamepads[0] = gamepad_info;
        //console.log(JSON.stringify(gamepad_info.axes));
    }
   

    window.requestAnimationFrame(update_game_pads);
}


function disable_change_to_gamepad()
{
    document.getElementById('game_pad_optionb1').setAttribute("disabled","disabled");
    document.getElementById('game_pad_optionb1').parentElement.style = "opacity: 0.5";
}
function enable_change_to_gamepad()
{
    
    document.getElementById('game_pad_optionb1').removeAttribute("disabled");
    document.getElementById('game_pad_optionb1').parentElement.style = "opacity: 1";
}

function add_controller(gamepad)
{
    enable_change_to_gamepad();
    ++player_num;
    gamepad_manager.push({player:gamepad_manager.length + 2,gamepad: gamepad});

    let player_path = "#player" + (gamepad_manager.length+1).toString();
    let pad_index = gamepad_manager.length - 1;
    let paragraph = document.querySelector(player_path + " p");
    paragraph.innerHTML= "";
    paragraph.className -= 'blink';
    
    document.querySelector(player_path + " h1").innerHTML = "Player " + gamepad_manager[ pad_index ].player.toString();
    document.querySelector(player_path + " img").classList.remove('invisible');
    document.querySelector(player_path + ' .checkmark_box').classList.remove('invisible');
}

function remove_controller(gamepad)
{
    if(gamepad_manager.length == 1)
        disable_change_to_gamepad();
    --player_num;
    let player_path = "#player" + (gamepad_manager.length+1).toString();

    gamepad_manager =  gamepad_manager.filter(function(value, index, arr)
    { return value.gamepad.index != gamepad.index; }); 
    console.log(player_path);
    let paragraph = document.querySelector(player_path + ' p');
    paragraph.innerHTML = "Press any gamepad controller button";
    paragraph.classList.add ('blink');
    document.querySelector(player_path + ' h1').innerHTML = "Not connected";
    document.querySelector(player_path + ' img').classList.add('invisible');
    document.querySelector(player_path + ' .checkmark_box').classList.add('invisible');
}

function change_to_gamepad_control(player)
{
    if(player != keyboard_user) return;

    const conected_gamepads = navigator.getGamepads();

    if(conected_gamepads[0])
    {
        change_gamepad_to_keyboard_user();
        
    }
}

function change_keyboard_user_to_gamepad(player)
{
    let target_gamepad;
    gamepad_manager =  gamepad_manager.filter(function(value)
    { if( value.player != player) return true; else  target_gamepad = value.gamepad }); 

    gamepad_manager.push({player: keyboard_user, gamepad: target_gamepad});

    console.log("changes in:" + player);
    document.getElementById('game_pad_optiona' +keyboard_user ).removeAttribute('checked');
    document.getElementById('game_pad_optionb' +keyboard_user ).setAttribute('checked', 'checked'); 
    document.getElementById('game_pad_optionb' +keyboard_user ).checked = true;
    document.getElementById('game_pad_optiona' +keyboard_user ).checked = false;

    keyboard_user = player;

}

function change_gamepad_to_keyboard_user()
{
    let target_player = keyboard_user == gamepad_manager.length+1 ? gamepad_manager.length : gamepad_manager.length+1;

    console.log("tRget user : " + target_player);
    console.log("keyb user : " + keyboard_user);
    let target_gamepad;

    gamepad_manager =  gamepad_manager.filter(function(value)
    { if( value.player != target_player) return true; else  target_gamepad = value.gamepad }); 

    gamepad_manager.push({player: keyboard_user, gamepad: target_gamepad});
    keyboard_user = target_player;

    document.getElementById('game_pad_optionb' +target_player ).removeAttribute('checked');
    document.getElementById('game_pad_optiona' +target_player ).setAttribute('checked', 'checked'); 
    document.getElementById('game_pad_optiona' +target_player ).checked = true;
    document.getElementById('game_pad_optionb' +target_player ).checked = false;
}

function change_to_keyboard_control(player)
{
    if(player == keyboard_user) return;

    const conected_gamepads = navigator.getGamepads();

    if(conected_gamepads[0])
    {
        console.log("keyboard user is: " + keyboard_user);

        if(keyboard_user == null)
        {
            add_controller(conected_gamepads[0]);
            keyboard_user = player;
            console.log("user : " + player);
        }
        else
        {
            change_keyboard_user_to_gamepad(player);
        }
        //function_swap_controllers(gamepad_manager[gamepad_manager.length-1].player, keyboard_user);
    }
}