class Prop
{
    //#region Constructor

    constructor(position, type)
    {
        this.type = type;
        this.position = position;

        this.select_canvas();
    }

    select_canvas()
    {
        switch(this.type)
        {
            case "platform": this.canvas = Prop.platform_canvas;
                break;
            case "tree_01":  this.canvas = Prop.tree01_canvas;
                break;
            case "tree_05":  this.canvas = Prop.tree05_canvas;
                break;
            case "artifact": this.canvas = Prop.artifact_canvas;
                break;
            case "left_spawn_point": this.canvas = Prop.leftspawnpoint_canvas;
                break;
            case "right_spawn_point": this.canvas = Prop.rightspawnpoint_canvas;
                break;
            case "down_spawn_point": this.canvas = Prop.downspawnpoint_canvas;
                break;
            case "top_spawn_point":  this.canvas = Prop.topspawnpoint_canvas;
                break;
            case "well": this.canvas = Prop.well_canvas;
                break;
            case "cactus1": this.canvas = Prop.cactus1_canvas; 
                 break;
            case "cactus2": this.canvas = Prop.cactus2_canvas;
                break;
            case "blueflag_dl": this.canvas = Prop.blueflagdl_canvas;
                break;
            case "blueflag_dr": this.canvas = Prop.blueflagdr_canvas;
                break;
            case "blueflag_rd": this.canvas = Prop.blueflagrd_canvas;
                break;
            case "blueflag_rt": this.canvas = Prop.blueflagrt_canvas;
                break;
            case "blueflag_ld": this.canvas = Prop.blueflagld_canvas;
                break;
            case "blueflag_lt": this.canvas = Prop.blueflaglt_canvas;
                break;
            case "redflag_dl": this.canvas = Prop.redflagdl_canvas;
                break;
            case "redflag_dr": this.canvas = Prop.redflagdr_canvas;
                break;
            case "redflag_rd": this.canvas = Prop.redflagrd_canvas;
                break;
            case "redflag_rt": this.canvas = Prop.redflagrt_canvas;
                break;
            case "redflag_tr": this.canvas = Prop.redflagtr_canvas;
                break;
            case "redflag_tl": this.canvas = Prop.redflagtl_canvas;
                break;
            case "blackflag_ld": this.canvas = Prop.blackflagld_canvas;
                break;
            case "blackflag_lt": this.canvas = Prop.blackflaglt_canvas;
                break;
            case "blackflag_rd": this.canvas = Prop.blackflagrd_canvas;
                break;
            case "blackflag_rt": this.canvas = Prop.blackflagrt_canvas;
                break;
            case "blackflag_tr": this.canvas = Prop.blackflagtr_canvas;
                break;
            case "blackflag_tl": this.canvas = Prop.blackflagtl_canvas;
                break;
            case "greenflag_ld": this.canvas = Prop.greenflagld_canvas;
                break;
            case "greenflag_dl": this.canvas = Prop.greenflagdl_canvas;
                break;
            case "greenflag_dr": this.canvas = Prop.greenflagdr_canvas;
                break;
            case "greenflag_lt": this.canvas = Prop.greenflaglt_canvas;
                break;
            case "greenflag_tr": this.canvas = Prop.greenflagtr_canvas;
                break;
            case "greenflag_tl": this.canvas = Prop.greenflagtl_canvas;
                break;
            default: console.error("Canvas not found: " + this.type);
        }
        this.pivot = { x: - this.canvas.width >> 1, y: - this.canvas.height >> 1 };

    }


    change_canvas(new_canvas)
    {
        console.log("canvas changed");
        this.canvas = new_canvas;
    }
    //#endregion

    draw(ctx)
    {
        ctx.drawImage(this.canvas, this.position.x + this.pivot.x, this.position.y + this.pivot.y);
    }

}