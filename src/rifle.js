//#region Machine gun class

/*
    * Machine gun weapon that the tanks can posses
    * that fires bullets fast, the bullets bounce when they 
    * hit the wall, and disappear after some time
*/

class Rifle extends Weapon
{
    //#region Initialization methods

    constructor(owner)
    {
        super("Rifle", owner, 1);

        Rifle.max_bullets = 1;

        //Index to select the bullet to fire
        this.bullet_index = 0;

        //Creates all the bullets
        this.load_ammo();  

        this.active_bullets = 0;
    }
    
    load_ammo()
    {
        this.bullets = new Array(Rifle.max_bullets);

        for(let i = 0; i < Rifle.max_bullets; ++i )
            this.bullets[i] = new RifleBullet(this);
    }
    
    //#endregion

    //#region Fire methods

    try_fire()
    {
        if(!this.is_on_cooldown) this.fire();
        this.is_on_cooldown=true;
    }

    fire()
    {

        if(this.owner.type == "IA Tank")
        {
            if(this.owner.player_is_near)
                Audio_Manager.instance.play_rifle_shoot();

        }
        else
        {
            Audio_Manager.instance.play_rifle_shoot();
        }
        //Put the bullet in the starting position
        let bullet_pos = this.owner.get_fire_point().from_world_to_box_scale();
        let bullet_transform = 
        {
            position: {x: bullet_pos.x , y: bullet_pos.y},
            GetAngle: function(){return 0;}
        }

        this.bullets[this.bullet_index].body.SetTransform(bullet_transform);

        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.bullets[this.bullet_index].body.GetPosition().x,  this.bullets[this.bullet_index].body.GetPosition().y);
        this.bullets[this.bullet_index].position = bodyPosition.from_box_scale_to_world();  


        //Update the direction and rotation and of the bullet
        let cannon_angle = this.owner.get_cannon_angle();

        let x_dir = -Math.sin(  cannon_angle );
        let y_dir = -Math.cos( cannon_angle );

        this.bullets[this.bullet_index].direction.set
        (
            x_dir * RifleBullet.speed,
            y_dir * RifleBullet.speed
        );

        this.bullets[this.bullet_index].body.SetLinearVelocity( this.bullets[this.bullet_index].direction);

        this.bullets[this.bullet_index].rotation = this.owner.get_bullet_rotation();

        if(!this.bullets[this.bullet_index].active)
        {
             //Activate the fired bulled. When its active it will call the bullet move method
            this.bullets[this.bullet_index].active = true;
            ++this.active_bullets;
        }

        //Update the bullet index
        if(++this.bullet_index == Rifle.max_bullets)
            this.bullet_index = 0;     
    }

   //#endregion

    //#region Updates and Draw methods

    update(deltaTime)
    {
        if(this.is_on_cooldown)
            this.update_cooldown(deltaTime);
        if(this.is_active())
            this.bullet_update(deltaTime);   
    }

    update_cooldown(deltaTime)
    {       
        this.elapsed_time_since_last_shoot += deltaTime
        if(this.elapsed_time_since_last_shoot  > this.cooldown)
        {
            this.is_on_cooldown = false;
            this.elapsed_time_since_last_shoot = 0;
        }
    }

    //Calls the update method of all the bullets
    bullet_update(deltaTime)
    { this.bullets.forEach(element => { element.update(deltaTime); }); }

    draw(ctx)
    {      
        if(this.is_active())
            this.bullet_draw(ctx); 
    }
    
    bullet_draw(ctx) { this.bullets.forEach(element => { element.draw(ctx); }); }

    //#endregion

    //#region Machine gun state methods

    is_active() { return this.active_bullets != 0;}

    //#endregion
}

//#endregion

//#region Bullet class

//Bullet that the machine gun fires
class RifleBullet extends Bullet
{
    //#region Constructor
    constructor(parent)
    {
        super(parent, "Gun Bullet", 10, 10);

        RifleBullet.image =  graphics.missil_rifle.image;
        RifleBullet.speed = 8;
        /*RifleBullet.height = 170;
        RifleBullet.width = 170;*/

        RifleBullet.bullet_pivot = 
        {
            x: -Rifle.canvas.width  >> 1, //  /2
            y: -Rifle.canvas.height >> 1
        };

        //Time(seconds) in which the bullet will disappear
        RifleBullet.time_alive = 4;

        //Elapsed time since firing this bullet
        this.elapsed_time = 0;
    }


    //#endregion

    //#region Movement Methods

    move(deltaTime) 
    {
        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.body.GetPosition().x,  this.body.GetPosition().y);

        this.position = bodyPosition.from_box_scale_to_world();  
    }

    stop() { this.body.SetLinearVelocity(new b2Vec2(0,0)); }

    //#endregion

    //#region Updates and Draw methods

    update(deltaTime)
    {
        if(this.active)
        { 
            this.move(deltaTime);
            this.update_life_timer(deltaTime); 
        }
    }

    //Makes the bullet dissapear when the time_alive period ends
    update_life_timer(deltaTime)
    {
        this.elapsed_time += deltaTime;
        if(this.elapsed_time > RifleBullet.time_alive)
        {
            this.deactivate();    
        }
    }
    
    draw(ctx)
    {   

        if(this.active)
        {
            ctx.save();
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.rotation);

            ctx.drawImage(Rifle.canvas, RifleBullet.bullet_pivot.x, RifleBullet.bullet_pivot.y);
            ctx.restore();
        }   
    }

    //#endregion

    //#region  Bullet activation methods

    deactivate()
    {
        this.elapsed_time = 0;
        this.active = false;
        --this.parent.active_bullets;
        this.stop();
        
        game_scene.delete_body(this.body);

    }

    //#endregion

    //#region Collision methods

    on_collision_enter(other)
    {
        if(this.active)
        {
            if(other.type == "Wall")
            {
                this.deactivate();
            }

            if(other.type == "Tank" || other.type == "IA Tank")
            {
                this.deactivate();
            }
        
        }
    }
    //#endregion
}



//#endregion