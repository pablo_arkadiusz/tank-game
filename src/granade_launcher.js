//#region Machine gun class

/*
    * Machine gun weapon that the tanks can posses
    * that fires bullets fast, the bullets bounce when they 
    * hit the wall, and disappear after some time
*/

class GranadeLauncher extends Weapon
{
    //#region Initialization methods

    constructor(owner)
    {
        super("Granade Launcher", owner,1.5);
        //Creates all the bullets
        this.load_ammo();  

        this.active_bullets = 0;
    }
    
    load_ammo() { this.bullet = new GranadeBullet(this);  }
    
    //#endregion

    //#region Fire methods

    try_fire()
    {
        if(!this.is_on_cooldown && !this.bullet.active) this.fire();
        this.is_on_cooldown=true;
    }

    fire()
    {
        if(this.owner.type == "IA Tank")
        {
            if(this.owner.player_is_near)
                Audio_Manager.instance.play_granade_shoot();
        }
        else
            Audio_Manager.instance.play_granade_shoot();



        this.reset_bullet_position();

             
        //Update the direction and rotation and of the bullet
        let cannon_angle = this.owner.get_cannon_angle();

        let x_dir = -Math.sin(  cannon_angle );
        let y_dir = -Math.cos( cannon_angle );

        this.bullet.direction.set
        (
            x_dir * GranadeBullet.speed,
            y_dir * GranadeBullet.speed
        );
        
           //Moves the body
           this.bullet.body.SetLinearVelocity(this.bullet.direction);
        //Activate the bulled. When its active it will call the bullet move method
        this.bullet.active = true;
    }

   //#endregion


   reset_bullet_position()
   {
        var bullet_transform = 
        {
            position:
            {
                x: this.owner.get_fire_point().x / scale , 
                y: (canvas_array[0].height - this.owner.get_fire_point().y)  / scale
            },
            GetAngle: function(){return 0;}
        }
        this.bullet.body.SetTransform(bullet_transform);
        let body_position = new Vector2(this.bullet.body.GetPosition().x,this.bullet.body.GetPosition().y );
        this.bullet.position = body_position.from_box_scale_to_world();   
   }
    //#region Updates and Draw methods

    update(delta_time)
    {
        if(this.is_on_cooldown)
            this.update_cooldown(delta_time);
        this.bullet_update(delta_time);   
    }


    update_cooldown(delta_time)
    {
        this.elapsed_time_since_last_shoot += delta_time
        if(this.elapsed_time_since_last_shoot  > this.cooldown)
        {
            this.is_on_cooldown = false;
            this.elapsed_time_since_last_shoot = 0;
        }
    }

    //Calls the update method of all the bullets
    bullet_update(delta_time)
    { this.bullet.update(delta_time); }

    draw(ctx)
    {      
        this.bullet_draw(ctx);

        
    }
    
    bullet_draw(ctx) {  this.bullet.draw(ctx); }

    //#endregion

    //#region Machine gun state methods

    is_active() { return this.bullet.active;}

    //#endregion
}

//#endregion

//#region Bullet class

//Bullet that the machine gun fires
class GranadeBullet extends Bullet
{
    //#region Constructor
    constructor(parent)
    {
        super(parent, "Gun Bullet", 10, 0);

        GranadeBullet.speed = 10;
       /* GranadeBullet.height = 80;
        GranadeBullet.width = 80;*/

        GranadeBullet.bullet_pivot = 
        {
            x: -GranadeLauncher.canvas.width  >> 1, //  /2
            y: -GranadeLauncher.canvas.height >> 1
        };
        GranadeBullet.sensor_radius = 145;
        //Time(seconds) in which the bullet will disappear
        GranadeBullet.time_alive = 0.4;

        //Elapsed time since firing this bullet
        this.elapsed_time = 0;

        this.animations = [];     
        this.animation_index = 0;
        this.create_sensor();
    }


    //#endregion

    create_sensor()
    {
         let shape = new b2CircleShape();
         shape.m_radius = (GranadeBullet.sensor_radius) / scale;

        let options = {
            density : 1.0,
            friction: 1.0,
            restitution : 0.5,
            }

        // Fixture: define physics properties (density, friction, restitution)
        var fix_def = new b2FixtureDef();
        fix_def.shape = shape;

        fix_def.density = options.density;
        fix_def.friction = options.friction;
        fix_def.restitution = options.restitution;
            
    
        fix_def.filter.categoryBits = collision_rule.SENSOR.collision;
        fix_def.filter.maskBits = collision_rule.SENSOR.layer;
        this.sensor = this.body.CreateFixture(fix_def);
        this.sensor.SetUserData("Sensor");
        this.sensor.SetSensor(true);  
        
        this.sensor_targets = [];
    }
    
    //#region Movement Methods

    move(deltaTime) 
    {


        //Moves the image according to the body position
        var bodyPosition = new Vector2( this.body.GetPosition().x,  this.body.GetPosition().y);

        this.position = bodyPosition.from_box_scale_to_world();  
 
    }

    stop() { this.body.SetLinearVelocity(new b2Vec2(0,0)); }

    //#endregion

    //#region Updates and Draw methods

    update(delta_time)
    {
        if(this.active)
        { 
            this.move(delta_time);
            this.update_life_timer(delta_time); 
        }

        this.play_explosion_animations(delta_time);
    }



    //Makes the bullet dissapear when the time_alive period ends
    update_life_timer(deltaTime)
    {
        this.elapsed_time += deltaTime;
        if(this.elapsed_time > GranadeBullet.time_alive)
        {
            this.deactivate();    
        }
    }
    
    draw(ctx)
    {   
        if(this.active)
        {
            ctx.save();
            ctx.translate(this.position.x + .5 | 0,this.position.y + .5 | 0);
            ctx.rotate(this.rotation);

            ctx.drawImage(GranadeLauncher.canvas, GranadeBullet.bullet_pivot.x, GranadeBullet.bullet_pivot.y);
            ctx.restore();
        }

        for(let i = 0 ; i < this.animations.length; ++i)
        {
            ctx.save();

            ctx.translate(this.animations[i].position.x+ .5 | 0, this.animations[i].position.y+ .5 | 0);
            this.animations[i].animation.Draw(ctx);
            ctx.restore();
        }
        
    }

    //#endregion

    //#region  Bullet activation methods

    deactivate()
    {
        this.explode();
        this.elapsed_time = 0;
        this.active = false;
        this.stop();
        
        game_scene.delete_body(this.body);

    }

    //#endregion

    //#region Collision methods

    on_collision_enter(other, fixture)
    {
        if(fixture.GetUserData() != "Sensor" )
        {
            if(this.active)
            {   
                if(other.type == "IA Tank" || other.type == "Tank")
                {
                    this.deactivate();
                }
            }
        }
        else if(other != this.parent.owner)
        {
            if(other.type == "IA Tank" || other.type == "Tank")
            {
                
                if(!this.sensor_targets.some(e => (e===(other))))
                {
                    this.sensor_targets.push(other);
                }
            }

        }
    }
    //#endregion

    play_explosion_animations(delta_time)
    {
        for(let i = 0 ; i < this.animations.length; ++i)
            if(!this.animations[i].animation.ended)
            {

                this.animations[i].animation.Update(delta_time);
            }

            else //If the animation end it delete it from the animation array
            {
                this.animations.splice(i,1);   
                --this.animation_index;    
            }
    }

    explode()
    {
        this.stop();
        this.animations.push(
            {animation: new SSAnimation(graphics.granade_explosion.image, graphics.granade_explosion.image.naturalWidth/3 ,graphics.granade_explosion.image.naturalHeight/3 ,  [3,3,3], 1/24, false), position: {x: this.position.x - 100 , y: this.position.y}
        });
        this.animations.push(
            {animation: new SSAnimation(graphics.granade_explosion.image, graphics.granade_explosion.image.naturalWidth/3 ,graphics.granade_explosion.image.naturalHeight/3 ,  [3,3,3], 1/24, false), position: {x: this.position.x + 100 , y: this.position.y}
        });
        this.animations.push(
            {animation: new SSAnimation(graphics.granade_explosion.image, graphics.granade_explosion.image.naturalWidth/3 ,graphics.granade_explosion.image.naturalHeight/3 ,  [3,3,3], 1/24, false), position: {x: this.position.x  , y: this.position.y}
        });

        this.animations.push(
            {animation: new SSAnimation(graphics.granade_explosion.image, graphics.granade_explosion.image.naturalWidth/3 ,graphics.granade_explosion.image.naturalHeight/3 ,  [3,3,3], 1/24, false), position: {x: this.position.x , y: this.position.y + 100}
        });
        this.animations.push(
            {animation: new SSAnimation(graphics.granade_explosion.image, graphics.granade_explosion.image.naturalWidth/3 ,graphics.granade_explosion.image.naturalHeight/3 ,  [3,3,3], 1/24, false), position: {x: this.position.x  , y: this.position.y - 100}
        });

        this.do_damage();
    }


    do_damage()
    {

        this.sensor_targets.forEach(element => 
        {
            let dist = element.position.subtract(this.position).magnitude();
            if(dist < GranadeBullet.sensor_radius)
            {
                //console.log("do damege to : " + element.type);
                element.take_damage(10, this.parent.owner);
            }

        });

        this.sensor_targets = [];
    }
}

//#endregion