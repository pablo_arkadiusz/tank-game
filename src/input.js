// key events
var lastPress = null;

const KEY_LEFT  = 37, KEY_A = 65;
const KEY_UP    = 38, KEY_W = 87;
const KEY_RIGHT = 39, KEY_D = 68;
const KEY_DOWN  = 40, KEY_S = 83;
const KEY_PAUSE = 19; KEY_Q = 81;
const KEY_SPACE = 32; KEY_E = 69;
const KEY_SCAPE = 27; KEY_TAB = 9;
const KEY_LSHIFT = 16;

const KEY_NP_0 = 96;
const KEY_NP_1 = 97;
const KEY_0 = 48;
const KEY_1 = 49;
const KEY_2 = 50;
const KEY_3 = 51;
const KEY_4 = 52;
const KEY_5 = 53;
const KEY_6 = 54;
const KEY_7 = 55;
const KEY_8 = 56;
const KEY_9 = 57;

const BUTTON_A      = 0;
const BUTTON_B      = 1;
const BUTTON_X      = 2;
const BUTTON_Y      = 3;
const BUTTON_LB     = 4;
const BUTTON_RB     = 5;
const BUTTON_LT     = 6;
const BUTTON_RT     = 7;
const BUTTON_back   = 8;
const BUTTON_START  = 9;
const BUTTON_L3     = 10;
const BUTTON_R3     = 11;
const BUTTON_UP     = 12;
const BUTTON_DOWN   = 13;
const BUTTON_LEFT   = 14;
const BUTTON_RIGHT  = 15;

actual_pad_index = 0;

let second_player_control =
{
    is_keyboard: true,
    left_move: KEY_A,
    right_move: KEY_D,
    bottom_move: KEY_S,
    up_move: KEY_W,
    fire: null,
    fire2: null,
    weapon_change: KEY_LSHIFT,
    left_cannon_rotation: KEY_Q,
    right_cannon_rotation: KEY_E,
    show_stats: KEY_SPACE,
};

let first_player_control =
{
    is_keyboard: false,
    left_move: BUTTON_LEFT,
    right_move: BUTTON_RIGHT,
    bottom_move: BUTTON_DOWN,
    up_move: BUTTON_UP,
    fire: BUTTON_B,
    fire2: BUTTON_RB,
    weapon_change: BUTTON_LB,
    left_cannon_rotation: BUTTON_X,
    right_cannon_rotation: BUTTON_Y,
    show_stats: BUTTON_START,
};


var Input = {
    mouse: {
        x: 0,
        y: 0,
        pressed: false
    },

    keyboard: {
        keyup: {},
        keypressed: {},
        keydown: {}
    },


    IsButtonPressed: function(keycode, pad_index) 
    {
        return gamepads[pad_index] && (gamepads[pad_index].buttons[keycode]);
    },
    IsKeyPressed: function(keycode) 
    {
        return  this.keyboard.keypressed[keycode];  
    },

    IsKeyDown: function(keycode) {
        return this.keyboard.keydown[keycode];
    },

    IsKeyUp: function (keycode) {
        return this.keyboard.keyup[keycode];
    },

    IsMousePressed: function () {
        return this.mouse.pressed;
    },

    Update: function() {
        for (var property in this.keyboard.keyup) {
            if (this.keyboard.keyup.hasOwnProperty(property)) {
                this.keyboard.keyup[property] = false;
            }
        }
    },

    PostUpdate: function () {
        for (var property in this.keyboard.keydown) {
            if (this.keyboard.keydown.hasOwnProperty(property)) {
                this.keyboard.keydown[property] = false;
            }
        }
    }
};

function SetupKeyboardEvents ()
{
    AddEvent(document, "keydown", function (e) {
        //console.log(e.keyCode);
        //if (Input.keyboard.keyup[e.keyCode])
        Input.keyboard.keydown[e.keyCode] = true;
        Input.keyboard.keypressed[e.keyCode] = true;
    } );

    AddEvent(document, "keyup", function (e) {
        Input.keyboard.keyup[e.keyCode] = true;
        Input.keyboard.keypressed[e.keyCode] = false;
    } );

    function AddEvent (element, eventName, func)
    {
        if (element.addEventListener)
            element.addEventListener(eventName, func, false);
        else if (element.attachEvent)
            element.attachEvent(eventName, func);
    }
}

function SetupMouseEvents ()
{
    // mouse click event
    canvas_array[0].addEventListener("mousedown", MouseDown, false);
    // mouse move event
    canvas_array[0].addEventListener("mousemove", MouseMove, false);
    // mouse up event
    canvas_array[0].addEventListener("mouseup", MouseUp, false);
}

function MouseDown (event)
{
  /*  var rect = canvas_array[0].getBoundingClientRect();
    var clickX = event.clientX - rect.left;
    var clickY = event.clientY - rect.top;*/

    Input.mouse.pressed = true;

    //console.log("MouseDown: " + "X=" + clickX + ", Y=" + clickY);
}

function MouseUp (event)
{
    /*var rect = canvas_array[0].getBoundingClientRect();
    var clickX = event.clientX - rect.left;
    var clickY = event.clientY - rect.top;*/
    Input.mouse.pressed = false;

    //console.log("MouseUp: " + "X=" + clickX + ", Y=" + clickY);
}
function MouseMove (event)
{
    var rect = canvas_array[0].getBoundingClientRect();
    Input.mouse.x = event.clientX - rect.left;
    Input.mouse.y = event.clientY - rect.top;
}

