class Horizontal_Search
{
    constructor(tank)
    {
        this.origin_node;
        this.target_node;

        this.owner = tank;
        this.path = [];

        this.to_print = [];

        this.index = 0;

        this.closed_nodes = [];

        Horizontal_Search.raycast_suceed = true;
    }

    create_path(last_node)
    {
        this.path = [];
        let node;
        for(node = last_node; node != this.origin_node; node = node.parent )
        {
            if(node == undefined){  return;} 
                this.path.push(to_vector2(node));
        }
        if(node != undefined)
            this.path.push(to_vector2(node));

        this.path = this.path.reverse(); 

        this.smooth_path();
        this.to_print =   this.path;
    }

    print_path(ctx)
    {  
        for(let i = 0 ; i < this.to_print.length; ++i)
        {
            ctx.beginPath();
          
            ctx.arc(this.to_print[i].x,this.to_print[i].y, grid.node_length/2, 0, 2 * Math.PI, false);
            ctx.lineWidth = 3;
            ctx.strokeStyle = '#000000';
            ctx.stroke();
        }
    }

    find_path(origin_pos, target_pos)
    {
        this.origin_node = grid.from_world_to_node(origin_pos.x,origin_pos.y);
        this.target_node = grid.from_world_to_node(target_pos.x,target_pos.y);

        let closed_nodes = [];
        let open_nodes = [];   
        
        let node = this.origin_node;
        open_nodes.push(node);

        for(let i = 0; i < 6; ++i)
        {
            let result = this.get_neighbours_of_node_list(open_nodes,closed_nodes);
            if(result.found_target)
            {
                this.create_path(this.target_node);
                return;
            }
            closed_nodes = [];
            closed_nodes = open_nodes;
            open_nodes = [];
            open_nodes = result;
        }
        
        let closeset = this.get_closest_neighbour_of_node_list(open_nodes,target_pos );
        this.create_path(closeset);
    }

   
    callback(fixture,point,normal,fraction)
    {
        if (fixture.GetUserData() != null && fixture.GetUserData().type === "Wall")
            Horizontal_Search.raycast_suceed = false;
    }


    smooth_path()
    {
        let raycast_origin =   this.owner.position.from_world_to_box_scale();
        let raycast_destination = to_vector2(this.path[this.path.length-1]).from_world_to_box_scale();

        world_array[0].RayCast( this.callback, raycast_origin, raycast_destination);

        if(Horizontal_Search.raycast_suceed)
        {
            this.path[1] = this.path[this.path.length-1];
            this.path.length = 2;
        }  
        Horizontal_Search.raycast_suceed = true;
    }

    get_closest_neighbour_of_node_list(node_array, target_pos)
    {
        let min_dist = 9999999999;
        let selected_neighbour = null;
        for(let i = 0; i < node_array.length; ++i)
        {
            let dist = to_vector2(node_array[i]).subtract(target_pos).magnitude();
            if(dist < min_dist)
            {
                selected_neighbour = node_array[i];
                min_dist = dist;
            }
        }

        if(selected_neighbour == undefined)
            console.log("dist: " + min_dist);
            return selected_neighbour;
    }


    get_neighbours_of_node_list(node_array, closed_nodes)
    {
        let neighbour_array = [];
        let found_target = false;
        
        outer_loop: for(let i = 0; i < node_array.length && !found_target; ++i)
        {
            let neighbours = node_array[i].get_walkable_neighbours();
            
            for(let j = 0; j < neighbours.length; ++j)
            {
                if(neighbours[j].equal(this.target_node))
                {
                    neighbours[j].parent = node_array[i];
                    found_target=true;
                    break outer_loop;
                }
                if(!closed_nodes.some(e => (e.equal(neighbours[j]))) && !node_array.some(e => (e.equal(neighbours[j]))))
                {
                    neighbours[j].parent = node_array[i];
                    neighbour_array.push( neighbours[j]);
                }
            }
        }

        return found_target ? { found_target: true} : neighbour_array;
    }

}
